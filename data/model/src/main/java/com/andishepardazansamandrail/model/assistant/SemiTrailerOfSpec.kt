package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

class SemiTrailerOfSpec (@SerializedName("semiTrailerDelivery") val semiTrailerDelivery : SemiTrailerDelivery,
                         @SerializedName("semiTrailerTechnicalSpecifications") val semiTrailerTechnicalSpecifications : List<SemiTrailerTechnicalSpecification>)