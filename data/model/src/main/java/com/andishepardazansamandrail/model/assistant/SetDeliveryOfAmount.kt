package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

data class SetDeliveryOfAmount (@SerializedName("technicalSpecificationId") val technicalSpecificationId : Long,
                                @SerializedName("deliveryOfAmount") val deliveryOfAmount : Double)