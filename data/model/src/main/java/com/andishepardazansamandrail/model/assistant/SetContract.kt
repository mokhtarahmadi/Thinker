package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

class SetContract(@SerializedName("contractId") val contractId : Int,
                  @SerializedName("technicalSpecificationId") val technicalSpecificationId : Int,
                  @SerializedName("deliveryId") val deliveryId : Int,
                  @SerializedName("operatorId") val operatorId : Int,
                  @SerializedName("deliveryAmount") val deliveryAmount : Int = 0,
                  @SerializedName("valueAmount") val valueAmount : Int = 0
                  )