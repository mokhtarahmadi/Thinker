package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

class SetDelivery(@SerializedName("deliveryId") val deliveryId : Int ,
                  @SerializedName("semiTrailerId") val semiTrailerId : Int,
                  @SerializedName("personId") val personId : Int,
                  @SerializedName("operatorId") val operatorId : Int?,
                  @SerializedName("transitionDate") val transitionDate : String?,
                  @SerializedName("deliveryDate") val deliveryDate : String?)
