package com.andishepardazansamandrail.model.thinker

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Driver(
    @PrimaryKey
    @SerializedName("personId") var personId : Int,
    @SerializedName("docNumber") var docNumber : Int?,
    @SerializedName("smartCardId") var smartCardId : Int?,
    @SerializedName("driverRegisterDate") var driverRegisterDate : String?,
    @SerializedName("identityNumber") var identityNumber : String?,
    @SerializedName("firstName") var firstName : String?,
    @SerializedName("lastName") var lastName : String?,
    @SerializedName("fatherName") var fatherName : String?,
    @SerializedName("birthDay") var birthDay : String?,
    @SerializedName("driverType") var driverType : Int?,
    @SerializedName("licenseNumber") var licenseNumber : String?,
    @SerializedName("insuranceNumber") var insuranceNumber : String?,
    @SerializedName("isActive") var isActive : Boolean,
    @SerializedName("expireDate") var expireDate : String?,
    @SerializedName("residenceCity") var residenceCity : String?,
    @SerializedName("receiptPlace") var receiptPlace : Int?,
    @SerializedName("comment") var comment : String?,
    @SerializedName("statusCode") var statusCode : Int?,
    @SerializedName("educationCode") var educationCode : Int?,
    @SerializedName("healthDate") var healthDate : String?,
    @SerializedName("healthNumber") var healthNumber : Int?,
    @SerializedName("branch") var branch : String?,
    @SerializedName("passportCode") var passportCode : String?,
    @SerializedName("phoneNumber") var phoneNumber : String?,
    @SerializedName("nationalCode") var nationalCode : String?)