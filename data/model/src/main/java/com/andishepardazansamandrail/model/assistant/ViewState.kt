package com.andishepardazansamandrail.model.assistant

import java.io.Serializable

data class ViewState(val key : Int , val value : String) : Serializable { constructor():this(0,"")}
