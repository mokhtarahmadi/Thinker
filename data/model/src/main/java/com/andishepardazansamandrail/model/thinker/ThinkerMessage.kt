package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

class ThinkerMessage(@SerializedName("id") val id : Int,
                     @SerializedName("operatorId") val operatorId : Int?,
                     @SerializedName("subject") val subject : String,
                     @SerializedName("description") val description : String,
                     @SerializedName("status") val status : String,
                     @SerializedName("persianDateTime") val persianDateTime : String,)