package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

class ShowMessage(@SerializedName("id") val id : Int)