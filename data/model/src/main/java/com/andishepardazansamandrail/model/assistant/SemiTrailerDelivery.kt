package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

class SemiTrailerDelivery(@SerializedName("semiTrailerId") val semiTrailerId : Int,
                          @SerializedName("capacityName") val capacityName : String,
                          @SerializedName("linsensePlate") val linsensePlate : String,
                          @SerializedName("chassisNumber") val chassisNumber : Int,
                          @SerializedName("personId") val personId : Int,
                          @SerializedName("trailerCode") val trailerCode : String,
                          @SerializedName("deliveryId") val deliveryId : Int,
                          @SerializedName("personName") val personName : String,
                          @SerializedName("trailerName") val trailerName : String,
                          @SerializedName("deliveryDate") val deliveryDate : String,
                          @SerializedName("transitionDate") val transitionDate : String,
                          @SerializedName("persianDeliveryDate") val persianDeliveryDate : String,
                          @SerializedName("persianTransitionDate") val persianTransitionDate : String)


