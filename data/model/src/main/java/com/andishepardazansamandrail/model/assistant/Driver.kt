package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

class Driver(@SerializedName("personId") val personId : Int,
             @SerializedName("docNumber") val docNumber : Int,
             @SerializedName("smartCardId") val smartCardId : Int,
             @SerializedName("driverRegisterDate") val driverRegisterDate : String,
             @SerializedName("identityNumber") val identityNumber : String,
             @SerializedName("firstName") val firstName : String,
             @SerializedName("lastName") val lastName : String,
             @SerializedName("fatherName") val fatherName : String,
             @SerializedName("birthDay") val birthDay : String,
             @SerializedName("driverType") val driverType : Int,
             @SerializedName("licenseNumber") val licenseNumber : String,
             @SerializedName("insuranceNumber") val insuranceNumber : String,
             @SerializedName("isActive") val isActive : Boolean,
             @SerializedName("expireDate") val expireDate : String,
             @SerializedName("residenceCity") val residenceCity : String,
             @SerializedName("receiptPlace") val receiptPlace : Int,
             @SerializedName("comment") val comment : String,
             @SerializedName("statusCode") val statusCode : Int,
             @SerializedName("educationCode") val educationCode : Int,
             @SerializedName("healthDate") val healthDate : String,
             @SerializedName("healthNumber") val healthNumber : Int,
             @SerializedName("branch") val branch : String,
             @SerializedName("passportCode") val passportCode : String,
             @SerializedName("nationalCode") val nationalCode : String)