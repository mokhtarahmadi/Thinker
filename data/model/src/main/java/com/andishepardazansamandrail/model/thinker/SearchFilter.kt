package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

data class SearchFilter (

    @SerializedName("id") val id : Int,
    @SerializedName("title") val title : String,
    @SerializedName("Date") val date : String,
    )