package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

class AskQuestion(@SerializedName("askQuestionId") val askQuestionId : Int? = 0 ,
                  @SerializedName("personId") val personId : Int?,
                  @SerializedName("operatorId") val operatorId : Int?,
                  @SerializedName("fileName") val fileName : String?,
                  @SerializedName("fileType") val fileType : String?,
                  @SerializedName("fileSize") val fileSize : Int = 0,
                  @SerializedName("filePath") val filePath : String?,
                  @SerializedName("message") val message : String?,
                  @SerializedName("reponseMessage") val reponseMessage : String?,
                  @SerializedName("fileByte") val fileByte : String?,
                  @SerializedName("financialId") val financialId : Int,
                  @SerializedName("createDate") val createDate : String?)