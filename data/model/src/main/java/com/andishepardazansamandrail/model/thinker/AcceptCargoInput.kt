package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

class AcceptCargoInput(@SerializedName("barRequestId") val barRequestId : Int)