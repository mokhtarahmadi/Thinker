package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

class SearchSemiTrailer(@SerializedName("driverName") val driverName : String = "",
                        @SerializedName("linsensePlate") val linsensePlate : String = "",
                        @SerializedName("trailerCode") val trailerCode : String = "",
                        @SerializedName("trailerName") val trailerName : String = "",
                        @SerializedName("deliveryStepType") val deliveryStepType : Int = 0,
)