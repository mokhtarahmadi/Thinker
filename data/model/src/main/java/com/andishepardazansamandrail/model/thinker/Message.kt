package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

class Message (@SerializedName("askQuestionId") val askQuestionId : Int,
               @SerializedName("personId") val personId : Int,
               @SerializedName("driverName") val driverName : String,
               @SerializedName("responseMessage") val responseMessage : String,
               @SerializedName("financialId") val financialId : Int,
               @SerializedName("wayBill") val wayBill : Int)