package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

data class LoginInputAssistant(@SerializedName("email") val email : String,
                          @SerializedName("password") val password : String,)