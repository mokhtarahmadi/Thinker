package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

class Financial(@SerializedName("financialId") val financialId : Int?,
                @SerializedName("wayBill") val wayBill : String?,
                @SerializedName("issueDate") val issueDate : String?,
                @SerializedName("nationalCode") val nationalCode : String?,
                @SerializedName("payable") val payable : Int?,
                @SerializedName("clearing") val clearing : Int?,
                @SerializedName("remaining") val remaining : Int?,
                @SerializedName("personId") val personId : Int?)