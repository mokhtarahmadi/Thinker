package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

data class Degree(@SerializedName("id") val id : Int,
                  @SerializedName("name") val name : String)