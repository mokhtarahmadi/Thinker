package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

class SemiTrailerTechnicalSpecification(@SerializedName("semiTrailerId") val semiTrailerId : Int,
                                        @SerializedName("technicalSpecificationId") val technicalSpecificationId : Int,
                                        @SerializedName("technicalSpecificationName") val technicalSpecificationName : String,
                                        @SerializedName("contractId") val contractId : Int,
                                        @SerializedName("deliveryAmount") val deliveryAmount : Int,
                                        @SerializedName("deliveryOfAmount") val deliveryOfAmount : Int,
                                        @SerializedName("valueAmount") val valueAmount : Int,
                                        @SerializedName("erosionAmount") val erosionAmount : Int,
                                        @SerializedName("description") val description : String,
                                        @SerializedName("deliveryId") val deliveryId : Int)