package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

data class CreateDriver(@SerializedName("personId") var personId : Int? = 0,
                   @SerializedName("docNumber") var docNumber : Int? = 0,
                   @SerializedName("smartCardId") var smartCardId : Int? = 0,
                   @SerializedName("driverRegisterDate") var driverRegisterDate : String? = null,
                   @SerializedName("identityNumber") var identityNumber : String? = null,
                   @SerializedName("firstName") var firstName : String? = null,
                   @SerializedName("lastName") var lastName : String? = null,
                   @SerializedName("fatherName") var fatherName : String? = null,
                   @SerializedName("birthDay") var birthDay : String? = null,
                   @SerializedName("driverType") var driverType : Int? = 0 ,
                   @SerializedName("licenseNumber") var licenseNumber : String? = null,
                   @SerializedName("insuranceNumber") var insuranceNumber : String? = null,
                   @SerializedName("isActive") var isActive : Boolean = false,
                   @SerializedName("expireDate") var expireDate : String? = null,
                   @SerializedName("residenceCity") var residenceCity : String? = null,
                   @SerializedName("receiptPlace") var receiptPlace : Int? = 0,
                   @SerializedName("comment") var comment : String? = null,
                   @SerializedName("statusCode") var statusCode : Int? = 0 ,
                   @SerializedName("educationCode") var educationCode : Int? = 0,
                   @SerializedName("healthDate") var healthDate : String? = null,
                   @SerializedName("healthNumber") var healthNumber : Int? = 0,
                   @SerializedName("branch") var branch : String? = null,
                   @SerializedName("passportCode") var passportCode : String? = null,
                   @SerializedName("phoneNumber") var phoneNumber : String? = null,
                   @SerializedName("nationalCode") var nationalCode : String? = null)