package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

data class LoginInput (

    @SerializedName("identityNumber") val identityNumber : String,
    @SerializedName("driverSmartCode") val driverSmartCode : String,

)