package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

data class Navy(@SerializedName("trailId") val trailId : Int,
                @SerializedName("parentId") val parentId : Int,
                @SerializedName("code") val code : Int,
                @SerializedName("maxWeight") val maxWeight : Int,
                @SerializedName("minWeight") val minWeight : Int,
                @SerializedName("name") val name : String,
                @SerializedName("slug") val slug : String)