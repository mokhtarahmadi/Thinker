package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

data class BarLocator(
    
    @SerializedName("barId") var barId : Int? = 0,
                      @SerializedName("latLong") var latLong : String,
                      @SerializedName("latitude") var latitude : String,
                      @SerializedName("longitude") var longitude : String)
