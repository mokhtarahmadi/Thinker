package com.andishepardazansamandrail.model

import com.google.gson.annotations.SerializedName

data class ApiResult<T>(@SerializedName("isSuccessed") val isSuccessed : Boolean,
                        @SerializedName("messages") val messages : List<String>,
                        @SerializedName("result") val result : T)
