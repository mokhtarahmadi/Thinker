package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Cargo(@SerializedName("barId") val barId : Int?,
                 @SerializedName("status") val status : Int?,
                 @SerializedName("operatorId") val operatorId : Int?,
                 @SerializedName("driverId") val driverId : Int?,
                 @SerializedName("appointmentTime") val appointmentTime : String?,
                 @SerializedName("appointmentDate") val appointmentDate : String?,
                 @SerializedName("price") val price : Int?,
                 @SerializedName("goodId") val goodId : Int?,
                 @SerializedName("packId") val packId : Int?,
                 @SerializedName("producerId") val producerId : String?,
                 @SerializedName("trailId") val trailId : Int?,
                 @SerializedName("fromCityId") val fromCityId : Int?,
                 @SerializedName("toCityId") val toCityId : Int?,
                 @SerializedName("good") val good : String?,
                 @SerializedName("fromCity") val fromCity : String?,
                 @SerializedName("toCity") val toCity : String?,
                 @SerializedName("pack") val pack : String?,
                 @SerializedName("trail") val trail : String?,
                 @SerializedName("wayBill") val wayBill : Long?,
                 @SerializedName("deliveryAddress") val deliveryAddress : String?,
                 @SerializedName("persianAppointmentDate") val persianAppointmentDate : String?,
                 @SerializedName("persianAppointmentTime") val persianAppointmentTime : String?,
                 @SerializedName("persianTripDuration") val persianTripDuration : String?,
                 @SerializedName("sourceAddress") val sourceAddress : String?,
                 @SerializedName("operatorPhonenumber") val operatorPhonenumber : Long?) : Serializable{
    constructor():this(
        0,0,0,0,"","",0,0,0,"",0,0,0,"","","","","",0,"","","","","",0
    )

}