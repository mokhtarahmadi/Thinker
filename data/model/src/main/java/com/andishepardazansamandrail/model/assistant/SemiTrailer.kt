package com.andishepardazansamandrail.model.assistant

import com.google.gson.annotations.SerializedName

class SemiTrailer (
    @SerializedName("semiTrailerId") val semiTrailerId : Int,
    @SerializedName("ownerId") val ownerId : Int,
    @SerializedName("trailerIid") val trailerIid : Int,
    @SerializedName("capacityId") val capacityId : Int,
    @SerializedName("capacityName") val capacityName : String,
    @SerializedName("productionDate") val productionDate : String,
    @SerializedName("contractorName") val contractorName : String,
    @SerializedName("chassisNumber") val chassisNumber : Int,
    @SerializedName("trailerCode") val trailerCode : String,
    @SerializedName("linsensePlate") val linsensePlate : String,
    @SerializedName("persianProductionDate") val persianProductionDate : String)