package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class FinancialMasterDetail(@SerializedName("totalPayable") val totalPayable : Long,
                            @SerializedName("totalClearing") val totalClearing : Long,
                            @SerializedName("totalRemaining") val totalRemaining : Long,
                            @SerializedName("financialDetails") val financialDetails : List<FinancialDetail>?) :
    Serializable{
        constructor() : this(0,0,0,null)
    }
class FinancialDetail(@SerializedName("financialId") val financialId : Int,
                      @SerializedName("wayBill") val wayBill : String,
                      @SerializedName("issueDate") val issueDate : String,
                      @SerializedName("deliveryDate") val deliveryDate : String,
                      @SerializedName("nationalCode") val nationalCode : String,
                      @SerializedName("payable") val payable : Int,
                      @SerializedName("clearing") val clearing : Int,
                      @SerializedName("remaining") val remaining : Int,
                      @SerializedName("personId") val personId : Int,
                      @SerializedName("payDate") val payDate : String) : Serializable{
    constructor() : this(0,"","","","",0,0,0,0,"")
}

class FinancialDetails: ArrayList<FinancialDetail>(), Serializable