package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

data class Login (

    @SerializedName("token") val token : String,
    @SerializedName("user") val user : String,
    @SerializedName("message") val message : String,
    @SerializedName("isSuccessed") val isSuccessed : Boolean,


)