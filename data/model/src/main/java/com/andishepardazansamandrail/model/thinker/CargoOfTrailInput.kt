package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

class CargoOfTrailInput(@SerializedName("trailId") val trailId : Int,
                        @SerializedName("barStatus") val barStatus : Int,
                        @SerializedName("persianDate") val persianDate : String?)