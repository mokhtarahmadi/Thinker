package com.andishepardazansamandrail.model.thinker

import com.google.gson.annotations.SerializedName

data class CargoInput(@SerializedName("barId") val barId : Int)