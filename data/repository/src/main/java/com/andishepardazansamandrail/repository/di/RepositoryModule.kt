package com.andishepardazansamandrail.repository.di

import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.repository.assistant.AssistantRepositoryImpl
import com.andishepardazansamandrail.repository.thinker.ThinkerRepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.dsl.module

val repositoryModule = module {
    factory {
        AppDispatchers(
            Dispatchers.Main,
            Dispatchers.IO
        )
    }


    factory { ThinkerRepositoryImpl(get(),get(),get()) as ThinkerRepository
    }

    factory { AssistantRepositoryImpl(get(),get()) as AssistantRepository
    }
}