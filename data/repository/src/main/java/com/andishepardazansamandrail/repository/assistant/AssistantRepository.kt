package com.andishepardazansamandrail.repository.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.local.preferences.PreferenceHelper
import com.andishepardazansamandrail.model.assistant.*
import com.andishepardazansamandrail.model.thinker.Login
import com.andishepardazansamandrail.remote.assistant.AssistantDataSource

class AssistantRepositoryImpl(private val dataSource: AssistantDataSource,
                              private val preferenceHelper: PreferenceHelper

                              ) : AssistantRepository{

    override fun setUser(user: String) = preferenceHelper.setUser(user)

    override fun getUser(): String? = preferenceHelper.getUser()

    override fun setToken(token: String?) = preferenceHelper.setToken(token)

    override fun getToken(): String? = preferenceHelper.getToken()

    override fun setTokenType(tokenType: String) = preferenceHelper.setTokenType(tokenType)

    override fun getTokenType(): String?= getTokenType()
    override fun getUserType(): Int = preferenceHelper.getUserType()
    override fun setUserType(value: Int)  = preferenceHelper.setUserType(value)

    override fun setIdentityNumber(identityNumber: String) = preferenceHelper.setIdentityNumber(identityNumber)

    override fun getIdentityNumber(): String? = preferenceHelper.getIdentityNumber()

    override fun setSmartNumber(smartNumber: String) = preferenceHelper.setSmartNumber(smartNumber)

    override fun getSmartNumber(): String? = preferenceHelper.getSmartNumber()
    override suspend fun fetchUserLoginAsync(
        url: String,
        body: LoginInputAssistant
    ): LiveData<Resource<Login>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.fetchUserLoginAsync(url, body)
                emit(Resource.success(res))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override fun getPercentsItems(): LiveData<Resource<List<Percent>>> {
        var percents : ArrayList<Percent> = ArrayList()
        var liveData: MutableLiveData<Resource<List<Percent>>> = MutableLiveData()

        percents.add(Percent(0,"0"))
        percents.add(Percent(5,"5"))
        percents.add(Percent(10,"10"))
        percents.add(Percent(15,"15"))
        percents.add(Percent(20,"20"))
        percents.add(Percent(25,"25"))
        percents.add(Percent(30,"30"))
        percents.add(Percent(35,"35"))
        percents.add(Percent(40,"40"))
        percents.add(Percent(45,"45"))
        percents.add(Percent(50,"50"))
        percents.add(Percent(55,"55"))
        percents.add(Percent(60,"60"))
        percents.add(Percent(65,"65"))
        percents.add(Percent(70,"70"))
        percents.add(Percent(75,"75"))
        percents.add(Percent(80,"80"))
        percents.add(Percent(85,"85"))
        percents.add(Percent(90,"90"))
        percents.add(Percent(95,"95"))
        percents.add(Percent(100,"100"))

        liveData.value = Resource(Resource.Status.SUCCESS,percents,null)
        return liveData
    }

    override suspend fun setDeliveryAsync(
        url: String,
        setDelivery: SetDelivery
    ): LiveData<Resource<Int>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.setDeliveryAsync(url, setDelivery)
                emit(Resource.success(res.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }
    override suspend fun setContractAsync(
        url: String,
        setContracts: List<SetContract>
    ): LiveData<Resource<Unit>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.setContractAsync(url, setContracts)
                emit(Resource.success(res))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }
    override suspend fun getDriversAsync(url: String): LiveData<Resource<List<Driver>>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.getDriversAsync(url)
                emit(Resource.success(res.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun setDeliveryOfAmount(
        url: String,
        setDeliveryOfAmounts: List<SetDeliveryOfAmount>
    ): LiveData<Resource<Unit>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.setDeliveryOfAmount(url,setDeliveryOfAmounts)
                emit(Resource.success(res))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }


    override suspend fun getTechnicalSpecificationBySemiTrailer(url: String , trailerCode : String): LiveData<Resource<SemiTrailerOfSpec>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.getTechnicalSpecificationBySemiTrailer(url,trailerCode)
                emit(Resource.success(res.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun getSemiTrailersAsync(url: String): LiveData<Resource<List<SemiTrailer>>> =
    liveData {
        emit(Resource.loading(null))
        try {
            val res = dataSource.getSemiTrailersAsync(url)
            emit(Resource.success(res.result))
        } catch (exception: Exception) {
            emit(Resource.error(exception, null))
        }
    }


    override suspend fun getSemiTrailerDeliveriesAsync(
        url: String,
        searchSemiTrailer: SearchSemiTrailer
    ): LiveData<Resource<List<SemiTrailerDelivery>>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.getSemiTrailerDeliveriesAsync(url,searchSemiTrailer)
                emit(Resource.success(res.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun getSemiTrailerDeliveriesAsync(url: String): LiveData<Resource<List<SemiTrailerDelivery>>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.getSemiTrailerDeliveriesAsync(url)
                emit(Resource.success(res.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun getSemiTrailersDeliveredAsync(url: String,searchSemiTrailer: SearchSemiTrailer): LiveData<Resource<List<SemiTrailersDelivered>>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val res = dataSource.getSemiTrailersDeliveredAsync(url,searchSemiTrailer)
                emit(Resource.success(res.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

}