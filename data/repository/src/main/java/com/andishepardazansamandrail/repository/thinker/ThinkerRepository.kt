package com.andishepardazansamandrail.repository.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.local.dao.DriverDao
import com.andishepardazansamandrail.local.preferences.PreferenceHelper
import com.andishepardazansamandrail.model.thinker.*
import com.andishepardazansamandrail.remote.thinker.ThinkerDataSource

class ThinkerRepositoryImpl(
    private val dataSource: ThinkerDataSource,
    private val driverDao: DriverDao,
    private val preferenceHelper: PreferenceHelper,
) : ThinkerRepository {

    override suspend fun userLogin(
        url: String,
        identityNumber: String,
        driverSmartCode: String
    ): LiveData<Resource<Login>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val login = dataSource.fetchUserLoginAsync(url, identityNumber, driverSmartCode)
                emit(Resource.success(login))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun driverInfo(url: String): LiveData<Resource<Driver>> =
        liveData {
            val disposable = emitSource(
                driverDao.getDriver().map {
                    Resource.loading(it)
                }
            )
            try {
                val user = dataSource.fetchDriverInfoAsync(url)

                disposable.dispose()
                driverDao.save(user.result)
                emitSource(
                    driverDao.getDriver().map {
                        Resource.success(it)
                    }
                )
            } catch (exception: Exception) {
                emitSource(
                    driverDao.getDriver().map {
                        Resource.error(exception, it)
                    }
                )
            }

    }

    override suspend fun fetchCargoAsync(url: String): LiveData<Resource<List<Cargo>>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.fetchCargoAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun fetchCargoRequestedAsync(url: String): LiveData<Resource<List<Cargo>>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.fetchCargoRequestedAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun fetchDriverFinancialAsync(url: String): LiveData<Resource<List<Financial>>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.fetchDriverFinancialAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun fetchDriverFinancialMasterDetailAsync(url: String): LiveData<Resource<FinancialMasterDetail>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.fetchDriverFinancialMasterDetailAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun fetchActiveCargoAsync(url: String): LiveData<Resource<Cargo>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.fetchActiveCargoAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }
    override suspend fun requestCargoAsync(
        url: String,
        cargoInput: CargoInput
    ): LiveData<Resource<Boolean>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.requestCargoAsync(url,cargoInput)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun acceptCargoAsync(
        url: String,
        cargoInput: CargoInput
    ): LiveData<Resource<Boolean>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.acceptCargoAsync(url,cargoInput)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun arrivedToLoadingPlace(
        url: String,
        cargoInput: CargoInput
    ): LiveData<Resource<Boolean>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.arrivedToLoadingPlace(url,cargoInput)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun arrivedToFinishPlace(
        url: String,
        cargoInput: CargoInput
    ): LiveData<Resource<Boolean>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.arrivedToFinishPlace(url,cargoInput)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun cancelCargoAsync(
        url: String,
    ): LiveData<Resource<Boolean>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.cancelCargoAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun cancelCargoRequestedAsync(
        url: String,
    ): LiveData<Resource<Boolean>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.cancelCargoRequestedAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }
    override suspend fun finishCargoAsync(
        url: String,
        cargoInput: CargoInput
    ): LiveData<Resource<Boolean>>   =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.finishCargoAsync(url,cargoInput)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun loadingFinishedAsync(
        url: String,
        cargoInput: CargoInput
    ): LiveData<Resource<Boolean>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.loadingFinishedAsync(url,cargoInput)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun barLocatorAsync(
        url: String,
        barLocator: BarLocator
    ): LiveData<Resource<Boolean>>  =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.barLocatorAsync(url,barLocator)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun editDriverInfoAsync(
        url: String,
        driver: CreateDriver
    ): LiveData<Resource<Boolean>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.editDriverInfoAsync(url,driver)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }

    override suspend fun fetchNavyAsync(url: String): LiveData<Resource<List<Navy>>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.fetchNavyAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
        }
    override suspend fun fetchNavyTypeAsync(url: String): LiveData<Resource<List<Navy>>> =
        liveData {
            emit(Resource.loading(null))
            try {
                val result = dataSource.fetchNavyTypeAsync(url)
                emit(Resource.success(result.result))
            } catch (exception: Exception) {
                emit(Resource.error(exception, null))
            }
    }

    override suspend fun fetchCargoesAsync(
        url: String,
        cargoOfTrailInput: CargoOfTrailInput
    ): LiveData<Resource<List<Cargo>>>  =
    liveData {
        emit(Resource.loading(null))
        try {
            val result = dataSource.fetchCargoesAsync(url,CargoOfTrailInput(preferenceHelper.getNavyTypeId()!! , 0,cargoOfTrailInput.persianDate))
            emit(Resource.success(result.result))
        } catch (exception: Exception) {
            emit(Resource.error(exception, null))
        }
    }

    override suspend fun askQuestionAsync(url: String, body: AskQuestion): LiveData<Resource<Int>> =   liveData {
        emit(Resource.loading(null))
        try {
            val result = dataSource.askQuestionAsync(url, body)
            emit(Resource.success(result.result))
        } catch (exception: Exception) {
            emit(Resource.error(exception, null))
        }
    }

    override suspend fun fetchMessagesAsync(url: String): LiveData<Resource<List<Message>>> =   liveData {
        emit(Resource.loading(null))
        try {
            val result = dataSource.fetchMessagesAsync(url)
            emit(Resource.success(result.result))
        } catch (exception: Exception) {
            emit(Resource.error(exception, null))
        }
    }

    override suspend fun fetchThinkerMessages(url: String): LiveData<Resource<List<ThinkerMessage>>> =   liveData {
        emit(Resource.loading(null))
        try {
            val result = dataSource.fetchThinkerMessages(url)
            emit(Resource.success(result.result))
        } catch (exception: Exception) {
            emit(Resource.error(exception, null))
        }
    }

    override suspend fun showMessage(url: String, showMessage: ShowMessage): LiveData<Resource<Boolean>> =   liveData {
        emit(Resource.loading(null))
        try {
            val result = dataSource.showMessage(url,showMessage)
            emit(Resource.success(result.result))
        } catch (exception: Exception) {
            emit(Resource.error(exception, null))
        }
    }

    override fun setUser(user: String) = preferenceHelper.setUser(user)

    override fun getUser(): String? = preferenceHelper.getUser()

    override fun setToken(token: String?) = preferenceHelper.setToken(token)

    override fun getToken(): String? = preferenceHelper.getToken()

    override fun setTokenType(tokenType: String) = preferenceHelper.setTokenType(tokenType)

    override fun getTokenType(): String?= getTokenType()
    override fun setCargoStatus(status: Int) = preferenceHelper.setCargoStatus(status)

    override fun getCargoStatus(): Int?= preferenceHelper.getCargoStatus()
    override fun setBarId(barId: Int) = preferenceHelper.setBarId(barId)
    override fun getBarId(): Int?= preferenceHelper.getBarId()

    override fun setIdentityNumber(identityNumber: String) = preferenceHelper.setIdentityNumber(identityNumber)

    override fun getIdentityNumber(): String? = preferenceHelper.getIdentityNumber()

    override fun setSmartNumber(smartNumber: String) = preferenceHelper.setSmartNumber(smartNumber)

    override fun getSmartNumber(): String? = preferenceHelper.getSmartNumber()
    override fun setNavyId(navyId: Int) = preferenceHelper.setNavyId(navyId)

    override fun getNavyId(): Int? = preferenceHelper.getNavyId()
    override fun setNavyTypeId(navyTypeId: Int) = preferenceHelper.setNavyTypeId(navyTypeId)

    override fun getNavyTypeId(): Int? = preferenceHelper.getNavyTypeId()

    override fun setViewSelected(viewState: Int) = preferenceHelper.setViewSelected(viewState)
    override fun getViewSelected(): Int = preferenceHelper.getViewSelected()

    override fun getDegrees(): LiveData<Resource<List<Degree>>> {
        var degrees : ArrayList<Degree> = ArrayList()
        var liveData: MutableLiveData<Resource<List<Degree>>> = MutableLiveData()
        degrees.add(Degree(1,"بی سواد"))
        degrees.add(Degree(2,"ابتدایی"))
        degrees.add(Degree(3,"سیکل"))
        degrees.add(Degree(3,"دبیرستان"))
        degrees.add(Degree(5,"دیپلم"))
        degrees.add(Degree(6,"کاردانی"))
        degrees.add(Degree(7,"کارشناسی"))
        degrees.add(Degree(8,"کارشناسی ارشد"))
        degrees.add(Degree(9,"دکترا"))
        liveData.value = Resource(Resource.Status.SUCCESS,degrees,null)
        return liveData
    }

    override fun getUserType(): Int = preferenceHelper.getUserType()
    override fun setUserType(value: Int)  = preferenceHelper.setUserType(value)
}
