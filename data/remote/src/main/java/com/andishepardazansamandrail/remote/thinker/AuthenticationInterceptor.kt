package com.andishepardazansamandrail.remote.thinker

import com.google.gson.Gson
import com.andishepardazansamandrail.local.preferences.PreferenceHelper
import com.andishepardazansamandrail.model.thinker.Login
import kotlinx.coroutines.*
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception

class AuthenticationInterceptor(private val preferenceHelper: PreferenceHelper):
    Interceptor {


    @Throws(IOException::class)
     override fun intercept(chain: Interceptor.Chain): Response {
         val originalRequest = chain.request()
         val authenticationRequest = request(originalRequest)
         val initialResponse = chain.proceed(authenticationRequest)

         if (initialResponse.code() == 401 || initialResponse.code() == 422) {

             CoroutineScope(Dispatchers.IO).launch {
                 if (preferenceHelper.getUserType()!=0){
                     when(preferenceHelper.getUserType())
                     {
                         1 -> refreshToken()
                         2 -> refreshToken2()
                     }
                 }
                 withContext(Dispatchers.IO) {
                     try {
                         intercept(chain)
                     } catch (ex: Exception) {
                         ex.printStackTrace()
                     }
                 }

             }

         }

         return initialResponse
     }

     private fun request(originalRequest: Request): Request {
         return originalRequest.newBuilder()
             .addHeader(
                 "Authorization",
                 "bearer " + preferenceHelper.getToken()
             )
             .addHeader("Content-Type", "application/json; charset=UTF-8").build()

     }

       private fun refreshToken(){
           val JSON = MediaType.get("application/json; charset=utf-8")
           val jsonObject = JSONObject()

           jsonObject.put("identityNumber", preferenceHelper.getIdentityNumber())
           jsonObject.put("driverSmartCode", preferenceHelper.getSmartNumber())

           var reqbody: RequestBody = RequestBody.create(JSON,jsonObject.toString())
           val client = OkHttpClient()
           val request = Request.Builder()
               .url( "http://193.176.240.109:8403/api/Account/login")
               .method("POST", reqbody)
               .addHeader("Content-Type","application/json; charset=UTF-8")
               .build()

           try {
               val response = client.newCall(request).execute()
               if (response.code() === 200) {
                   // Get response
                   val jsonData = response.body()?.string()

                   val gson = Gson()
                   val refreshTokenResponseModel = gson.fromJson(jsonData, Login::class.java)
                   if (refreshTokenResponseModel!=null) {
                       preferenceHelper.setToken(refreshTokenResponseModel.token)

                   }

               }
           } catch (e: IOException) {
               e.printStackTrace()
           }

    }

       private fun refreshToken2(){
        val JSON = MediaType.get("application/json; charset=utf-8")
        val jsonObject = JSONObject()

        jsonObject.put("email", preferenceHelper.getIdentityNumber())
        jsonObject.put("password", preferenceHelper.getSmartNumber())

        var reqbody: RequestBody = RequestBody.create(JSON,jsonObject.toString())
        val client = OkHttpClient()
        val request = Request.Builder()
            .url( "http://193.176.240.109:8403/api/Account/Assistant/login")
            .method("POST", reqbody)
            .addHeader("Content-Type","application/json; charset=UTF-8")
            .build()

        try {
            val response = client.newCall(request).execute()
            if (response.code() === 200) {
                // Get response
                val jsonData = response.body()?.string()

                val gson = Gson()
                val refreshTokenResponseModel = gson.fromJson(jsonData, Login::class.java)
                if (refreshTokenResponseModel!=null) {
                    preferenceHelper.setToken(refreshTokenResponseModel.token)

                }

            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
 }
