package com.andishepardazansamandrail.remote.di

import com.andishepardazansamandrail.remote.assistant.AssistantDataSource
import com.andishepardazansamandrail.remote.assistant.AssistantService
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.andishepardazansamandrail.remote.thinker.AuthenticationInterceptor
import com.andishepardazansamandrail.remote.thinker.ThinkerDataSource
import com.andishepardazansamandrail.remote.thinker.ThinkerService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun createRemoteModule(baseUrl: String) = module {

    factory<Interceptor> {
        HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.HEADERS)
    }

    factory {
        OkHttpClient.Builder().addInterceptor(AuthenticationInterceptor(get())).build()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    factory{ get<Retrofit>().create(ThinkerService::class.java) }

    factory { ThinkerDataSource(get()) }

    factory{ get<Retrofit>().create(AssistantService::class.java) }

    factory { AssistantDataSource(get()) }

}