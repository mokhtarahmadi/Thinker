package com.andishepardazansamandrail.remote.thinker

import com.andishepardazansamandrail.model.ApiResult
import com.andishepardazansamandrail.model.thinker.*
import retrofit2.http.Body
import retrofit2.http.Url

class ThinkerDataSource (private val thinkerService: ThinkerService)  {

    suspend fun fetchUserLoginAsync(url :String ,identityNumber: String, driverSmartCode: String) =
        thinkerService.fetchUserLoginAsync( url, LoginInput(identityNumber, driverSmartCode))
    suspend fun fetchDriverInfoAsync(url :String) = thinkerService.fetchDriverInfoAsync( url)
    suspend fun fetchCargoAsync(url: String) = thinkerService.fetchCargoAsync(url)
    suspend fun fetchCargoRequestedAsync(url: String) = thinkerService.fetchCargoRequestedAsync(url)
    suspend fun fetchActiveCargoAsync(url: String) = thinkerService.fetchActiveCargoAsync(url)
    suspend fun acceptCargoAsync(url: String, cargoInput: CargoInput)= thinkerService.acceptCargoAsync(url,cargoInput)
    suspend fun requestCargoAsync(url: String, cargoInput: CargoInput)= thinkerService.requestCargoAsync(url,cargoInput)
    suspend fun cancelCargoAsync(url: String)= thinkerService.cancelCargoAsync(url)
    suspend fun cancelCargoRequestedAsync(url: String)= thinkerService.cancelCargoRequestedAsync(url)
    suspend fun finishCargoAsync(url: String, cargoInput: CargoInput)= thinkerService.finishCargoAsync(url,cargoInput)
    suspend fun loadingFinishedAsync(url: String, cargoInput: CargoInput)= thinkerService.loadingFinishedAsync(url,cargoInput)
    suspend fun editDriverInfoAsync( url: String, driver: CreateDriver)= thinkerService.editDriverInfoAsync(url,driver)
    suspend fun barLocatorAsync(url: String, barLocator: BarLocator)= thinkerService.barLocatorAsync(url,barLocator)
    suspend fun fetchDriverFinancialAsync( url: String)= thinkerService.fetchDriverFinancialAsync(url)
    suspend fun fetchDriverFinancialMasterDetailAsync( url: String)= thinkerService.fetchDriverFinancialMasterDetailAsync(url)
    suspend fun fetchNavyAsync( url: String)= thinkerService.fetchNavyAsync(url)
    suspend fun fetchNavyTypeAsync( url: String)= thinkerService.fetchNavyTypeAsync(url)
    suspend fun fetchCargoesAsync(url: String, cargoOfTrailInput: CargoOfTrailInput)= thinkerService.fetchCargoesAsync(url,cargoOfTrailInput)
    suspend fun askQuestionAsync(url: String, body: AskQuestion) = thinkerService.askQuestionAsync(url,body)
    suspend fun fetchMessagesAsync(url: String) = thinkerService.fetchMessagesAsync(url)
    suspend fun arrivedToLoadingPlace(url: String, body: CargoInput) = thinkerService.arrivedToLoadingPlace(url,body)
    suspend fun arrivedToFinishPlace(url: String, body: CargoInput) = thinkerService.arrivedToFinishPlace(url,body)
    suspend fun fetchThinkerMessages(url: String) = thinkerService.fetchThinkerMessages(url)
    suspend fun showMessage( url: String, body: ShowMessage) = thinkerService.showMessage(url,body)

}