package com.andishepardazansamandrail.remote.assistant

import com.andishepardazansamandrail.model.ApiResult
import com.andishepardazansamandrail.model.assistant.*
import com.andishepardazansamandrail.model.thinker.Login
import com.andishepardazansamandrail.model.thinker.LoginInput
import retrofit2.http.*

interface AssistantService {

    @POST
    suspend fun fetchUserLoginAsync(@Url url: String, @Body body: LoginInputAssistant): Login
    @POST
    suspend fun setDeliveryAsync(@Url url: String, @Body setDelivery: SetDelivery): ApiResult<Int>
    @POST
    suspend fun setContractAsync(@Url url: String, @Body setContracts: List<SetContract>): Unit
    @PUT
    suspend fun setDeliveryOfAmount(@Url url: String, @Body setContracts: List<SetDeliveryOfAmount>): Unit
    @GET
    suspend fun getDriversAsync(@Url url: String): ApiResult<List<Driver>>
    @GET
    suspend fun getSemiTrailersAsync(@Url url: String): ApiResult<List<SemiTrailer>>
    @POST
    suspend fun getSemiTrailerDeliveriesAsync(@Url url: String,@Body searchSemiTrailer : SearchSemiTrailer): ApiResult<List<SemiTrailerDelivery>>
    @GET
    suspend fun getSemiTrailerDeliveriesAsync(@Url url: String): ApiResult<List<SemiTrailerDelivery>>
    @GET
    suspend fun getTechnicalSpecificationBySemiTrailer(@Url url: String): ApiResult<SemiTrailerOfSpec>
    @POST
    suspend fun getSemiTrailersDeliveredAsync(@Url url: String,@Body searchSemiTrailer: SearchSemiTrailer): ApiResult<List<SemiTrailersDelivered>>


}