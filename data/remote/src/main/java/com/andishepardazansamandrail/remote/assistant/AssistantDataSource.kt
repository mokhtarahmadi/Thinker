package com.andishepardazansamandrail.remote.assistant

import com.andishepardazansamandrail.model.assistant.*
import retrofit2.http.Body
import retrofit2.http.Url

class AssistantDataSource(private val assistantService: AssistantService) {

    suspend fun fetchUserLoginAsync( url: String, body: LoginInputAssistant)= assistantService.fetchUserLoginAsync(url,body)
    suspend fun setDeliveryAsync( url: String, setDelivery: SetDelivery) = assistantService.setDeliveryAsync(url,setDelivery)
    suspend fun setContractAsync( url: String, setContracts: List<SetContract>) = assistantService.setContractAsync(url,setContracts)
    suspend fun getDriversAsync( url: String) = assistantService.getDriversAsync(url)
    suspend fun setDeliveryOfAmount( url: String, setDeliveryOfAmounts: List<SetDeliveryOfAmount>)= assistantService.setDeliveryOfAmount(url,setDeliveryOfAmounts)
    suspend fun getSemiTrailersAsync( url: String) = assistantService.getSemiTrailersAsync(url)
    suspend fun getSemiTrailerDeliveriesAsync( url: String,  searchSemiTrailer : SearchSemiTrailer) = assistantService.getSemiTrailerDeliveriesAsync(url,searchSemiTrailer)
    suspend fun getSemiTrailerDeliveriesAsync( url: String) = assistantService.getSemiTrailerDeliveriesAsync(url)
    suspend fun getTechnicalSpecificationBySemiTrailer( url: String , trailerCode : String) = assistantService.getTechnicalSpecificationBySemiTrailer(url)
    suspend fun getSemiTrailersDeliveredAsync(@Url url: String, searchSemiTrailer: SearchSemiTrailer)= assistantService.getSemiTrailersDeliveredAsync(url,searchSemiTrailer)
}