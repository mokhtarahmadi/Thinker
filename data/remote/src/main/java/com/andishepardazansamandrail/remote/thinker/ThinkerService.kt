package com.andishepardazansamandrail.remote.thinker

import com.andishepardazansamandrail.model.ApiResult
import com.andishepardazansamandrail.model.thinker.*
import retrofit2.http.*

interface ThinkerService {

    @POST
    suspend fun fetchUserLoginAsync(@Url url: String, @Body body: LoginInput): Login
    @GET
    suspend fun fetchDriverInfoAsync(@Url url: String): ApiResult<Driver>
    @GET
    suspend fun fetchCargoAsync(@Url url: String): ApiResult<List<Cargo>>
    @GET
    suspend fun fetchCargoRequestedAsync(@Url url: String): ApiResult<List<Cargo>>
    @GET
    suspend fun fetchActiveCargoAsync(@Url url: String): ApiResult<Cargo>
    @GET
    suspend fun fetchDriverFinancialAsync(@Url url: String): ApiResult<List<Financial>>
    @GET
    suspend fun fetchDriverFinancialMasterDetailAsync(@Url url: String): ApiResult<FinancialMasterDetail>
    @POST
    suspend fun requestCargoAsync(@Url url: String,@Body body: CargoInput): ApiResult<Boolean>
    @POST
    suspend fun acceptCargoAsync(@Url url: String,@Body body: CargoInput): ApiResult<Boolean>
    @POST
    suspend fun fetchCargoesAsync(@Url url: String,@Body body: CargoOfTrailInput): ApiResult<List<Cargo>>
    @POST
    suspend fun finishCargoAsync(@Url url: String,@Body body: CargoInput): ApiResult<Boolean>
    @PUT
    suspend fun editDriverInfoAsync(@Url url: String,@Body body: CreateDriver): ApiResult<Boolean>
    @POST
    suspend fun loadingFinishedAsync(@Url url: String,@Body body: CargoInput): ApiResult<Boolean>
    @DELETE
    suspend fun cancelCargoAsync(@Url url: String): ApiResult<Boolean>
    @DELETE
    suspend fun cancelCargoRequestedAsync(@Url url: String): ApiResult<Boolean>
    @POST
    suspend fun barLocatorAsync(@Url url: String,@Body body: BarLocator): ApiResult<Boolean>
    @POST
    suspend fun askQuestionAsync(@Url url: String,@Body body: AskQuestion): ApiResult<Int>
    @GET
    suspend fun fetchNavyAsync(@Url url: String): ApiResult<List<Navy>>
    @GET
    suspend fun fetchNavyTypeAsync(@Url url: String): ApiResult<List<Navy>>
    @GET
    suspend fun fetchMessagesAsync(@Url url: String): ApiResult<List<Message>>
    @POST
    suspend fun arrivedToLoadingPlace(@Url url: String,@Body body: CargoInput): ApiResult<Boolean>
    @POST
    suspend fun arrivedToFinishPlace(@Url url: String,@Body body: CargoInput): ApiResult<Boolean>
    @GET
    suspend fun fetchThinkerMessages(@Url url: String): ApiResult<List<ThinkerMessage>>
    @POST
    suspend fun showMessage(@Url url: String,@Body body: ShowMessage): ApiResult<Boolean>
}