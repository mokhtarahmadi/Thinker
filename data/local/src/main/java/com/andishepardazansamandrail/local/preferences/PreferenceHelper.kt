package com.andishepardazansamandrail.local.preferences

import android.content.Context
import android.content.SharedPreferences

interface PreferenceHelper {
    fun setUser(user : String)
    fun getUser(): String?
    fun setToken(token: String?)
    fun getToken(): String?
    fun setTokenType(tokenType: String)
    fun getTokenType(): String?
    fun setCargoStatus(status: Int)
    fun getCargoStatus(): Int?
    fun setIdentityNumber(identityNumber : String)
    fun getIdentityNumber(): String?
    fun setSmartNumber(smartNumber : String)
    fun getSmartNumber(): String?
    fun setBarId(barId: Int)
    fun getBarId() : Int?
    fun setNavyId(barId: Int)
    fun getNavyId() : Int?
    fun setNavyTypeId(barId: Int)
    fun getNavyTypeId() : Int?
    fun getViewSelected(): Int
    fun setViewSelected(viewState: Int)
    fun getUserType(): Int
    fun setUserType(value: Int)


}
class AppPreferenceHelper(private val context: Context, private val prefFileName: String): PreferenceHelper {


    companion object {
        private const val PREF_KEY_TOKEN = "PREF_KEY_TOKEN"
        private const val PREF_KEY_TOKEN_TYPE = "PREF_KEY_TOKEN_TYPE"
        private const val PREF_USER= "PREF_USER"
        private const val PREF_CARGO_STATUS= "PREF_CARGO_STATUS"
        private const val PREF_IDENTITY_NUMBER= "PREF_IDENTITY_NUMBER"
        private const val PREF_SMART_NUMBER= "PREF_SMART_NUMBER"
        private const val PREF_BAR_ID= "PREF_BAR_ID"
        private const val PREF_NAVY_ID= "PREF_NAVY_ID"
        private const val PREF_NAVY_TYPE_ID= "PREF_NAVY_TYPE_ID"
        private const val PREF_KEY_VIEW_SELECTED = "PREF_KEY_VIEW_SELECTED"
        private const val PREF_KEY_USER_TYPE = "PREF_KEY_USER_TYPE"


    }


    private val mPrefs: SharedPreferences =
        context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    override fun setToken(token: String?) = mPrefs.edit().putString(PREF_KEY_TOKEN, token).apply()
    override fun getToken(): String? = mPrefs.getString(PREF_KEY_TOKEN, null)

    override fun setTokenType(tokenType: String) = mPrefs.edit().putString(PREF_KEY_TOKEN_TYPE, tokenType).apply()
    override fun getTokenType(): String?  = mPrefs.getString(PREF_KEY_TOKEN_TYPE, null)

    override fun setCargoStatus(status: Int) = mPrefs.edit().putInt(PREF_CARGO_STATUS, status).apply()
    override fun getCargoStatus(): Int? = mPrefs.getInt(PREF_CARGO_STATUS, 0)

    override fun setIdentityNumber(identityNumber: String) = mPrefs.edit().putString(PREF_IDENTITY_NUMBER, identityNumber).apply()
    override fun getIdentityNumber(): String? =  mPrefs.getString(PREF_IDENTITY_NUMBER, null)

    override fun setSmartNumber(smartNumber: String)= mPrefs.edit().putString(PREF_SMART_NUMBER, smartNumber).apply()
    override fun getSmartNumber(): String?  =  mPrefs.getString(PREF_SMART_NUMBER, null)

    override fun setBarId(barId: Int) = mPrefs.edit().putInt(PREF_BAR_ID, barId).apply()
    override fun getBarId(): Int? =  mPrefs.getInt(PREF_BAR_ID, 0)

    override fun setNavyId(navyId: Int)  = mPrefs.edit().putInt(PREF_NAVY_ID, navyId).apply()
    override fun getNavyId(): Int? = mPrefs.getInt(PREF_NAVY_ID, 0)

    override fun setNavyTypeId(navyTypeId: Int) =  mPrefs.edit().putInt(PREF_NAVY_TYPE_ID, navyTypeId).apply()
    override fun getNavyTypeId(): Int? =  mPrefs.getInt(PREF_NAVY_TYPE_ID, 0)

    override fun setUser(user: String) = mPrefs.edit().putString(PREF_USER, user).apply()
    override fun getUser(): String? = mPrefs.getString(PREF_USER, null)

    override fun getViewSelected(): Int  = mPrefs.getInt(PREF_KEY_VIEW_SELECTED, 0)
    override fun setViewSelected(viewState: Int)= mPrefs.edit().putInt(PREF_KEY_VIEW_SELECTED, viewState).apply()

    override fun getUserType(): Int  = mPrefs.getInt(PREF_KEY_USER_TYPE, 0)
    override fun setUserType(value: Int) = mPrefs.edit().putInt(PREF_KEY_USER_TYPE, value).apply()
}