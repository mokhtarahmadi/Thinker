package com.andishepardazansamandrail.local.di

import com.andishepardazansamandrail.local.ThinkerDatabase
import com.andishepardazansamandrail.local.preferences.AppPreferenceHelper
import com.andishepardazansamandrail.local.preferences.PreferenceHelper
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

private const val DATABASE = "DATABASE"

val localModule = module {
    single { AppPreferenceHelper(androidContext(),"Thinker_pref") as PreferenceHelper }
    single(named(DATABASE)) { ThinkerDatabase.buildDatabase(androidContext()) }
    factory { (get(named(DATABASE)) as ThinkerDatabase).driverDao() }
}