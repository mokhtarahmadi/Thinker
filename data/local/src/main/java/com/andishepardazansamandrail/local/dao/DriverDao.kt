package com.andishepardazansamandrail.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.andishepardazansamandrail.model.thinker.Driver

@Dao
abstract class DriverDao : BaseDao<Driver>(){

    suspend fun save(driver: Driver) = insert(driver)

    @Query("SELECT * FROM Driver")
    abstract fun getDriver(): LiveData<Driver>

}