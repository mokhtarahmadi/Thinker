package com.andishepardazansamandrail.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.andishepardazansamandrail.local.converter.Converters
import com.andishepardazansamandrail.local.dao.DriverDao
import com.andishepardazansamandrail.model.thinker.Driver

@Database(entities = [Driver::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class ThinkerDatabase : RoomDatabase(){

    abstract fun driverDao(): DriverDao

    companion object {

        fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, ThinkerDatabase::class.java, "Thinker.db")
                .build()
    }
}