package com.andishepardazansamandrail.common.extension

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.andexert.library.RippleView
import com.google.android.material.snackbar.Snackbar
import com.andishepardazansamandrail.common.R
import com.andishepardazansamandrail.common.utils.Event
import com.google.android.material.bottomsheet.BottomSheetDialog
import ir.hamsaa.persiandatepicker.Listener
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog
import ir.hamsaa.persiandatepicker.util.PersianCalendar

fun Fragment.showSnackbar(snackbarText: String, timeLength: Int) {

    activity?.let {

        val snackBar = Snackbar.make(
            it.findViewById<View>(android.R.id.content),
            snackbarText,
            timeLength
        )
        val mView = snackBar.view
        mView.background = ResourcesCompat.getDrawable(resources, R.drawable.bg_rounded_corner, null)
        val mTextView = mView.findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
        mTextView.setTextColor(Color.parseColor("#FFFFFF"))
        mTextView.setTypeface(mTextView.typeface, Typeface.BOLD)
        mTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        snackBar.view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackBar.show()

    }
}

/**
 * Triggers a snackbar message when the value contained by snackbarTaskMessageLiveEvent is modified.
 */
fun Fragment.setupSnackbar(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<Int>>,
    timeLength: Int
) {
    snackbarEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let { res ->
            context?.let { showSnackbar(it.getString(res), timeLength) }
        }
    })
}

fun Fragment.setupSnackbarMessage(
    lifecycleOwner: LifecycleOwner,
    snackbarEvent: LiveData<Event<String>>,
    timeLength: Int
) {
    snackbarEvent.observe(lifecycleOwner, Observer { event ->
        event.getContentIfNotHandled()?.let { text ->
            context?.let { showSnackbar(text, timeLength) }
        }
    })
}

/*
 * flag = InputMethodManager.SHOW_FORCED : Show
 * flag = InputMethodManager.HIDE_IMPLICIT_ONLY : Hide
 */
fun Fragment.handleSoftKeyboardVisibility(flag: Int) {
    val imm: InputMethodManager? =
        activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm!!.toggleSoftInput(flag, 0)

}

fun Fragment.addFragment(fragment: Fragment, frameId: Int) {
    childFragmentManager.inTransaction { add(frameId, fragment) }
}

fun Fragment.replaceFragment(fragment: Fragment, frameId: Int) {
    childFragmentManager.inTransaction { replace(frameId, fragment) }
}

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

fun Fragment.initToolbar(
    title: String?,
    txt_title: AppCompatTextView?,
    rpl_back: RippleView?
) {

    if (title != null && txt_title != null) {
        txt_title.text = title
    }
    rpl_back?.setOnRippleCompleteListener {
        requireActivity().onBackPressed()
    }

}

fun AppCompatEditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            if (!editable.isNullOrEmpty())
                afterTextChanged.invoke(editable.toString())
        }
    })
}

fun View.startRefreshAnimation(){

    val animation: Animation = RotateAnimation(
        0.0f, 360.0f,
        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
        0.5f
    )
    animation.repeatCount = -1
    animation.duration = 2000

    this.animation = animation
}

fun View.stopRefreshAnimation(){
    this.clearAnimation()
}

fun View.viewVisibility(visibility: Int){
    this.visibility = visibility
}

fun showDatePicker(activity: Context, txt: TextView){

    val picker = PersianDatePickerDialog(activity)
        .setPositiveButtonString("باشه")
        .setNegativeButton("بیخیال")
        .setTodayButton("امروز")
        .setTodayButtonVisible(true)
        .setMinYear(1300)
        .setMaxYear(PersianDatePickerDialog.THIS_YEAR)
        //.setInitDate(initDate)
        .setTitleColor(activity.resources.getColor(R.color.colorPrimary))
        .setActionTextColor(Color.GRAY)
        //.setTypeFace(android.R.attr.typeface)
        .setTitleType(PersianDatePickerDialog.WEEKDAY_DAY_MONTH_YEAR)
        .setShowInBottomSheet(true)
        .setListener(object : Listener {
            override fun onDateSelected(persianCalendar: PersianCalendar) {

                val year = persianCalendar.persianYear
                val month = if (persianCalendar.persianMonth < 10) "0${persianCalendar.persianMonth}" else persianCalendar.persianMonth
                val day = if (persianCalendar.persianDay < 10) "0${persianCalendar.persianDay}" else persianCalendar.persianDay

                txt.text = "${year}/${month}/${day}"
            }

            override fun onDismissed() {}
        })

    picker.show()
}


fun dismissView(dialog: BottomSheetDialog?) {
    Handler(Looper.getMainLooper()).postDelayed({
        dialog?.dismiss()
    }, 200)
}
