package com.andishepardazansamandrail.common.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andishepardazansamandrail.common.R
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.model.thinker.SearchFilter
import kotlinx.android.synthetic.main.item_search.view.*

class SearchFilterAdapter(
    private val data: List<SearchFilter>,
    private val itemClickListener: OnItemClickListener
) :
    RecyclerView.Adapter<SearchFilterAdapter.VH>() {

    private var rowIndex = 0

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(
            itemView: View,
            data: List<SearchFilter>,
            position: Int,
            clickListener: OnItemClickListener,
            searchFilterAdapter: SearchFilterAdapter
        ) {
            if (data[position].title.isNotEmpty())
                itemView.txt_title.text = data[position].title

            itemView.setOnClickListener{
                clickListener.onItemClicked(data[position])
                searchFilterAdapter.rowIndex = position
                searchFilterAdapter.notifyDataSetChanged()
            }

            if (searchFilterAdapter.rowIndex==position) {
                itemView.txt_title.setBackgroundResource(R.drawable.bg_rounded);
                itemView.txt_title.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorWhite))
            } else {
                itemView.txt_title.setBackgroundResource(R.drawable.bg_rounded_white);
                itemView.txt_title.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorPrimary))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_search,
            parent,
            false
        )
        return VH(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(holder.itemView, data, position, itemClickListener, this)
    }

    interface OnItemClickListener {

        fun onItemClicked(searchItem: SearchFilter)
    }
}