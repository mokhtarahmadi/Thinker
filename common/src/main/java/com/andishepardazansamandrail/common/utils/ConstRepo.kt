package com.andishepardazansamandrail.common.utils

object ConstRepo {

    // Base url
    const val BASE_URL = "http://37.32.24.162:9002/api/"

    // Apis
    const val LOGIN_URL = BASE_URL + "Account/login"
    const val DRIVER_INFO_URL = BASE_URL + "Driver"
    const val GET_BAR_URL = BASE_URL + "Driver/Bar/Status"
    const val REQUEST_BAR_URL = BASE_URL + "Driver/Bar/Request"
    const val ALL_BAR_REQUESTED_URL = BASE_URL + "Driver/Bar/Request"
    const val ACCEPT_BAR_URL = BASE_URL + "Driver/Bar/Accept"
    const val ARRIVED_LOADING_URL = BASE_URL + "Driver/Bar/ArrivedToLoadingPlace"
    const val ARRIVED_FINISH_URL = BASE_URL + "Driver/Bar/ArrivedToFinishPlace"
    const val CANCEL_BAR_URL = BASE_URL + "Driver/Bar"
    const val CANCEL_BAR_REQUESTED_URL = BASE_URL + "Driver/Bar/Request/Bar"
    const val ACTIVE_BAR_URL = BASE_URL + "Driver/Bar/Active"
    const val WAYBILL_BAR_URL = BASE_URL + "Driver/Bar/WayBill"
    const val FINISH_BAR_URL = BASE_URL + "Driver/Bar/Finish"
    const val LOADING_FINISHED_BAR_URL = BASE_URL + "Driver/Bar/LoadingFinished"
    const val BAR_LOCATOR = BASE_URL + "BarLocator"
    const val DRIVER_FINANCIAL = BASE_URL + "Driver/Financial"
    const val DRIVER_FINANCIAL_MASTER_DETAIL = BASE_URL + "Driver/FinancialMasterDetails"
    const val NAVY = BASE_URL + "Driver/Trail/Type"
    const val NAVY_TYPE = BASE_URL + "Driver/Trail/TrailId"
    const val GET_BAR_Trail_URL = BASE_URL + "Driver/Bar/Trail"
    const val EDIT_DRIVER_URL = BASE_URL + "Driver/Update"
    const val ASK_QUESTION_URL = BASE_URL + "AskQuestion/Insert"
    const val GET_MESSAGES_URL = BASE_URL + "AskQuestion/GetMessagesByPersonId"
    const val GET_THINKER_MESSAGES_URL = BASE_URL + "Message"
    const val SHOW_MESSAGE_URL = BASE_URL + "Driver/ShowMessage"


    //Apis assistant
    const val LOGIN_ASSISTANT_URL = BASE_URL + "Account/Assistant/login"
    const val GET_DRIVER_URL = BASE_URL + "Driver/GetAll"
    const val GET_SEMI_TRAILER_URL = BASE_URL + "SemiTrailer/GetAll"
    const val SET_DELIVERY_URL = BASE_URL + "Delivery/SetDelivery"
    const val SET_CONTRACT_URL = BASE_URL + "Contract/SetContract"
    const val SET_DELIVERY_OF_AMOUNT_URL = BASE_URL + "Contract/SetDeliveryOfAmount"
    const val GetSemiTrailersDelivered_URL = BASE_URL + "SemiTrailer/GetSemiTrailersDelivered"
    const val GetTechnicalSpecificationBySemiTrailer_URL = BASE_URL + "SemiTrailer/GetTechnicalSpecificationBySemiTrailer"

}