package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class SetUserTypeUseCase (private val repository: ThinkerRepository) {

    operator fun invoke(value : Int){
        repository.setUserType(value)
    }
}