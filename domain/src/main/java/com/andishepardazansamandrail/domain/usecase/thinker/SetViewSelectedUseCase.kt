package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class SetViewSelectedUseCase (private val repository: ThinkerRepository) {

    operator fun invoke(viewState : Int){
        return repository.setViewSelected(viewState)
    }
}