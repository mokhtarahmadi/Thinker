package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo

class ActiveCargoUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke( url:String): LiveData<Resource<Cargo>> {
        return Transformations.map(repository.fetchActiveCargoAsync(url)) {
            it?.apply {
                if (it.status == Resource.Status.SUCCESS)
                    it.data.let {
                        if (it==null){
                            repository.setBarId(0)
                            repository.setCargoStatus(0)
                        }else{
                            repository.setBarId(it.barId!!)
                            if (it.status==7)
                            repository.setCargoStatus(0)
                            else
                                repository.setCargoStatus(it.status!!)

                        }
                    }

            }
        }
    }
}