package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.CreateDriver
import com.andishepardazansamandrail.model.thinker.Driver

class EditDriverInfoUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke(url:String,driver : CreateDriver): LiveData<Resource<Boolean>> {
        return Transformations.map(repository.editDriverInfoAsync(url,driver)) {
            it
        }
    }
}