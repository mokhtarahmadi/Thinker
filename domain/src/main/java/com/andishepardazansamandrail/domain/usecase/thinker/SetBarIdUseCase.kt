package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class SetBarIdUseCase (private val repository: ThinkerRepository) {

    operator fun invoke(barId : Int){
        return repository.setBarId(barId)
    }
}