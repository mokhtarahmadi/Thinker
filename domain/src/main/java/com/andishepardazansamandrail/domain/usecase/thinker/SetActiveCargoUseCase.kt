package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class SetActiveCargoUseCase (private val repository: ThinkerRepository) {

    operator fun invoke(value : Int){
        return repository.setCargoStatus(value)
    }
}