package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Driver
import com.andishepardazansamandrail.model.thinker.Login

class GetDriverInfoUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke( url:String): LiveData<Resource<Driver>> {
        return Transformations.map(repository.driverInfo(url)) {
            it
        }
    }
}