package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.CargoInput

class FinishCargoUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke(url:String, cargoInput: CargoInput): LiveData<Resource<Boolean>> {
        return Transformations.map(repository.finishCargoAsync(url, cargoInput)) {
            it?.apply {
                it.data?.apply {
                    if (this)
                        repository.setCargoStatus(0)
                }
            }
        }
    }
}