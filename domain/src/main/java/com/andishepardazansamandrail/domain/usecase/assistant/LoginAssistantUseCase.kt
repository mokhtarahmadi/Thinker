package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.LoginInputAssistant
import com.andishepardazansamandrail.model.thinker.Login

class LoginAssistantUseCase(private val repository: AssistantRepository) {

    suspend operator fun invoke(url:String, loginA : LoginInputAssistant): LiveData<Resource<Login>> {
        return Transformations.map(repository.fetchUserLoginAsync(url,loginA)) {
            it?.apply {
                it.data?.let { login ->

                    repository.setToken(login.token)
                    repository.setTokenType("bearer")
                    repository.setUser(login.user)
                    repository.setIdentityNumber(loginA.email)
                    repository.setSmartNumber(loginA.password)
                }

            }
        }
    }
}