package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.Driver

class GetDriversUseCase(private val repository: AssistantRepository) {

    suspend operator fun invoke(url:String): LiveData<Resource<List<Driver>>> {
        return Transformations.map(repository.getDriversAsync(url)) {
            it
        }
    }
}