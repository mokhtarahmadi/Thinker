package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.SetDelivery

class SetDeliveryUseCase(private val repository: AssistantRepository) {

    suspend operator fun invoke(url:String, setDelivery: SetDelivery): LiveData<Resource<Int>> {
        return Transformations.map(repository.setDeliveryAsync(url,setDelivery)) {
            it
        }
    }
}