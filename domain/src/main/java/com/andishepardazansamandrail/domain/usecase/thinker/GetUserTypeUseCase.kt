package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class GetUserTypeUseCase(private val repository: ThinkerRepository) {

    operator fun invoke(): Int{
        return repository.getUserType()
    }
}