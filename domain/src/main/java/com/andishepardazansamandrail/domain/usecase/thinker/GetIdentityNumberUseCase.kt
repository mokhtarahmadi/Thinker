package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class GetIdentityNumberUseCase(private val repository: ThinkerRepository) {

    operator fun invoke(): String?{
        return repository.getIdentityNumber()
    }
}