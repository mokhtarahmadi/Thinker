package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Navy

class GetNavyTypeUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke(url:String): LiveData<Resource<List<Navy>>> {
        return Transformations.map(repository.fetchNavyAsync(url)) {
            it
        }
    }
}