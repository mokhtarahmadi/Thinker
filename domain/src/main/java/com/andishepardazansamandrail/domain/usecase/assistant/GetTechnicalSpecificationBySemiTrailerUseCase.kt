package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.SemiTrailerOfSpec

class GetTechnicalSpecificationBySemiTrailerUseCase(private val repository: AssistantRepository) {

    suspend operator fun invoke(url:String, trailerCode : String): LiveData<Resource<SemiTrailerOfSpec>> {
        return Transformations.map(repository.getTechnicalSpecificationBySemiTrailer(url,trailerCode)) {
            it
        }
    }
}