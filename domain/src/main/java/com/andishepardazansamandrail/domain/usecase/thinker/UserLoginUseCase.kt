package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Login

class UserLoginUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke( url:String, userName: String,  password: String): LiveData<Resource<Login>> {
        return Transformations.map(repository.userLogin(url, userName, password)) {
            it?.apply {
                it.data?.let { login ->

                    repository.setToken(login.token)
                    repository.setTokenType("bearer")
                    repository.setUser(login.user)
                    repository.setIdentityNumber(userName)
                    repository.setSmartNumber(password)
                }

            }
        }
    }
}