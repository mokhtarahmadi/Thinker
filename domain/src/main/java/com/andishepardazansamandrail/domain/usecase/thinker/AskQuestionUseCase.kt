package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.AskQuestion
import com.andishepardazansamandrail.model.thinker.CargoInput

class AskQuestionUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke(url:String, askQuestion: AskQuestion): LiveData<Resource<Int>> {
        return Transformations.map(repository.askQuestionAsync(url, askQuestion)) {
            it
        }
    }
}