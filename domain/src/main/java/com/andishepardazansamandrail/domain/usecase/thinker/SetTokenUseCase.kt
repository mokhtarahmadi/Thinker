package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class SetTokenUseCase (private val repository: ThinkerRepository) {

    operator fun invoke(){
        repository.setTokenType("")
        return repository.setToken("")
    }
}