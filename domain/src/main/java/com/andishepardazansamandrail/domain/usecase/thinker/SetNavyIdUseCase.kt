package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class SetNavyIdUseCase (private val repository: ThinkerRepository) {

    operator fun invoke(navyId : Int){
        return repository.setNavyId(navyId)
    }
}