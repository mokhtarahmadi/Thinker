package com.andishepardazansamandrail.domain.usecase.assistant

import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository

class GetUserUseCase(private val repository: AssistantRepository) {

    operator fun invoke(): String{
        return repository.getUser()!!
    }
}