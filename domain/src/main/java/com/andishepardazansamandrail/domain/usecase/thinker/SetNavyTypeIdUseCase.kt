package com.andishepardazansamandrail.domain.usecase.thinker

import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository

class SetNavyTypeIdUseCase (private val repository: ThinkerRepository) {

    operator fun invoke(navyTypeId : Int){
        return repository.setNavyTypeId(navyTypeId)
    }
}