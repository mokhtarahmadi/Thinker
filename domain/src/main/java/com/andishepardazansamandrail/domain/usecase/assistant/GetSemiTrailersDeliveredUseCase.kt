package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.SearchSemiTrailer
import com.andishepardazansamandrail.model.assistant.SemiTrailersDelivered

class GetSemiTrailersDeliveredUseCase(private val repository: AssistantRepository) {

    suspend operator fun invoke(url:String,searchSemiTrailer: SearchSemiTrailer): LiveData<Resource<List<SemiTrailersDelivered>>> {
        return Transformations.map(repository.getSemiTrailersDeliveredAsync(url,searchSemiTrailer)) {
            it
        }
    }
}