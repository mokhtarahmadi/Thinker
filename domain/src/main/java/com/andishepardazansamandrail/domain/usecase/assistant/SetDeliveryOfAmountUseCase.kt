package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.SetDeliveryOfAmount

class SetDeliveryOfAmountUseCase(private val repository: AssistantRepository) {

    suspend operator fun invoke(url:String, setDeliveryOfAmounts: List<SetDeliveryOfAmount>): LiveData<Resource<Unit>> {
        return Transformations.map(repository.setDeliveryOfAmount(url,setDeliveryOfAmounts)) {
            it
        }
    }
}