package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.SemiTrailer

class GetSemiTrailersUseCase(private val repository: AssistantRepository) {

    suspend operator fun invoke(url:String): LiveData<Resource<List<SemiTrailer>>> {
        return Transformations.map(repository.getSemiTrailersAsync(url)) {
            it
        }
    }
}