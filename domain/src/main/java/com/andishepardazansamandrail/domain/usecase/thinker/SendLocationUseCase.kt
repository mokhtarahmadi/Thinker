package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.BarLocator

class SendLocationUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke( url:String,barLocator: BarLocator): LiveData<Resource<Boolean>> {
        return Transformations.map(repository.barLocatorAsync(url, barLocator)) {
            it
        }
    }
}