package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.Driver

class GetCargoesUseCase(private val repository: ThinkerRepository) {

    suspend operator fun invoke( url:String): LiveData<Resource<List<Cargo>>> {
        return Transformations.map(repository.fetchCargoAsync(url)) {
            it
        }
    }
}