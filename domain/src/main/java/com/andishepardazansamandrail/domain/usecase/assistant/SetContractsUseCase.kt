package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.SetContract

class SetContractsUseCase(private val repository: AssistantRepository) {

    suspend operator fun invoke(url:String, setContracts: List<SetContract>): LiveData<Resource<Unit>> {
        return Transformations.map(repository.setContractAsync(url,setContracts)) {
            it
        }
    }
}