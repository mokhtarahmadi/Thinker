package com.andishepardazansamandrail.domain.usecase.assistant

import androidx.lifecycle.LiveData
import com.andishepardazansamandrail.domain.repositories.assistant.AssistantRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.Percent

class GetPercentsUseCase(private val repository: AssistantRepository) {
    fun invoke(): LiveData<Resource<List<Percent>>> =  repository.getPercentsItems()
}