package com.andishepardazansamandrail.domain.usecase.thinker

import androidx.lifecycle.LiveData
import com.andishepardazansamandrail.domain.repositories.thinker.ThinkerRepository
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Degree

class GetDegreeUseCase(private val repository: ThinkerRepository) {
    fun invoke(): LiveData<Resource<List<Degree>>> =  repository.getDegrees()
}