package com.andishepardazansamandrail.domain.repositories.thinker

import androidx.lifecycle.LiveData
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.*

interface ThinkerRepository {

    suspend fun userLogin(url :String ,identityNumber: String, driverSmartCode: String): LiveData<Resource<Login>>
    suspend fun driverInfo(url :String): LiveData<Resource<Driver>>
    suspend fun fetchCargoAsync(url :String): LiveData<Resource<List<Cargo>>>
    suspend fun fetchCargoRequestedAsync(url :String): LiveData<Resource<List<Cargo>>>
    suspend fun fetchDriverFinancialAsync(url :String): LiveData<Resource<List<Financial>>>
    suspend fun fetchDriverFinancialMasterDetailAsync(url :String): LiveData<Resource<FinancialMasterDetail>>
    suspend fun fetchActiveCargoAsync(url :String): LiveData<Resource<Cargo>>
    suspend fun requestCargoAsync(url :String, cargoInput: CargoInput): LiveData<Resource<Boolean>>
    suspend fun acceptCargoAsync(url :String, cargoInput: CargoInput): LiveData<Resource<Boolean>>
    suspend fun arrivedToLoadingPlace(url :String, cargoInput: CargoInput): LiveData<Resource<Boolean>>
    suspend fun arrivedToFinishPlace(url :String, cargoInput: CargoInput): LiveData<Resource<Boolean>>
    suspend fun cancelCargoAsync(url :String): LiveData<Resource<Boolean>>
    suspend fun cancelCargoRequestedAsync(url :String): LiveData<Resource<Boolean>>
    suspend fun finishCargoAsync(url :String, cargoInput: CargoInput): LiveData<Resource<Boolean>>
    suspend fun loadingFinishedAsync(url :String, cargoInput: CargoInput): LiveData<Resource<Boolean>>
    suspend fun barLocatorAsync(url: String, barLocator: BarLocator): LiveData<Resource<Boolean>>
    suspend fun editDriverInfoAsync( url: String, driver: CreateDriver): LiveData<Resource<Boolean>>
    suspend fun fetchNavyAsync(url :String): LiveData<Resource<List<Navy>>>
    suspend fun fetchNavyTypeAsync(url: String): LiveData<Resource<List<Navy>>>
    suspend fun fetchCargoesAsync( url: String, cargoOfTrailInput: CargoOfTrailInput): LiveData<Resource<List<Cargo>>>
    suspend fun askQuestionAsync(url: String, body: AskQuestion) : LiveData<Resource<Int>>
    suspend fun fetchMessagesAsync(url: String) : LiveData<Resource<List<Message>>>
    suspend fun fetchThinkerMessages(url: String) : LiveData<Resource<List<ThinkerMessage>>>
    suspend fun showMessage(url: String, showMessage: ShowMessage): LiveData<Resource<Boolean>>
    fun setUser(user : String)
    fun getUser(): String?
    fun setToken(token: String?)
    fun getToken(): String?
    fun setTokenType(tokenType: String)
    fun getTokenType(): String?
    fun setCargoStatus(status: Int)
    fun getCargoStatus(): Int?
    fun setBarId(barId: Int)
    fun getBarId() : Int?
    fun setIdentityNumber(identityNumber: String)
    fun getIdentityNumber(): String?
    fun setSmartNumber(smartNumber: String)
    fun getSmartNumber(): String?
    fun setNavyId(navyId: Int)
    fun getNavyId() : Int?
    fun setNavyTypeId(navyTypeId: Int)
    fun getNavyTypeId() : Int?
    fun getViewSelected(): Int
    fun setViewSelected(viewState: Int)
    fun getDegrees(): LiveData<Resource<List<Degree>>>
    fun getUserType(): Int
    fun setUserType(value: Int)
}