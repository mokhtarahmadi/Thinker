package com.andishepardazansamandrail.domain.repositories.assistant

import androidx.lifecycle.LiveData
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.*
import com.andishepardazansamandrail.model.thinker.Login


interface AssistantRepository {
    fun setUser(user : String)
    fun getUser(): String?
    fun setToken(token: String?)
    fun getToken(): String?
    fun setTokenType(tokenType: String)
    fun getTokenType(): String?
    fun setUserType(value: Int)
    fun getUserType(): Int?
    fun setIdentityNumber(identityNumber: String)
    fun getIdentityNumber(): String?
    fun setSmartNumber(smartNumber: String)
    fun getSmartNumber(): String?
    suspend fun fetchUserLoginAsync( url: String, body: LoginInputAssistant): LiveData<Resource<Login>>
    fun getPercentsItems(): LiveData<Resource<List<Percent>>>
    suspend fun setDeliveryAsync( url: String, setDelivery: SetDelivery) : LiveData<Resource<Int>>
    suspend fun setContractAsync( url: String, setContracts: List<SetContract>) : LiveData<Resource<Unit>>
    suspend fun getDriversAsync( url: String) : LiveData<Resource<List<Driver>>>
    suspend fun setDeliveryOfAmount( url: String, setDeliveryOfAmounts: List<SetDeliveryOfAmount>): LiveData<Resource<Unit>>
    suspend fun getTechnicalSpecificationBySemiTrailer( url: String , trailerCode : String) : LiveData<Resource<SemiTrailerOfSpec>>
    suspend fun getSemiTrailersAsync( url: String) : LiveData<Resource<List<SemiTrailer>>>
    suspend fun getSemiTrailerDeliveriesAsync( url: String,  searchSemiTrailer : SearchSemiTrailer) : LiveData<Resource<List<SemiTrailerDelivery>>>
    suspend fun getSemiTrailerDeliveriesAsync( url: String) : LiveData<Resource<List<SemiTrailerDelivery>>>
    suspend fun getSemiTrailersDeliveredAsync( url: String,searchSemiTrailer: SearchSemiTrailer) : LiveData<Resource<List<SemiTrailersDelivered>>>
}