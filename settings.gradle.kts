include(
    ":app", ":common", ":domain", ":data:remote", ":data:model", ":data:local", ":data:repository",
    ":features:thinker","features:assistant", ":navigation")