package com.andishepardazansamandrail.app.ui.cargoReserve

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemCargoReserveBinding
import com.andishepardazansamandrail.model.thinker.Cargo
import saman.zamani.persiandate.PersianDate
import java.text.SimpleDateFormat
import java.util.*

class CargoReserveViewHolder(parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemCargoReserveBinding.bind(parent)

    fun bindTo(cargo: Cargo, viewModel: CargoReserveViewModel, position: Int) {
        binding.cargo = cargo
        binding.viewModel = viewModel
        cargo?.apply {
            persianAppointmentDate?.let {
               // binding.tvDate.text = "${it}  ساعت ${persianAppointmentTime} } "
            }

        }
    }


}