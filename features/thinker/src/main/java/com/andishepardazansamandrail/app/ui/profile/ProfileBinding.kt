package com.andishepardazansamandrail.app.ui.profile

import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.Driver

object ProfileBinding {


    @BindingAdapter("bind:nameText")
    @JvmStatic fun setNameText(textView: TextView, driver : Driver?) {
        driver?.apply {
            textView.text = "${driver.firstName} ${driver.lastName}"

        }
    }

    @BindingAdapter("bind:phoneText")
    @JvmStatic fun setPhoneText(textView: TextView, driver : Driver?) {
      driver?.apply {
         // textView.text = "${driver.}"

      }
    }
}