package com.andishepardazansamandrail.app.ui.cargoHistory

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemCargoHistoryBinding
import com.andishepardazansamandrail.app.databinding.ItemCargoReserveBinding
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveViewModel
import com.andishepardazansamandrail.model.thinker.Cargo
import kotlinx.android.synthetic.main.fragment_cargo_detail.*
import saman.zamani.persiandate.PersianDate
import java.text.SimpleDateFormat
import java.util.*

class CargoHistoryViewHolder (parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemCargoHistoryBinding.bind(parent)

    fun bindTo(cargo: Cargo, viewModel: CargoHistoryViewModel, position: Int) {
        binding.cargo = cargo
        binding.viewModel = viewModel

        cargo?.apply {
            persianAppointmentDate?.let {
//                if (it != "") {
//                    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
//                    var date: Date?  = sdf.parse( it)
//                    val pdate = PersianDate(date)
                    binding.tvDate.text = persianAppointmentDate
               // }
            }
        }

    }


}