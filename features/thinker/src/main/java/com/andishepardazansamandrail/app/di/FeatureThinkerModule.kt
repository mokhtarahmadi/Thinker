package com.andishepardazansamandrail.app.di

import com.andishepardazansamandrail.app.ui.cargoDetail.CargoDetailViewModel
import com.andishepardazansamandrail.app.ui.cargoHistory.CargoHistoryViewModel
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveViewModel
import com.andishepardazansamandrail.app.ui.cargoSalon.CargoSalonViewModel
import com.andishepardazansamandrail.app.ui.cargoWorkflow.CargoWorkFlowViewModel
import com.andishepardazansamandrail.app.ui.currentCorgo.CurrentCargoViewModel
import com.andishepardazansamandrail.app.ui.financial.FinancialViewModel
import com.andishepardazansamandrail.app.ui.financial.detail.FinancialDetailViewModel
import com.andishepardazansamandrail.app.ui.freight.FreightViewModel
import com.andishepardazansamandrail.app.ui.home.HomeViewModel
import com.andishepardazansamandrail.app.ui.login.LoginViewModel
import com.andishepardazansamandrail.app.ui.main.MainViewModel
import com.andishepardazansamandrail.app.ui.map.MapViewModel
import com.andishepardazansamandrail.app.ui.mess.MessViewModel
import com.andishepardazansamandrail.app.ui.messages.MessageViewModel
import com.andishepardazansamandrail.app.ui.myGood.MyGoodViewModel
import com.andishepardazansamandrail.app.ui.navy.ChangeNavyViewModel
import com.andishepardazansamandrail.app.ui.profile.ProfileViewModel
import com.andishepardazansamandrail.app.ui.selectNavy.NavyViewModel
import com.andishepardazansamandrail.app.ui.selectUserType.SelectUserTypeViewModel
import com.andishepardazansamandrail.app.ui.splash.SplashViewModel
import com.andishepardazansamandrail.app.ui.suggestedCargo.SuggestedCargoViewModel
import com.andishepardazansamandrail.app.ui.support.SupportViewModel
import com.andishepardazansamandrail.app.ui.thinkerMessages.ThinkerMessageViewModel
import com.andishepardazansamandrail.app.ui.userInfo.UserInfoViewModel
import com.andishepardazansamandrail.app.ui.validation.ValidationViewModel
import com.andishepardazansamandrail.domain.usecase.thinker.*
import com.andishepardazansamandrail.model.thinker.Cargo
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureThinkerModule = module {
    factory { UserLoginUseCase(get()) }
    factory { GetTokenUseCase(get()) }
    factory { GetDriverInfoUseCase(get()) }
    factory { GetCargoesUseCase(get()) }
    factory { AcceptCargoUseCase(get()) }
    factory { CancelCargoUseCase(get()) }
    factory { RequestCargoUseCase(get()) }
    factory { ActiveCargoUseCase(get()) }
    factory { FinishCargoUseCase(get()) }
    factory { GetCargoesRequestedUseCase(get()) }
    factory { CancelCargoRequestedUseCase(get()) }
    factory { LoadingFinishedUseCase(get()) }
    factory { SetActiveCargoUseCase(get()) }
    factory { SetTokenUseCase(get()) }
    factory { GetCargoStatusUseCase(get()) }
    factory { SendLocationUseCase(get()) }
    factory { GetBarIdUseCase(get()) }
    factory { SetBarIdUseCase(get()) }
    factory { SetNavyIdUseCase(get()) }
    factory { SetNavyTypeIdUseCase(get()) }
    factory { GetNavyIdUseCase(get()) }
    factory { GetNavyTypeIdUseCase(get()) }
    factory { GetDriverFinancialUserCase(get()) }
    factory { GetDriverFinancialMasterDetailUseCase(get()) }
    factory { GetNavyTypeUseCase(get()) }
    factory { GetNavyUseCase(get()) }
    factory { GetCargoesOfTrailUseCase(get()) }
    factory { GetViewSelectedUseCase(get()) }
    factory { SetViewSelectedUseCase(get()) }
    factory { EditDriverInfoUseCase(get()) }
    factory { GetDegreeUseCase(get()) }
    factory { GetUserTypeUseCase(get()) }
    factory { SetUserTypeUseCase(get()) }
    factory { GetIdentityNumberUseCase(get()) }
    factory { AskQuestionUseCase(get()) }
    factory { GetMessagesUseCase(get()) }
    factory { ArrivedToLoadingPlaceUseCase(get()) }
    factory { ArrivedToFinishPlaceUseCase(get()) }
    factory { ShowedMessageUseCase(get()) }
    factory { GetThinkerMessagesUseCase(get()) }
    viewModel { SplashViewModel(get(),get(),get(),get(),get(),get()) }
    viewModel { LoginViewModel(get(),get(),get()) }
    viewModel { ValidationViewModel(get(),get(),get()) }
    viewModel { FinancialViewModel(get(),get(),get(),get()) }
    viewModel { FinancialDetailViewModel(get(),get(),get(),get()) }
    viewModel { MainViewModel(get(),get(),get(),get(),get()) }
    viewModel { MapViewModel() }
    viewModel { SupportViewModel() }
    viewModel { ProfileViewModel(get(),get() ,get(),get(),get()) }
    viewModel { HomeViewModel()}
    viewModel { SuggestedCargoViewModel() }
    viewModel { CargoSalonViewModel(get(),get(),get(),get()) }
    viewModel { FreightViewModel() }
    viewModel { (cargo : Cargo)->CargoDetailViewModel(cargo,get(),get(),get()) }
    viewModel { CurrentCargoViewModel(get(),get(),get(),get(),get(),get(),get(),get(),get(),get()) }
    viewModel { CargoHistoryViewModel(get(),get(),get()) }
    viewModel { UserInfoViewModel(get(),get(),get(),get()) }
    viewModel { MyGoodViewModel() }
    viewModel { CargoReserveViewModel(get(),get(),get()) }
    viewModel { MessageViewModel(get(),get(),get()) }
    viewModel { ThinkerMessageViewModel(get(),get(),get(),get()) }
    viewModel { CargoWorkFlowViewModel() }
    viewModel { SelectUserTypeViewModel() }
    viewModel { MessViewModel() }
    viewModel { ChangeNavyViewModel(get(),get(),get(),get(),get()) }
    viewModel { (type : Int) -> NavyViewModel(type , get(),get(),get(),get(),get()) }
}
