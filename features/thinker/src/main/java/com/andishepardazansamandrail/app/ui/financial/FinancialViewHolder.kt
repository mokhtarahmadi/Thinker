package com.andishepardazansamandrail.app.ui.financial

import android.Manifest
import android.app.Activity
import android.os.Build
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.databinding.ItemFinancialBinding
import com.andishepardazansamandrail.app.ui.financial.detail.FinancialDetailViewModel
import com.andishepardazansamandrail.model.thinker.FinancialDetail
import com.devlomi.record_view.OnRecordListener
import com.devlomi.record_view.RecordPermissionHandler
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit
import java.io.InputStream


class FinancialViewHolder(parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemFinancialBinding.bind(parent)
    var audioRecorder = AudioRecorder()
    var recordFile: File? = null
    fun bindTo(financial : FinancialDetail, viewModel: FinancialDetailViewModel, position: Int, fragmentActivity: FragmentActivity) {
        binding.financial = financial
        binding.viewModel = viewModel

        binding.recordButton.setRecordView(binding.recordView)
        binding.recordButton.isListenForRecord = true



        //Cancel Bounds is when the Slide To Cancel text gets before the timer . default is 8
        binding.recordView.cancelBounds = 8f
        binding.recordView.setCustomSounds(R.raw.record_start, R.raw.record_finished, 0)
        binding.recordView.setRecordPermissionHandler( RecordPermissionHandler {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return@RecordPermissionHandler true
            }
            val recordPermissionAvailable = ContextCompat.checkSelfPermission(
                fragmentActivity,
                Manifest.permission.RECORD_AUDIO
            ) == PermissionChecker.PERMISSION_GRANTED
            if (recordPermissionAvailable) {
                return@RecordPermissionHandler true
            }
            ActivityCompat.requestPermissions(
                fragmentActivity, arrayOf(Manifest.permission.RECORD_AUDIO),
                0
            )
            false
        })
        binding.recordView.setOnRecordListener(object : OnRecordListener {
            override fun onStart() {
                val path = fragmentActivity.filesDir
                recordFile = File(path, UUID.randomUUID().toString() + ".mp3")
                try {
                    audioRecorder.start(recordFile!!.path)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                Log.d("RecordView", "onStart")
            }

            override fun onCancel() {
                stopRecording(true)
                audioRecorder.destroyMediaRecorder()
                Log.d("RecordView", "onCancel")
            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onFinish(recordTime: Long, limitReached: Boolean) {

                stopRecording(false)
                val time = getHumanTimeText(recordTime)
                Log.d("RecordView", "onFinish Limit Reached? $recordTime")
                //Log.d("RecordTime", time)

                val finput: InputStream = FileInputStream(recordFile)
                val imageBytes = ByteArray((recordFile?.length() as Long).toInt())
                finput.read(imageBytes, 0, imageBytes.size)
                finput.close()

                val imageStr: String = Base64.getEncoder().encodeToString(imageBytes)
                var length =(recordFile?.length() as Long).toInt()
                viewModel.askQuestion(financial.financialId,imageStr,recordFile?.name!!,".mp3",length)
                audioRecorder.destroyMediaRecorder()

            }



            override fun onLessThanSecond() {
                stopRecording(true)
                audioRecorder.destroyMediaRecorder()
                Log.d("RecordView", "onLessThanSecond")
            }

        })


    }
    private fun stopRecording(deleteFile: Boolean) {
        audioRecorder.stop()
        if (recordFile != null && deleteFile) {
            recordFile?.delete()
        }
    }


    private fun getHumanTimeText(milliseconds: Long): String? {
        return String.format(
            "%02d:%02d",
            TimeUnit.MILLISECONDS.toMinutes(milliseconds),
            TimeUnit.MILLISECONDS.toSeconds(milliseconds) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds))
        )
    }


}