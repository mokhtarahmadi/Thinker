package com.andishepardazansamandrail.app.ui.cargoHistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.andishepardazansamandrail.app.databinding.FragmentCargoHistoryBinding
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveAdapter
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.viewVisibility
import kotlinx.android.synthetic.main.loading_central.*
import org.koin.android.viewmodel.ext.android.viewModel

class CargoHistoryFragment :BaseFragment(){
    private val viewModel: CargoHistoryViewModel by viewModel()
    private lateinit var binding: FragmentCargoHistoryBinding
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCargoHistoryBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRecyclerView()

        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })
    }

    private fun configureRecyclerView() {
        binding.cargoRv.adapter = CargoHistoryAdaptor(viewModel)
    }
}