package com.andishepardazansamandrail.app.ui.currentCorgo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.ui.main.MainFragmentDirections
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.*
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.AcceptCargoInput
import com.andishepardazansamandrail.model.thinker.BarLocator
import com.andishepardazansamandrail.model.thinker.CargoInput
import com.andishepardazansamandrail.model.thinker.Cargo
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CurrentCargoViewModel (
    private val activeCargoUseCase: ActiveCargoUseCase,
    private val cancelCargoUseCase: CancelCargoUseCase,
    private val  acceptCargoUseCase: AcceptCargoUseCase,
    private val  loadingFinishedUseCase: LoadingFinishedUseCase,
    private val  finishCargoUseCase: FinishCargoUseCase,
    private val  getCargoStatusUseCase: GetCargoStatusUseCase,
    private val  sendLocationUseCase: SendLocationUseCase,
    private val  arrivedToLoadingPlaceUseCase: ArrivedToLoadingPlaceUseCase,
    private val  arrivedToFinishPlaceUseCase: ArrivedToFinishPlaceUseCase,
    private val dispatchers: AppDispatchers
): BaseViewModel(){

    // FOR DATA
    private val _cargo1 = MediatorLiveData<Resource<Cargo?>>()
    val cargo1: LiveData<Resource<Cargo?>> get() = _cargo1
    private var cargo1Source: LiveData<Resource<Cargo>> = MutableLiveData()

    // FOR DATA
    private val _cancelCargo = MediatorLiveData<Resource<Boolean>>()
    val cancelCargo: LiveData<Resource<Boolean>> get() = _cancelCargo
    private var _cancelCargoSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _arrivedLoading = MediatorLiveData<Resource<Boolean>>()
    val arrivedLoading: LiveData<Resource<Boolean>> get() = _arrivedLoading
    private var _arrivedLoadingSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _arrivedFinish = MediatorLiveData<Resource<Boolean>>()
    val arrivedFinish: LiveData<Resource<Boolean>> get() = _arrivedFinish
    private var _arrivedFinishSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _isAccept = MediatorLiveData<Resource<Boolean>>()
    val isAccept: LiveData<Resource<Boolean>> get() = _isAccept
    private var isAcceptSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _isLocation = MediatorLiveData<Resource<Boolean>>()
    val isLocation: LiveData<Resource<Boolean>> get() = _isLocation
    private var isLocationSource: LiveData<Resource<Boolean>> = MutableLiveData()


    private val _loadingFinished = MediatorLiveData<Resource<Boolean>>()
    val loadingFinished: LiveData<Resource<Boolean>> get() = _loadingFinished
    private var loadingFinishedSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _cargoFinished = MediatorLiveData<Resource<Boolean>>()
    val cargoFinished: LiveData<Resource<Boolean>> get() = _cargoFinished
    private var cargoFinishedSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation


    private val _isAccept1 = MutableLiveData<Boolean>()
    val isAccept1: LiveData<Boolean> get() = _isAccept1



    init {
        cargoes()
    }



    private val _cargoStatus = MutableLiveData<Int>()
    val cargoStatus: LiveData<Int> get() = _cargoStatus

    fun getCargoStatus() : Int  {

        return getCargoStatusUseCase.invoke()!!
    }

     fun cargoes() =
        viewModelScope.launch(dispatchers.main) {
            _cargo1.removeSource(cargo1Source)
            withContext(dispatchers.io) {
                cargo1Source = activeCargoUseCase.invoke(
                    ConstRepo.ACTIVE_BAR_URL
                )
            }
            _cargo1.addSource(cargo1Source) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cargo1.value = it

                        }
                      //  _loadingAnimation.value = false
                    }

                    // Loading state
                 //   Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                      //  _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

    fun onClickRefresh() = cargoes()

    fun onClickAccept(barId : Int) {
        acceptCargo(barId)
    }
//    fun onClickItem() {
//
//        _isAccept1.value = true
//
//    }
    private fun acceptCargo(barId : Int) =
        viewModelScope.launch(dispatchers.main) {
            _isAccept.removeSource(isAcceptSource)
            withContext(dispatchers.io) {
                isAcceptSource = acceptCargoUseCase.invoke(
                    ConstRepo.ACCEPT_BAR_URL,
                    CargoInput(barId)
                )
            }
            _isAccept.addSource(isAcceptSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _isAccept.value = it
                        }
                    }

                    // Loading state
                   // Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                      //  _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

    private fun arrivedLoadingPlace(barId : Int) =
        viewModelScope.launch(dispatchers.main) {
            _arrivedLoading.removeSource(_arrivedLoadingSource)
            withContext(dispatchers.io) {
                _arrivedLoadingSource = acceptCargoUseCase.invoke(
                    ConstRepo.ARRIVED_LOADING_URL,
                    CargoInput(barId)
                )
            }
            _arrivedLoading.addSource(_arrivedLoadingSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _arrivedLoading.value = it
                        }
                    }

                    // Loading state
                    // Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        //  _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

    private fun arrivedFinishPlace(barId : Int) =
        viewModelScope.launch(dispatchers.main) {
            _arrivedFinish.removeSource(_arrivedFinishSource)
            withContext(dispatchers.io) {
                _arrivedFinishSource = acceptCargoUseCase.invoke(
                    ConstRepo.ARRIVED_FINISH_URL,
                    CargoInput(barId)
                )
            }
            _arrivedFinish.addSource(_arrivedFinishSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _arrivedFinish.value = it
                        }
                    }

                    // Loading state
                    // Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        //  _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }
    private fun cancelCargo(cargoInput : CargoInput) =
        viewModelScope.launch(dispatchers.main) {
            _cancelCargo.removeSource(_cancelCargoSource)
            withContext(dispatchers.io) {
                _cancelCargoSource = cancelCargoUseCase.invoke(ConstRepo.CANCEL_BAR_URL + "/ ${cargoInput.barId}")
            }
            _cancelCargo.addSource(_cancelCargoSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cancelCargo.value = it

                        }
                      //  _loadingAnimation.value = false
                    }

                  //  // Loading state
                   // Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                       // _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }


    private fun sendLocation(locator: BarLocator) =
        viewModelScope.launch(dispatchers.main) {
            _isLocation.removeSource(isLocationSource)
            withContext(dispatchers.io) {
                isLocationSource = sendLocationUseCase.invoke(ConstRepo.BAR_LOCATOR , locator)
            }
            _isLocation.addSource(isLocationSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _isLocation.value = it


                        }
                        //  _loadingAnimation.value = false
                    }

                    //  // Loading state
                    // Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        // _loadingAnimation.value = false
                       // _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }


    private fun loadingFinished(cargoInput : CargoInput) =
        viewModelScope.launch(dispatchers.main) {
            _loadingFinished.removeSource(loadingFinishedSource)
            withContext(dispatchers.io) {
                loadingFinishedSource = loadingFinishedUseCase.invoke(ConstRepo.LOADING_FINISHED_BAR_URL ,cargoInput)
            }
            _loadingFinished.addSource(loadingFinishedSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _loadingFinished.value = it

                        }
                      //  _loadingAnimation.value = false
                    }

                    // Loading state
                   // Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                      //  _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }


    private fun finishedCargo(cargoInput : CargoInput) =
        viewModelScope.launch(dispatchers.main) {
            _cargoFinished.removeSource(cargoFinishedSource)
            withContext(dispatchers.io) {
                cargoFinishedSource = finishCargoUseCase.invoke(ConstRepo.FINISH_BAR_URL ,cargoInput)
            }
            _cargoFinished.addSource(cargoFinishedSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cargoFinished.value = it
                        }
                        _loadingAnimation.value = false
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

    fun onClickCancelCargo(barId : Int) {
        cancelCargo(CargoInput(barId))
    }
    fun onClickFinishCargo(barId : Int) {
        finishedCargo(CargoInput(barId))
    }
    fun onClickLoadingFinished(barId : Int) = loadingFinished(CargoInput(barId))
    fun onClickLoadingPlace(barId : Int) = arrivedLoadingPlace(barId)
    fun onClickFinishPlace(barId : Int) = arrivedFinishPlace(barId)
    fun onClickSendLocator(barLocator: BarLocator) = sendLocation(barLocator)
    fun onClickItem(cargo: Cargo) = navigateToCargoDetail(cargo)
    fun navigateToCargoDetail(cargo: Cargo) {
        navigate(MainFragmentDirections.actionMainFragmentToCargoDetailFragment(cargo))
    }

}