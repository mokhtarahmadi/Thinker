package com.andishepardazansamandrail.app.ui.selectNavy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.databinding.FragmentNavy2Binding
import com.andishepardazansamandrail.app.ui.selectNavy.NavyList.NavyAdapter
import com.andishepardazansamandrail.app.ui.selectNavy.NavyTypeList.NavyTypeAdapter
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import kotlinx.android.synthetic.main.fragment_navy2.*
import kotlinx.coroutines.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class NavyFragment : BaseFragment() {

    private val args: NavyFragmentArgs by navArgs()

    private val viewModel: NavyViewModel by viewModel { parametersOf(args.type)}
    private lateinit var binding: FragmentNavy2Binding
    override fun getViewModel(): BaseViewModel = viewModel

    private var navyAdapter: NavyAdapter? = null
    private var navyTypeAdapter: NavyTypeAdapter? = null
    var navyDialog: BottomSheetDialog? = null
    var navyTypeDialog: BottomSheetDialog? = null
    private var navyText: String? = null
    private var navyTypeText: String? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentNavy2Binding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRecyclerView()

        spin_navi.setOnClickListener {
            navyDialog?.show()
        }

        spin_navi_type.setOnClickListener {
            if (!navyText.isNullOrEmpty())
                 navyTypeDialog?.show()
        }
        viewModel.navy.observe(viewLifecycleOwner, Observer {
            navyAdapter?.apply {
                it.data?.let { it1 ->
                    updateData(it1)
                    if (activity != null && isAdded)
                        setupNavyBottomSheetDialog()
                }
            }
        })

        viewModel.navyType.observe(viewLifecycleOwner, Observer {
            navyTypeAdapter?.apply {
                it.data?.let { it1 ->
                    updateData(it1)
                    if (activity != null && isAdded)
                        setupNavyTypeBottomSheetDialog()
                }
            }
        })

        viewModel.selectedNavy.observe(viewLifecycleOwner , Observer {
            navyDialog?.dismiss()
            it?.apply {
                navyText = it.name
                spin_navi.text = it.name
                viewModel.navyType(it.trailId)
            }

        })

        viewModel.selectedNavyType.observe(viewLifecycleOwner , Observer {
            navyTypeDialog?.dismiss()
            it?.apply {
                navyTypeText = it.name
                spin_navi_type.text = it.name
                viewModel.navyType(it.trailId)
            }

        })

        btn_next.setOnClickListener {
            if (!navyText.isNullOrEmpty() && !navyTypeText.isNullOrEmpty())
                viewModel.navigateToMain()
            else
                Toast.makeText(context,"ناوگان و نوع ناوگان را انتخاب کنید",Toast.LENGTH_LONG).show()
        }
    }

    private fun configureRecyclerView() {
        navyAdapter = NavyAdapter(viewModel)
        navyTypeAdapter = NavyTypeAdapter(viewModel)

        navyDialog = BottomSheetDialog(requireActivity())
        navyTypeDialog = BottomSheetDialog(requireActivity())

    }

    private fun setupNavyBottomSheetDialog() {

        val dialogView = layoutInflater.inflate(R.layout.bottom_sheet_recycler_view, null)
        navyDialog?.setContentView(dialogView)

        val rvItems: RecyclerView = dialogView.findViewById(R.id.rvItems)
        rvItems.adapter = navyAdapter

    }

    private fun setupNavyTypeBottomSheetDialog() {

        val dialogView = layoutInflater.inflate(R.layout.bottom_sheet_recycler_view, null)
        navyTypeDialog?.setContentView(dialogView)

        val rvItems: RecyclerView = dialogView.findViewById(R.id.rvItems)
        rvItems.adapter = navyTypeAdapter

    }
}