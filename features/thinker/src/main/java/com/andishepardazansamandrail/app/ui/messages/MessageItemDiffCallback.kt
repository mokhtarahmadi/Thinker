package com.andishepardazansamandrail.app.ui.messages

import androidx.recyclerview.widget.DiffUtil
import com.andishepardazansamandrail.model.thinker.Message

class MessageItemDiffCallback(private val oldList: List<Message>,
                              private val newList: List<Message>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return true
    }
}