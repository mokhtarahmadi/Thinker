package com.andishepardazansamandrail.app.ui.cargoSalon

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemCargoBinding
import com.andishepardazansamandrail.model.thinker.Cargo

class CargoSalonViewHolder (parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemCargoBinding.bind(parent)

    fun bindTo(cargo: Cargo, viewModel: CargoSalonViewModel, position: Int) {
        binding.cargo = cargo
        binding.viewModel = viewModel
        binding.root.setOnClickListener {
            viewModel.navigateToCargoDetail(cargo)
        }
    }


}