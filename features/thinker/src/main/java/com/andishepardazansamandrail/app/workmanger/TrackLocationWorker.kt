package com.andishepardazansamandrail.app.workmanger

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.domain.usecase.thinker.GetBarIdUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetCargoStatusUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.SendLocationUseCase
import com.andishepardazansamandrail.local.preferences.PreferenceHelper
import com.andishepardazansamandrail.model.thinker.BarLocator
import io.nlopez.smartlocation.SmartLocation
import kotlinx.coroutines.*
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.json.JSONObject
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.io.IOException
import kotlin.coroutines.CoroutineContext

class TrackLocationWorker(appContext: Context, workerParams: WorkerParameters): Worker(
    appContext,
    workerParams
)  , CoroutineScope , KoinComponent {

    private val sendLocationUseCase: SendLocationUseCase by inject()
    private val preferenceHelper: PreferenceHelper by inject()
    private val getBarIdUseCase: GetBarIdUseCase by inject()
    override fun doWork(): Result {
        return try {

                var loc2 = BarLocator(0,"","","")
                SmartLocation.with(applicationContext).location()/*.config(builder.build())*/.start { location ->

                        var barId : Int = getBarIdUseCase.invoke()!!

//                        var loc = BarLocator(
//                            barId, location.latitude.toString() + "," + location.longitude.toString(),
//                            location.latitude.toString(),
//                            location.longitude.toString()
//                        )

                    loc2.barId = barId
                    loc2.latLong = location.latitude.toString() + "," + location.longitude.toString()
                    loc2.latitude = location.latitude.toString()
                    loc2.longitude = location.longitude.toString()
//                        withContext(Dispatchers.IO) {
//                           sendLocationUseCase.invoke(ConstRepo.BAR_LOCATOR , loc)
//                        }

                }

            sendLocation(loc2)


            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }

     private fun sendLocation(barLocator: BarLocator){
        val JSON = MediaType.get("application/json; charset=utf-8")
        val jsonObject = JSONObject()
        var url  = "http://193.176.240.109:8403/api/BarLocator"
        jsonObject.put("barId", barLocator.barId)
        jsonObject.put("latLong", barLocator.latLong)
        jsonObject.put("latitude", barLocator.latitude)
        jsonObject.put("longitude", barLocator.longitude)
        var reqbody: RequestBody = RequestBody.create(JSON, jsonObject.toString())
        val client = OkHttpClient()
        val request = Request.Builder()
            .url(url)
            .method("POST", reqbody)
            .addHeader(
                "Authorization",
                "bearer " + preferenceHelper.getToken()
            )
            .addHeader("Content-Type", "application/json; charset=UTF-8")
            .build()

        try {
            val response = client.newCall(request).execute()
            if (response.code() === 200) {
                // Get response
                val jsonData = response.body()?.string()



            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }



override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + Job()
}