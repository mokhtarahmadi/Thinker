package com.andishepardazansamandrail.app.ui.support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andishepardazansamandrail.app.databinding.FragmentSupportBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SupportFragment : BaseFragment() {
    private val viewModel: SupportViewModel by  viewModel()
    private lateinit var binding: FragmentSupportBinding

    companion object {
        fun newInstance() = SupportFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSupportBinding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }
    override fun getViewModel(): BaseViewModel = viewModel
}