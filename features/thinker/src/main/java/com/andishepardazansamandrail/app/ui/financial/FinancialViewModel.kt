package com.andishepardazansamandrail.app.ui.financial

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.GetCargoesUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetDriverFinancialMasterDetailUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetDriverFinancialUserCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetIdentityNumberUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.CargoInput
import com.andishepardazansamandrail.model.thinker.Financial
import com.andishepardazansamandrail.model.thinker.FinancialDetails
import com.andishepardazansamandrail.model.thinker.FinancialMasterDetail
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FinancialViewModel (
    private val getDriverFinancialUserCase: GetDriverFinancialUserCase,
    private val getDriverFinancialMasterDetailUserCase: GetDriverFinancialMasterDetailUseCase,
    private val getIdentityNumberUseCase : GetIdentityNumberUseCase,
    private val dispatchers: AppDispatchers
): BaseViewModel(){

    // FOR DATA
    private val _financial = MediatorLiveData<Resource<List<Financial>>>()
    val financial: LiveData<Resource<List<Financial>>> get() = _financial
    private var financialSource: LiveData<Resource<List<Financial>>> = MutableLiveData()

    private val _financialMd = MediatorLiveData<Resource<FinancialMasterDetail>>()
    val financialMd: LiveData<Resource<FinancialMasterDetail>> get() = _financialMd
    private var financialMdSource: LiveData<Resource<FinancialMasterDetail>> = MutableLiveData()


    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation
    var identityNumber : String? = null

    init {
       // getDriverFinancialUserCase()
        getDriverFinancialMasterDetail()
      //  identityNumber = getIdentityNumberUseCase.invoke()

    }






    private fun getDriverFinancialUserCase() =
        viewModelScope.launch(dispatchers.main) {
            _financial.removeSource(financialSource)
            withContext(dispatchers.io) {
                financialSource = getDriverFinancialUserCase.invoke(ConstRepo.DRIVER_FINANCIAL+ "/Page/1/Count/200")
            }
            _financial.addSource(financialSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _financial.value = it

                        }
                        _loadingAnimation.value = false
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }



    private fun getDriverFinancialMasterDetail() =
        viewModelScope.launch(dispatchers.main) {
            _financialMd.removeSource(financialMdSource)
            withContext(dispatchers.io) {
                var url  =  "/page/1/count/200"
                financialMdSource = getDriverFinancialMasterDetailUserCase.invoke(ConstRepo.DRIVER_FINANCIAL_MASTER_DETAIL + url)
            }
            _financialMd.addSource(financialMdSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _financialMd.value = it

                        }
                        _loadingAnimation.value = false
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

    fun navigateToFinancialDetail(financialDetails: FinancialDetails) = navigate(FinancialFragmentDirections.actionFinancialFragmentFinancialDetailFragment(financialDetails))
}