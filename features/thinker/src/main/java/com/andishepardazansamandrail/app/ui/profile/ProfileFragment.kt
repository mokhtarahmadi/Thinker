package com.andishepardazansamandrail.app.ui.profile

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatTextView
import androidx.work.WorkManager
import com.andexert.library.RippleView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.databinding.FragmentProfileBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.dismissView
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.coroutines.*
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.coroutines.CoroutineContext
import kotlin.system.exitProcess

class ProfileFragment : BaseFragment() , CoroutineScope {
    private val viewModel: ProfileViewModel by  viewModel()
    private lateinit var  binding: FragmentProfileBinding

    companion object {
        fun newInstance() = ProfileFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return  binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


//        cons_finance.setOnClickListener {
//            Toast.makeText(context,"بزودی",Toast.LENGTH_LONG).show()
//        }

//        cons_navy.setOnClickListener {
//            Toast.makeText(context,"بزودی",Toast.LENGTH_LONG).show()
//        }

//        cons_messages.setOnClickListener {
//            Toast.makeText(context,"بزودی",Toast.LENGTH_LONG).show()
//        }


        cons_introduction_score.setOnClickListener {
            Toast.makeText(context,"بزودی",Toast.LENGTH_LONG).show()
        }

        cons_setting.setOnClickListener {
            Toast.makeText(context,"بزودی",Toast.LENGTH_LONG).show()
        }

        cons_exit.setOnClickListener {
            WorkManager.getInstance().cancelAllWorkByTag("JobSendLocation");
            viewModel.setToken()
            setupBottomSheetDialog()
        }
    }
    override fun getViewModel(): BaseViewModel = viewModel


    private fun setupBottomSheetDialog() {

        val dialogView = layoutInflater.inflate(R.layout.bottom_sheet_logout, null)
        val dialog: BottomSheetDialog? = BottomSheetDialog(requireActivity())
        dialog?.setContentView(dialogView)
        dialog?.show()

        val txtTitle: AppCompatTextView = dialogView.findViewById(R.id.txt_title)
        val txtDes: AppCompatTextView = dialogView.findViewById(R.id.txt_description)
        val rplYes: RippleView = dialogView.findViewById(R.id.rpl_yes)
        val rplNo: RippleView = dialogView.findViewById(R.id.rpl_no)

        val chbDeleteData: AppCompatCheckBox = dialogView.findViewById(R.id.chbDeleteData)

        txtTitle.text = "خروج از حساب کاربری"
        txtDes.text = "مطمئنید که می خواهید از برنامه خارج شوید؟"

        rplYes.setOnRippleCompleteListener {
            dismissView(dialog)
            logout(chbDeleteData.isChecked)
        }
        rplNo.setOnRippleCompleteListener {
            dismissView(dialog)
        }

    }


    private fun logout(clearData: Boolean) {

        val mPrefs: SharedPreferences = requireContext().getSharedPreferences("Thinker_pref", Context.MODE_PRIVATE)
        mPrefs.edit().clear().apply()
        if (clearData)
            launch {
                withContext(Dispatchers.IO) {
                    exitProcess(-1)
                    //   viewModel.clearDB()
                }
            }
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()
}