package com.andishepardazansamandrail.app.ui.navy.NavyList

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemNavy2Binding
import com.andishepardazansamandrail.app.ui.navy.ChangeNavyViewModel
import com.andishepardazansamandrail.model.thinker.Navy

class NavyViewHolder(parent: View): RecyclerView.ViewHolder(parent) {

    private val binding = ItemNavy2Binding.bind(parent)

    fun bindTo(item: Navy, viewModel: ChangeNavyViewModel, position: Int) {
        binding.navyItem = item
        binding.viewModel = viewModel
        binding.navyPosition = position
    }
}