package com.andishepardazansamandrail.app.ui.navy.navyTypeList

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemNavyType2Binding
import com.andishepardazansamandrail.app.ui.navy.ChangeNavyViewModel
import com.andishepardazansamandrail.model.thinker.Navy

class NavyTypeViewHolder(parent: View): RecyclerView.ViewHolder(parent) {

    private val binding = ItemNavyType2Binding.bind(parent)

    fun bindTo(item: Navy, viewModel: ChangeNavyViewModel, position: Int) {
        binding.navyItem = item
        binding.viewModel = viewModel
        binding.navyPosition = position
    }
}