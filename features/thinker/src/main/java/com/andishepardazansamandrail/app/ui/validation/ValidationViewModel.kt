package com.andishepardazansamandrail.app.ui.validation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.ActiveCargoUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetDriverInfoUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.Driver
import com.andishepardazansamandrail.model.thinker.Login
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ValidationViewModel(private val getDriverInfoUseCase : GetDriverInfoUseCase,
                          private val activeCargoUseCase: ActiveCargoUseCase,
                          private val dispatchers: AppDispatchers) : BaseViewModel() {

    // FOR DATA
    private val _driverInfo = MediatorLiveData<Resource<Driver>>()
    val driverInfo: LiveData<Resource<Driver>> get() = _driverInfo
    private var driverInfoSource: LiveData<Resource<Driver>> = MutableLiveData()

    private val _cargo = MediatorLiveData<Resource<Cargo>>()
    val cargo: LiveData<Resource<Cargo>> get() = _cargo
    private var cargoSource: LiveData<Resource<Cargo>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    init {
        driverInfo()
    }
    private fun driverInfo() = viewModelScope.launch(dispatchers.main) {
            _driverInfo.removeSource(driverInfoSource)
            withContext(dispatchers.io) {
                driverInfoSource = getDriverInfoUseCase.invoke(
                    ConstRepo.DRIVER_INFO_URL
                )
            }
            _driverInfo.addSource(driverInfoSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        _driverInfo.value = it
                        _loadingAnimation.value = false

                        cargo()

                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
     }

    fun cargo() =
        viewModelScope.launch(dispatchers.main) {
            _cargo.removeSource(cargoSource)
            withContext(dispatchers.io) {
                cargoSource = activeCargoUseCase.invoke(
                    ConstRepo.ACTIVE_BAR_URL
                )
            }
            _cargo.addSource(cargoSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cargo.value = it

                        }
                    }

                    // Loading state

                    // Error state
                    Resource.Status.ERROR -> {
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }
    fun navigateToMain() = navigate(ValidationFragmentDirections.actionValidationFragmentToNavyFragment(0))
}