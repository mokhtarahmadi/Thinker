package com.andishepardazansamandrail.app.ui.messages

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.model.thinker.Message

class MessageAdapter (private val viewModel: MessageViewModel): RecyclerView.Adapter<MessageViewHolder>() {

    private val items: MutableList<Message> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = MessageViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_message,
            parent,
            false
        )
    )
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        if (!items[position].responseMessage.isNullOrEmpty() && !items[position].responseMessage.isNullOrBlank())
            holder.bindTo(items[position], viewModel,position)
    }

    fun updateData(messages: List<Message>) {

        val diffCallback = MessageItemDiffCallback(items, messages)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(messages.filter { x -> x.responseMessage!=null })

        //notifyDataSetChanged()
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(message: Message, position: Int){

        items[position] = message
        notifyItemChanged(position)
    }
}