package com.andishepardazansamandrail.app.ui.cargoSalon

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo


object CargoSalonBinding {
    @BindingAdapter("app:items")
    @JvmStatic fun setItems(recyclerView: RecyclerView, resource: Resource<List<Cargo>>?) {
        with(recyclerView.adapter as CargoSalonAdapter) {
            resource?.data?.let { updateData(it) }
        }

    }


    @JvmStatic @BindingAdapter("bind:setCargoesNoData")
    fun setCargoesNoData(view: LinearLayoutCompat, resource: Resource<List<Cargo>>?) {

        resource?.data?.let {
            if (it.isNotEmpty())
                view.visibility = View.GONE
            else
                view.visibility = View.VISIBLE
        }

    }


    @BindingAdapter("bind:cargoPriceSalon")
    @JvmStatic fun setCargoPriceSalon(view: TextView, resource: Int) {
        view.text = "${resource} ریال "
    }

    @BindingAdapter("bind:setOriginSalon")
    @JvmStatic fun setOriginSalon(view: TextView, resource: Cargo) {
        view.text = "${resource.fromCity}  ${resource.sourceAddress}"
    }

    @BindingAdapter("bind:setDestSalon")
    @JvmStatic fun setDestSalon(view: TextView, resource: Cargo) {
        view.text = "${resource.toCity}  ${resource.deliveryAddress}"
    }
}