package com.andishepardazansamandrail.app.ui.selectUserType

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andishepardazansamandrail.app.databinding.FragmentSelectUserTypeBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SelectUserTypeFragment : BaseFragment()  {
    private val viewModel: SelectUserTypeViewModel by  viewModel()
    private lateinit var binding: FragmentSelectUserTypeBinding



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSelectUserTypeBinding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }
    override fun getViewModel(): BaseViewModel = viewModel
}