package com.andishepardazansamandrail.app.ui.financial

import androidx.recyclerview.widget.DiffUtil
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.Financial
import com.andishepardazansamandrail.model.thinker.FinancialDetail

class FinancialItemDiffCallback (private val oldList: List<FinancialDetail>,
                                 private val newList: List<FinancialDetail>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return true
    }
}