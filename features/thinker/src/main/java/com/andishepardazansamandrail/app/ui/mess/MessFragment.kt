package com.andishepardazansamandrail.app.ui.mess

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.databinding.FragmentMessBinding
import com.andishepardazansamandrail.app.ui.cargoHistory.CargoHistoryFragment
import com.andishepardazansamandrail.app.ui.messages.MessageFragment
import com.andishepardazansamandrail.app.ui.thinkerMessages.ThinkerMessageFragment
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.dueeeke.tablayout.listener.OnTabSelectListener
import kotlinx.android.synthetic.main.fragment_mess.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.ArrayList

class MessFragment : BaseFragment() {
    private val viewModel: MessViewModel by viewModel()
    private lateinit var binding: FragmentMessBinding

    override fun getViewModel(): BaseViewModel = viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMessBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupTabLayout()
    }


    private fun setupTabLayout() {

        val mTitles = arrayOf(
            resources.getString(R.string.title_message_currency),
            resources.getString(R.string.title_message_thinker),
        )

        val mFragments = ArrayList<Fragment>()
        mFragments.add(MessageFragment())
        mFragments.add(ThinkerMessageFragment())

        val mAdapter = MyPagerAdapter(childFragmentManager, mFragments, mTitles)
        viewPager.adapter = mAdapter

        //tabLayouts.setupWithViewPager(viewPager)

        tabLayouts.setTabData(mTitles)
        tabLayouts.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                viewPager.currentItem = position
            }

            override fun onTabReselect(position: Int) {}
        })

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                tabLayouts.currentTab = position
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        viewPager.currentItem = 0
    }

    class MyPagerAdapter(
        private val fm: FragmentManager,
        private val mFragments: ArrayList<Fragment>,
        private val mTitles: Array<String>
    ) : FragmentPagerAdapter(fm) {

        override fun getCount(): Int = mFragments.size

        override fun getPageTitle(position: Int): CharSequence {
            return mTitles[position]
        }

        override fun getItem(position: Int): Fragment {
            return mFragments[position]
        }
    }
}