package com.andishepardazansamandrail.app.ui.financial

import android.Manifest
import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.lifecycle.Observer
import com.andishepardazansamandrail.app.databinding.FragmentCargoReserveBinding
import com.andishepardazansamandrail.app.databinding.FragmentFinancialBinding
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveAdapter
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveViewModel
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.viewVisibility
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.FinancialDetails
import com.andishepardazansamandrail.model.thinker.FinancialMasterDetail
import com.devlomi.record_view.RecordPermissionHandler
import kotlinx.android.synthetic.main.fragment_financial.*
import kotlinx.android.synthetic.main.loading_central.*
import org.koin.android.viewmodel.ext.android.viewModel

class FinancialFragment : BaseFragment(){
    private val viewModel: FinancialViewModel by viewModel()
    private lateinit var binding: FragmentFinancialBinding
    override fun getViewModel(): BaseViewModel = viewModel

    var financialMasterDetail : FinancialMasterDetail? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentFinancialBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        permission()

        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })

        viewModel.financialMd.observe(viewLifecycleOwner, Observer {
            if (it.status == Resource.Status.SUCCESS )
                it?.data?.apply {
                    financialMasterDetail = this
                }
        })

        rl_details.setOnClickListener {
            if (financialMasterDetail==null)
                Toast.makeText(context,"جزئیاتی ندارد",Toast.LENGTH_LONG).show()
            financialMasterDetail?.let {
                var financialDetailss =  FinancialDetails()
                financialMasterDetail!!.financialDetails?.forEach { x ->  financialDetailss.add(x) }
                viewModel.navigateToFinancialDetail(financialDetailss)
            }
        }
    }

    private fun permission() {

    }
}