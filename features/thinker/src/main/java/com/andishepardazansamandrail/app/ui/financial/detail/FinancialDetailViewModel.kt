package com.andishepardazansamandrail.app.ui.financial.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.assistant.GetUserUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.AskQuestionUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetDriverFinancialUserCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.AskQuestion
import com.andishepardazansamandrail.model.thinker.Financial
import com.andishepardazansamandrail.model.thinker.FinancialMasterDetail
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FinancialDetailViewModel(private val getDriverFinancialUserCase: GetDriverFinancialUserCase,
                               private val dispatchers: AppDispatchers,
                               private val askQuestionUseCase: AskQuestionUseCase,
                               private val getUserUseCase: GetUserUseCase,
) : BaseViewModel() {

    private val _record = MediatorLiveData<Resource<Int>>()
    val record: LiveData<Resource<Int>> get() = _record
    private var recordSource: LiveData<Resource<Int>> = MutableLiveData()

     fun askQuestion( financialId : Int, content : String,fileName: String, fileType: String , fileSize : Int) =
        viewModelScope.launch(dispatchers.main) {
            var personId = getUserUseCase.invoke().toInt()
            var askQuestion  = AskQuestion(0,personId,null,fileName,fileType,fileSize,null,null,null,content,financialId,null)

                _record.removeSource(recordSource)
            withContext(dispatchers.io) {
                recordSource = askQuestionUseCase.invoke(ConstRepo.ASK_QUESTION_URL,askQuestion)
            }
            _record.addSource(recordSource) {

                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _record.value = it

                        }
                    }

                    // Loading state

                    // Error state
                    Resource.Status.ERROR -> {
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }


}