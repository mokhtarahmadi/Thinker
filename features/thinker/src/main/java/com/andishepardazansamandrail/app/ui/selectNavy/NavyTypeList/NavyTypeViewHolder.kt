package com.andishepardazansamandrail.app.ui.selectNavy.NavyTypeList

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemNavyTypeBinding
import com.andishepardazansamandrail.app.ui.selectNavy.NavyViewModel
import com.andishepardazansamandrail.model.thinker.Navy

class NavyTypeViewHolder(parent: View): RecyclerView.ViewHolder(parent) {

    private val binding = ItemNavyTypeBinding.bind(parent)

    fun bindTo(item: Navy, viewModel: NavyViewModel, position: Int) {
        binding.navyItem = item
        binding.viewModel = viewModel
        binding.navyPosition = position
    }
}