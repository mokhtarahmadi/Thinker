package com.andishepardazansamandrail.app.ui.currentCorgo

import android.content.Intent
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.andishepardazansamandrail.app.databinding.FragmentCurrentCargoBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.viewVisibility
import com.andishepardazansamandrail.model.thinker.BarLocator
import com.andishepardazansamandrail.model.thinker.Cargo
import io.nlopez.smartlocation.SmartLocation
import kotlinx.android.synthetic.main.fragment_current_cargo.*
import kotlinx.android.synthetic.main.fragment_current_cargo.btn_apply_origin
import kotlinx.android.synthetic.main.fragment_current_cargo.stepper_1
import kotlinx.android.synthetic.main.fragment_current_cargo.stepper_2
import kotlinx.android.synthetic.main.fragment_current_cargo.stepper_3
import kotlinx.android.synthetic.main.fragment_current_cargo.stepper_4
import kotlinx.android.synthetic.main.loading_central.*
import kotlinx.coroutines.*
import moe.feng.common.stepperview.VerticalStepperItemView
import moe.feng.common.stepperview.VerticalStepperItemView.STATE_NORMAL
import moe.feng.common.stepperview.VerticalStepperItemView.STATE_SELECTED
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.coroutines.CoroutineContext


class CurrentCargoFragment :BaseFragment(), CoroutineScope {

    private val viewModel: CurrentCargoViewModel by viewModel()
    private lateinit var binding: FragmentCurrentCargoBinding
    private var cargo : Cargo? = null
    var location : Location?  = null
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCurrentCargoBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cargo?.apply {
        }

        viewModel.isAccept1.observe(viewLifecycleOwner, Observer {
            li_apply.viewVisibility(View.GONE)
            li_call.viewVisibility(View.VISIBLE)

        })
        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })


        viewModel.cancelCargo.observe(viewLifecycleOwner, Observer {

        })

        viewModel.cargo1.observe(viewLifecycleOwner, Observer {
            it?.data?.let { it ->
                if (it?.status != 0 || it?.status != 6) {
                    cst_content.viewVisibility(View.VISIBLE)
                    binding.cargo = it
                    cargo = it
                    chack()
                } else
                    cst_content.viewVisibility(View.GONE)

            }

        })

        stepper_1.setOnClickListener {
            if (stepper_1.state==0){
                stepper_1.state = STATE_SELECTED
            }else{
                stepper_1.state = STATE_NORMAL
            }

        }
        btn_call.setOnClickListener {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            var uri = "tel:" + "${cargo?.operatorPhonenumber}"
            dialIntent.data = Uri.parse(uri)
            startActivity(dialIntent)
        }

        viewModel.isAccept.observe(viewLifecycleOwner, Observer {

            it?.data.let {
                if (it!!)
                    viewModel.cargoes()

            }
        })

        viewModel.arrivedLoading.observe(viewLifecycleOwner, Observer {
            it?.data.let {
                if (it!!)
                    viewModel.cargoes()
            }
        })

        viewModel.arrivedFinish.observe(viewLifecycleOwner, Observer {
            it?.data.let {
                if (it!!)
                    viewModel.cargoes()
            }
        })

        viewModel.loadingFinished.observe(viewLifecycleOwner, Observer {
            it?.data.let {
                if (it!!)
                    viewModel.cargoes()

                try {
                    SmartLocation.with(context).location()/*.config(builder.build())*/
                        .start { location ->

                            var loc = BarLocator(
                                cargo?.barId!!,location.latitude.toString() + "," + location.longitude.toString(),
                                        location.latitude.toString(),
                                location.longitude.toString(),
                            )
                            viewModel.onClickSendLocator(loc)


                        }
                } catch (e: Exception) {

                }


            }

        })

        viewModel.isLocation.observe(viewLifecycleOwner, Observer {

        })

        btn_apply_origin.setOnClickListener {

            viewModel.onClickAccept(cargo?.barId!!)
        }


        btn_apply_unload.setOnClickListener {
            try {
                SmartLocation.with(context).location()/*.config(builder.build())*/.start { location ->

                            var loc = BarLocator(
                                cargo?.barId!!, location.latitude.toString() + location.longitude.toString(),
                                location.latitude.toString(),
                                location.longitude.toString(),

                            )
                            viewModel.onClickSendLocator(loc)



                }
            }catch (e: Exception){

            }


            viewModel.onClickFinishCargo(cargo?.barId!!)
        }


        viewModel.cargoFinished.observe(viewLifecycleOwner, Observer {
            it?.data.let { it ->
                if (it!!) {
                    cst_content.viewVisibility(View.GONE)
                    Toast.makeText(context, "عملیات با موفقیت انجام شد", Toast.LENGTH_LONG).show()

                }

            }
        })


    }

    fun chack(){

        when(viewModel.getCargoStatus()){
            0 -> {
                cst_content.viewVisibility(View.GONE)
            }
            1 -> state1()
            2 -> state2()
            3 -> state3()
            4 -> state5()
            5 -> state6()
            6 -> state7()
        }
    }
    private fun state1(){
        cst_content.viewVisibility(View.VISIBLE)
        stepper_1.state = STATE_SELECTED
        stepper_2.state = STATE_SELECTED
        li_apply.viewVisibility(View.VISIBLE)
        li_call.viewVisibility(View.GONE)
    }

    private fun state2(){
        cst_content.viewVisibility(View.VISIBLE)
        stepper_1.state = STATE_SELECTED
        stepper_2.state = STATE_SELECTED
        stepper_3.state = STATE_SELECTED
        li_apply.viewVisibility(View.GONE)
        li_call.viewVisibility(View.VISIBLE)
    }

    private fun state3(){
        cst_content.viewVisibility(View.VISIBLE)
        stepper_1.state = STATE_SELECTED
        stepper_2.state = STATE_SELECTED
        stepper_3.state = STATE_SELECTED
        stepper_4.state = STATE_SELECTED
        li_apply.viewVisibility(View.GONE)
        li_call.viewVisibility(View.VISIBLE)
        lv_way_bill.viewVisibility(View.VISIBLE)
    }


    private fun state4(){
        cst_content.viewVisibility(View.VISIBLE)
        stepper_1.state = STATE_SELECTED
        stepper_2.state = STATE_SELECTED
        stepper_3.state = STATE_SELECTED
        stepper_4.state = STATE_SELECTED
        li_apply.viewVisibility(View.GONE)
        li_call.viewVisibility(View.VISIBLE)
        lv_way_bill.viewVisibility(View.VISIBLE)

    }

    private fun state5(){
        cst_content.viewVisibility(View.VISIBLE)
        stepper_1.state = STATE_SELECTED
        stepper_2.state = STATE_SELECTED
        stepper_3.state = STATE_SELECTED
        stepper_4.state = STATE_SELECTED
        stepper_5.state = STATE_SELECTED
        li_apply.viewVisibility(View.GONE)
        tv_loading_place.viewVisibility(View.GONE)
        btn_apply_loading_place.viewVisibility(View.GONE)
        tv_bar_txt.viewVisibility(View.GONE)
        tv_bar_id.viewVisibility(View.GONE)
        li_call.viewVisibility(View.VISIBLE)
        li_apply.viewVisibility(View.GONE)
        li_call.viewVisibility(View.VISIBLE)
        lv_way_bill.viewVisibility(View.VISIBLE)
        tv_loading_finished.text = "اگر بارگیری انجام شده دکمه تایید را بزنید"
        btn_apply_loading.viewVisibility(View.VISIBLE)


    }

    private fun state6(){
        stepper_1.state = STATE_SELECTED
        stepper_2.state = STATE_SELECTED
        stepper_3.state = STATE_SELECTED
        stepper_4.state = STATE_SELECTED
        stepper_5.state = STATE_SELECTED
        stepper_6.state = STATE_SELECTED
        li_apply.viewVisibility(View.GONE)
        tv_loading_place.viewVisibility(View.GONE)
        btn_apply_loading_place.viewVisibility(View.GONE)
        tv_bar_txt.viewVisibility(View.GONE)
        tv_bar_id.viewVisibility(View.GONE)
        lv_way_bill.viewVisibility(View.VISIBLE)
        li_apply.viewVisibility(View.GONE)
        li_call.viewVisibility(View.VISIBLE)
        tv_loading_finished.text = "بارگیری با موفقیت انجام شده است"
        btn_apply_loading.viewVisibility(View.GONE)

    }

    private fun state7(){
        stepper_1.state = STATE_SELECTED
        stepper_2.state = STATE_SELECTED
        stepper_3.state = STATE_NORMAL
        stepper_4.state = STATE_SELECTED
        stepper_5.state = STATE_SELECTED
        stepper_6.state = STATE_SELECTED
        stepper_7.state = STATE_SELECTED
        li_apply.viewVisibility(View.GONE)
        tv_loading_place.viewVisibility(View.GONE)
        btn_apply_loading_place.viewVisibility(View.GONE)
        tv_bar_txt.viewVisibility(View.GONE)
        tv_bar_id.viewVisibility(View.GONE)
        lv_way_bill.viewVisibility(View.VISIBLE)
        tv_Unload_finished_place.viewVisibility(View.GONE)
        btn_apply_unload_place.viewVisibility(View.GONE)
        li_apply.viewVisibility(View.GONE)
        li_call.viewVisibility(View.VISIBLE)
        tv_loading_finished.text = "بارگیری با موفقیت انجام شده است"
        btn_apply_loading.viewVisibility(View.GONE)

    }

    private fun state8(){
        stepper_1.state = STATE_SELECTED
        stepper_2.state = STATE_SELECTED
        stepper_3.state = STATE_NORMAL
        stepper_4.state = STATE_SELECTED
        stepper_5.state = STATE_SELECTED
        lv_way_bill.viewVisibility(View.VISIBLE)
        stepper_6.state = VerticalStepperItemView.STATE_DONE
        li_apply.viewVisibility(View.GONE)
        li_call.viewVisibility(View.VISIBLE)
        tv_loading_finished.text = "بارگیری با موفقیت انجام شده است"
        btn_apply_loading.viewVisibility(View.GONE)
        tv_Unload_finished.text = "تخلیه با موفقیت انجام شده است"
        btn_apply_unload.viewVisibility(View.GONE)

    }


    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + Job()

}