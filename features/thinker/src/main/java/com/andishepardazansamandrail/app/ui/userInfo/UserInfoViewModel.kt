package com.andishepardazansamandrail.app.ui.userInfo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.EditDriverInfoUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetDegreeUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetDriverInfoUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.CreateDriver
import com.andishepardazansamandrail.model.thinker.Degree
import com.andishepardazansamandrail.model.thinker.Driver
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserInfoViewModel(private val getDriverInfoUseCase : GetDriverInfoUseCase,
                        private val dispatchers: AppDispatchers,
                        private val getDegreeUseCase: GetDegreeUseCase,
                        private val editDriverInfoUseCase: EditDriverInfoUseCase) : BaseViewModel() {
    // FOR DATA
    private val _driverInfo = MediatorLiveData<Resource<Driver>>()
    val driverInfo: LiveData<Resource<Driver>> get() = _driverInfo
    private var driverInfoSource: LiveData<Resource<Driver>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    private val _createDriver = MediatorLiveData<Resource<Boolean>>()
    val createDriver: LiveData<Resource<Boolean>> get() = _createDriver
    private var createDriverSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _degree = MediatorLiveData<List<Degree>>()
    val degree: LiveData<List<Degree>> get() = _degree



    private val _selectDegree = MediatorLiveData<Degree>()
    val selectDegree: LiveData<Degree> get() = _selectDegree

    init {
        getDegrees()
        driverInfo()
    }
    private fun driverInfo() = viewModelScope.launch(dispatchers.main) {
        _driverInfo.removeSource(driverInfoSource)
        withContext(dispatchers.io) {
            driverInfoSource = getDriverInfoUseCase.invoke(
                ConstRepo.DRIVER_INFO_URL
            )
        }
        _driverInfo.addSource(driverInfoSource) {


            when (it.status) {

                // Success state
                Resource.Status.SUCCESS -> {
                    _driverInfo.value = it
                  //  _loadingAnimation.value = false

                }

                // Loading state
               // Resource.Status.LOADING -> _loadingAnimation.value = true

                // Error state
                Resource.Status.ERROR -> {
                  //  _loadingAnimation.value = false
                    _snackbarError.value = Event(R.string.an_error_happened)

                }
            }
        }
    }

    fun getDegrees() {
        _degree.value = getDegreeUseCase.invoke().value?.data
    }
    fun editDriverInfo(driverInfo: CreateDriver){
        if (driverInfo.firstName.isNullOrEmpty()){
            _snackbarErrorServer.value = Event("نام الزامی میباشد")
        }
        else if (driverInfo.lastName.isNullOrEmpty()){
            _snackbarErrorServer.value = Event("نام خانوادگی الزامی میباشد")
        }
        else if (driverInfo.residenceCity.isNullOrEmpty() ){
            _snackbarErrorServer.value = Event("محل اقامت الزامی میباشد")
        }
        else if (driverInfo.phoneNumber.isNullOrEmpty() ){
            _snackbarErrorServer.value = Event("شماره همراه الزامی میباشد")
        }
        else if (driverInfo.educationCode == 0 ){
            _snackbarErrorServer.value = Event("تحصیلات الزامی میباشد")
        }else{
            editDriver(driverInfo)
        }
    }

    fun setDegree(degree : Degree){
        _selectDegree.value = degree;
    }
    fun editDriver(createDriver: CreateDriver)= viewModelScope.launch(dispatchers.main) {
        _createDriver.removeSource(createDriverSource)
        withContext(dispatchers.io) { createDriverSource =  editDriverInfoUseCase.invoke( ConstRepo.EDIT_DRIVER_URL,createDriver) }
        _createDriver.addSource(createDriverSource) {
            if (it.status == Resource.Status.ERROR){
                _loadingAnimation.value = false
            }
            else if (it.status == Resource.Status.SUCCESS){
                driverInfo()
                _loadingAnimation.value = false
                _createDriver.value = it
                _snackbarErrorServer.value = Event("اطلاعات با موفقیت ثبت شد")
            }
            else if (it.status == Resource.Status.LOADING)
                _loadingAnimation.value = true

        }
    }

}