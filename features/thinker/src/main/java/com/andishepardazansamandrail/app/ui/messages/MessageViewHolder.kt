package com.andishepardazansamandrail.app.ui.messages

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemMessageBinding
import com.andishepardazansamandrail.model.thinker.Message

class MessageViewHolder (parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemMessageBinding.bind(parent)

    fun bindTo(message: Message, viewModel: MessageViewModel, position: Int) {
        binding.message = message
        binding.viewModel = viewModel

    }


}