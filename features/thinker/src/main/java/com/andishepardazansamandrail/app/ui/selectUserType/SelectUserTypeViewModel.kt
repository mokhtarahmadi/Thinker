package com.andishepardazansamandrail.app.ui.selectUserType

import com.andishepardazansamandrail.common.base.BaseViewModel

class SelectUserTypeViewModel : BaseViewModel() {

    fun navigateToLoginAssistant() = navigate(SelectUserTypeFragmentDirections.actionSelectUserTypeToLoginAssistantFragment())

    fun navigateToLoginDriver() = navigate(SelectUserTypeFragmentDirections.actionSelectUserTypeToLoginDriverFragment())
}