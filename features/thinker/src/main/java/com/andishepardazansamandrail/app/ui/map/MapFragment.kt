package com.andishepardazansamandrail.app.ui.map

import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.databinding.FragmentMapBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class MapFragment : BaseFragment() , OnMapReadyCallback {
    private val viewModel: MapViewModel by viewModel()
    private lateinit var binding: FragmentMapBinding

    private lateinit var mMap: GoogleMap
    private var updateListener: LocationListener? = null
    private var locationManager: LocationManager? = null
    private lateinit var lastLocation: Location
    private var centerOfMap: LatLng? = null
    private var sourceMarker: Marker? = null
    private var destMarker: Marker? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val LOCATION_PERMISSION_REQUEST_CODE = 1
    private var isCheckedPoint = true


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)


        setupMap()


    }





    private fun setupMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        mapAction()

        mMap.setOnMarkerClickListener { marker ->
            Toast.makeText(activity, marker.title, Toast.LENGTH_SHORT).show()
            true
        }
    }

    private fun mapAction() {
        if (ActivityCompat.checkSelfPermission(activity!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
            if (location != null) {
                lastLocation = location
                updateLocation(location)
            }
        }
    }

    private fun addMapMarker(track : LatLng, origin: Drawable): Marker {
        var loc =  LatLng(track.latitude,track.longitude)
        val marker = mMap.addMarker(MarkerOptions().position(loc).icon(BitmapDescriptorFactory.fromBitmap(convertToBitmap(origin, dpToPx(10), dpToPx(10)))))
        //  marker.title = track.trackPdate.toString() + " " + track.trackTime
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(loc.latitude - 0.0005, loc.longitude - 0.0005), 16f))
        return marker
    }

    private fun convertToBitmap(drawable: Drawable, widthPixels: Int, heightPixels: Int): Bitmap {
        val mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(mutableBitmap)
        drawable.setBounds(0, 0, widthPixels, heightPixels)
        drawable.draw(canvas)
        return mutableBitmap
    }

    private fun dpToPx(dp: Int) = (dp * Resources.getSystem().displayMetrics.density).toInt()

    private fun updateLocation(loc: Location) {
        centerOfMap = LatLng(loc.latitude, loc.longitude)
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(centerOfMap, 16f))
//        fabCurrentLocation.isClickable = true
        mMap.setOnCameraIdleListener {
            //viewModel.getCurrentLocation(mMap.cameraPosition.target.latitude,mMap.cameraPosition.target.longitude)
        }

    }

    private fun polyLines(list : List<LatLng>){
        val options: PolylineOptions = PolylineOptions().width(5f).color(Color.BLUE).geodesic(true)
        for ( z in  list) {
            val point = LatLng(z.latitude,z.longitude)
            options.add(point)
        }
        mMap.addPolyline(options)
    }
    override fun getViewModel(): BaseViewModel = viewModel


}