package com.andishepardazansamandrail.app.ui.cargoDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.GetDriverInfoUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.RequestCargoUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.CargoInput
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.Driver
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CargoDetailViewModel(private val cargo: Cargo,
                           private val getDriverInfoUseCase : GetDriverInfoUseCase,
                           private val  requestCargoUseCase: RequestCargoUseCase, private val dispatchers: AppDispatchers) : BaseViewModel() {
    var cargoInfo = cargo

init {
    driverInfo()
}
    // FOR DATA
    private val _driverInfo = MediatorLiveData<Resource<Driver>>()
    val driverInfo: LiveData<Resource<Driver>> get() = _driverInfo
    private var driverInfoSource: LiveData<Resource<Driver>> = MutableLiveData()

    private val _isAccept = MediatorLiveData<Resource<Boolean>>()
    val isAccept: LiveData<Resource<Boolean>> get() = _isAccept
    private var isAcceptSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    fun onClickItem() = acceptCargo()
    private fun acceptCargo() =
        viewModelScope.launch(dispatchers.main) {
            _isAccept.removeSource(isAcceptSource)
            withContext(dispatchers.io) {
                isAcceptSource = requestCargoUseCase.invoke(
                    ConstRepo.REQUEST_BAR_URL,
                    CargoInput(cargo.barId!!)
                )
            }
            _isAccept.addSource(isAcceptSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _isAccept.value = it
                            navigateUp()
                        }

                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }


    private fun driverInfo() = viewModelScope.launch(dispatchers.main) {
        _driverInfo.removeSource(driverInfoSource)
        withContext(dispatchers.io) {
            driverInfoSource = getDriverInfoUseCase.invoke(
                ConstRepo.DRIVER_INFO_URL
            )
        }
        _driverInfo.addSource(driverInfoSource) {


            when (it.status) {

                // Success state
                Resource.Status.SUCCESS -> {
                    _driverInfo.value = it
                    //  _loadingAnimation.value = false

                }

                // Loading state
                // Resource.Status.LOADING -> _loadingAnimation.value = true

                // Error state
                Resource.Status.ERROR -> {
                    //  _loadingAnimation.value = false
                    _snackbarError.value = Event(R.string.an_error_happened)

                }
            }
        }
    }

    fun setError(message : String){
        _snackbarErrorServer.value = Event(message)
    }

}