package com.andishepardazansamandrail.app.ui.userInfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.databinding.FragmentSupportBinding
import com.andishepardazansamandrail.app.databinding.FragmentUserInfo2Binding
import com.andishepardazansamandrail.app.databinding.FragmentUserInfoBinding
import com.andishepardazansamandrail.app.ui.support.SupportFragment
import com.andishepardazansamandrail.app.ui.support.SupportViewModel
import com.andishepardazansamandrail.app.ui.userInfo.degree.DegreeAdapter
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.showDatePicker
import com.andishepardazansamandrail.common.extension.viewVisibility
import com.andishepardazansamandrail.model.thinker.CreateDriver
import com.andishepardazansamandrail.model.thinker.Degree
import com.andishepardazansamandrail.model.thinker.Driver
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_user_info2.*
import kotlinx.android.synthetic.main.loading_central.*
import kotlinx.android.synthetic.main.toolbar_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class UserInfoFragment : BaseFragment() {
    private val viewModel: UserInfoViewModel by  viewModel()
    private lateinit var binding: FragmentUserInfo2Binding
    private var createDriver: CreateDriver? = null
    private var degree: List<Degree>? = null

    var degreeDialog: BottomSheetDialog? = null
    private var degreeAdapter: DegreeAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUserInfo2Binding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createDriver = CreateDriver()
        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })

        btn_apply.setOnClickListener {
            createDriver?.firstName = ev_name.text.toString()
            createDriver?.lastName = ev_family.text.toString()
            createDriver?.residenceCity = ev_life.text.toString()
            createDriver?.phoneNumber = ev_phone.text.toString()
            if (!ev_birth.text.equals("تاریخ تولد"))
                 createDriver?.birthDay = ev_birth.text.toString()
            viewModel.editDriverInfo(createDriver!!)
        }

        viewModel.selectDegree.observe(viewLifecycleOwner, Observer {
            it?.apply {
                 ev_degree.text = it.name
                binding.evDegree.text = it.name
                createDriver?.educationCode = it.id
                degreeDialog?.dismiss()
            }
        })
        viewModel.degree.observe(viewLifecycleOwner , Observer {
            degreeAdapter?.apply {
                it?.let { it1 ->
                    updateData(it1)
                    if (activity != null && isAdded)
                        setupDegreeTypeBottomSheetDialog()
                }
            }

            degree = it
        })

        rl_birth.setOnClickListener {
            showDatePicker(requireActivity(), ev_birth)
        }

//        rl_life.setOnClickListener {
//            cityDialog?.show()
//        }

        rl_degree.setOnClickListener {
            degreeDialog?.show()
        }
        viewModel.driverInfo.observe(viewLifecycleOwner , Observer {
            it?.data?.apply {
                createDriver?.fatherName = it.data?.fatherName
                createDriver?.educationCode = it.data?.educationCode
                createDriver?.phoneNumber = it.data?.phoneNumber
                createDriver?.receiptPlace = it.data?.receiptPlace
                createDriver?.residenceCity = it.data?.residenceCity
                createDriver?.birthDay = it.data?.birthDay
                createDriver?.branch = it.data?.branch
                createDriver?.firstName = it.data?.firstName
                createDriver?.lastName = it.data?.lastName
                createDriver?.licenseNumber = it.data?.licenseNumber
                createDriver?.comment = it.data?.comment
                createDriver?.docNumber = it.data?.docNumber
                createDriver?.driverRegisterDate = it.data?.driverRegisterDate
                createDriver?.driverType = it.data?.driverType
                createDriver?.insuranceNumber = it.data?.insuranceNumber
                createDriver?.identityNumber = it.data?.identityNumber
                createDriver?.expireDate = it.data?.expireDate
                createDriver?.nationalCode = it.data?.nationalCode
                createDriver?.passportCode = it.data?.passportCode
                createDriver?.smartCardId = it.data?.smartCardId
                createDriver?.statusCode = it.data?.statusCode
            }


            it?.data?.firstName?.also {it ->  ev_name.setText(it)}
            it?.data?.lastName?.also { it -> ev_family.setText(it)  }
            it?.data?.phoneNumber?.also { ev_phone.setText(it)  }
            it?.data?.residenceCity?.also { ev_life.setText(it)  }
            it?.data?.birthDay?.also { ev_birth.text = it }
            it?.data?.educationCode?.let {
                ev_degree.text = degree?.find { x -> x.id == it }?.name
            }

        })


        viewModel.createDriver.observe(viewLifecycleOwner, Observer {

        })
        configureRecyclerView()
    }
    override fun getViewModel(): BaseViewModel = viewModel

    private fun configureRecyclerView() {
        degreeAdapter = DegreeAdapter(viewModel)
        degreeDialog = BottomSheetDialog(requireActivity())
    }

    private fun setupDegreeTypeBottomSheetDialog() {

        val dialogView = layoutInflater.inflate(R.layout.bottom_sheet_recycler_view, null)
        degreeDialog?.setContentView(dialogView)
        val rvItems: RecyclerView = dialogView.findViewById(R.id.rvItems)
        rvItems.adapter = degreeAdapter

    }
}