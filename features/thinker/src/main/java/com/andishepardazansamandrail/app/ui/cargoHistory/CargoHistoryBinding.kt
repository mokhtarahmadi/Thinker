package com.andishepardazansamandrail.app.ui.cargoHistory

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveAdapter
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.Financial

object CargoHistoryBinding {

    @BindingAdapter("bind:itemsCargoesHistory")
    @JvmStatic fun setItems(recyclerView: RecyclerView, resource: Resource<List<Cargo>>?) {
        with(recyclerView.adapter as CargoHistoryAdaptor) {
            resource?.data?.let { updateData(it) }
        }
    }

    @BindingAdapter("bind:setCargoesHistoryNoData")
    @JvmStatic fun setCargoesHistoryNoData(view: LinearLayoutCompat, resource: Resource<List<Cargo>>?) {

        resource?.data?.let {
            if (it.isNotEmpty())
                view.visibility = View.GONE
            else
                view.visibility = View.VISIBLE
        }

    }


    @BindingAdapter("bind:cargoPrice")
    @JvmStatic fun setCargoePrice(view: TextView, resource: String) {
         view.text = "${resource} ریال "
    }

}