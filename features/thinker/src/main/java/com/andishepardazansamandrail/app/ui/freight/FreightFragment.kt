package com.andishepardazansamandrail.app.ui.freight

import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class FreightFragment :BaseFragment() {
    private val viewModel: FreightViewModel by viewModel()

    override fun getViewModel(): BaseViewModel = viewModel
}