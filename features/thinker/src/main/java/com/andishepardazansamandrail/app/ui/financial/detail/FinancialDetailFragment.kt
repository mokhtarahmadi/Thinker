package com.andishepardazansamandrail.app.ui.financial.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.andishepardazansamandrail.app.databinding.FragmentFinancialDetailBinding
import com.andishepardazansamandrail.app.ui.financial.FinancialAdepter
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.viewVisibility
import kotlinx.android.synthetic.main.loading_central.*
import org.koin.android.viewmodel.ext.android.viewModel

class FinancialDetailFragment : BaseFragment() {
    private val args: FinancialDetailFragmentArgs by navArgs()

    private val viewModel: FinancialDetailViewModel by viewModel()
    private lateinit var binding: FragmentFinancialDetailBinding
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentFinancialDetailBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        args.financialMD?.also {
            if (it.count() > 0)
                configureRecyclerView()
        }

        viewModel.record.observe(viewLifecycleOwner, Observer {

        })

//        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
//            if (it) {
//                loading.viewVisibility(View.VISIBLE)
//            } else {
//                loading.viewVisibility(View.GONE)
//            }
//        })
    }

    private fun configureRecyclerView() {
        var adapter = FinancialAdepter(viewModel,requireActivity())
        binding.financialRv.adapter = adapter
        adapter.updateData(args.financialMD)

    }
}