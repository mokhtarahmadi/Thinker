package com.andishepardazansamandrail.app.ui.cargoReserve

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.model.thinker.Cargo

class CargoReserveAdapter(private val viewModel: CargoReserveViewModel): RecyclerView.Adapter<CargoReserveViewHolder>() {

    private val items: MutableList<Cargo> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = CargoReserveViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_cargo_reserve,
            parent,
            false
        )
    )
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: CargoReserveViewHolder, position: Int)  = holder.bindTo(items[position], viewModel,position)

    fun updateData(cargoes: List<Cargo>) {

        val diffCallback = CargoReserveItemDiffCallback(items, cargoes)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(cargoes)

        //notifyDataSetChanged()
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(cargo: Cargo, position: Int){

        items[position] = cargo
        notifyItemChanged(position)
    }
}