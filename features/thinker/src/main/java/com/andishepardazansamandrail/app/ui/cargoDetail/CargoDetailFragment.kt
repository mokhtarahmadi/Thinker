package com.andishepardazansamandrail.app.ui.cargoDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.andishepardazansamandrail.app.databinding.FragmentCargoDetail2Binding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.model.thinker.Driver
import kotlinx.android.synthetic.main.fragment_cargo_detail2.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CargoDetailFragment : BaseFragment() {
    private val args: CargoDetailFragmentArgs by navArgs()

    private val viewModel: CargoDetailViewModel by  viewModel { parametersOf(args.cargo)}
    private lateinit var  binding: FragmentCargoDetail2Binding
    private  var  driver: Driver? = null




    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCargoDetail2Binding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return  binding.root

    }
    override fun getViewModel(): BaseViewModel = viewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
            if (it) {
                btn_apply.startAnimation()

            } else {
                btn_apply.revertAnimation()

            }
        })


        viewModel.isAccept.observe(viewLifecycleOwner, Observer {

        })

        viewModel.driverInfo.observe(viewLifecycleOwner, Observer {
            it?.data?.apply {
                driver = it.data
            }
        })
        btn_apply.setOnClickListener {
            driver?.apply {
                if (firstName.isNullOrEmpty() || lastName.isNullOrEmpty())
                    viewModel.setError("مشخصات کاربری را کامل کنید")
                else
                viewModel.onClickItem()

            }
        }

        args.cargo?.apply {
            var source = sourceAddress
            if (source==null)
                source = ""

            var dest = deliveryAddress
            if (dest==null)
                dest = ""
         //   tv_origin.text = "${fromCity} ، ${source}"
          //  tv_dest.text = "${toCity} ، ${dest}"

        }
    }

}