package com.andishepardazansamandrail.app.ui.financial

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.ui.financial.detail.FinancialDetailViewModel
import com.andishepardazansamandrail.model.thinker.Financial
import com.andishepardazansamandrail.model.thinker.FinancialDetail
import com.andishepardazansamandrail.model.thinker.FinancialMasterDetail

class FinancialAdepter(private val viewModel: FinancialDetailViewModel,private val fragmentActivity: FragmentActivity): RecyclerView.Adapter<FinancialViewHolder>() {

    private val items: MutableList<FinancialDetail> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = FinancialViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_financial,
            parent,
            false
        )
    )
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: FinancialViewHolder, position: Int)  = holder.bindTo(items[position], viewModel,position,fragmentActivity)

    fun updateData(financials: List<FinancialDetail>) {

        val diffCallback = FinancialItemDiffCallback(items, financials)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(financials)

        //notifyDataSetChanged()
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(financials: FinancialDetail, position: Int){

        items[position] = financials
        notifyItemChanged(position)
    }
}