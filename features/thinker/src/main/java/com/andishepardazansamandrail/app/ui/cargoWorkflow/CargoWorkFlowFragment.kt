package com.andishepardazansamandrail.app.ui.cargoWorkflow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.andishepardazansamandrail.app.databinding.FragmentCargoWorkflowBinding
import com.andishepardazansamandrail.app.databinding.FragmentProfileBinding
import com.andishepardazansamandrail.app.ui.profile.ProfileFragment
import com.andishepardazansamandrail.app.ui.profile.ProfileViewModel
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class CargoWorkFlowFragment: BaseFragment() {
    private val viewModel: CargoWorkFlowViewModel by  viewModel()
    private lateinit var  binding: FragmentCargoWorkflowBinding

    companion object {
        fun newInstance() = ProfileFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCargoWorkflowBinding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return  binding.root

    }
    override fun getViewModel(): BaseViewModel = viewModel
}