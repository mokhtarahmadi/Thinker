package com.andishepardazansamandrail.app.ui.cargoReserve

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.ui.main.MainFragmentDirections
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.CancelCargoRequestedUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetCargoesUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.CargoInput
import com.andishepardazansamandrail.model.thinker.Cargo
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CargoReserveViewModel(private val cargoesUseCase: GetCargoesUseCase,
                            private val cancelCargoUseCase: CancelCargoRequestedUseCase,
                            private val dispatchers: AppDispatchers
): BaseViewModel(){

    // FOR DATA
    private val _reserveCargoes = MediatorLiveData<Resource<List<Cargo>>>()
    val reserveCargoes: LiveData<Resource<List<Cargo>>> get() = _reserveCargoes
    private var reserveCargoesSource: LiveData<Resource<List<Cargo>>> = MutableLiveData()


    // FOR DATA
    private val _cancelCargo = MediatorLiveData<Resource<Boolean>>()
    val cancelCargo: LiveData<Resource<Boolean>> get() = _cancelCargo
    private var _cancelCargoSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation


    init {
        cargoes()
    }





    private fun cargoes() =
        viewModelScope.launch(dispatchers.main) {
            _reserveCargoes.removeSource(reserveCargoesSource)
            withContext(dispatchers.io) {
                reserveCargoesSource = cargoesUseCase.invoke(
                    ConstRepo.ALL_BAR_REQUESTED_URL
                )
            }
            _reserveCargoes.addSource(reserveCargoesSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _reserveCargoes.value = it

                        }
                        _loadingAnimation.value = false
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }



    private fun cancelCargo(cargoInput : CargoInput) =
        viewModelScope.launch(dispatchers.main) {
            _cancelCargo.removeSource(_cancelCargoSource)
            withContext(dispatchers.io) {
                _cancelCargoSource = cancelCargoUseCase.invoke(ConstRepo.CANCEL_BAR_REQUESTED_URL+ "/ ${cargoInput.barId}")
            }
            _cancelCargo.addSource(_cancelCargoSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cancelCargo.value = it

                            cargoes()

                        }
                        _loadingAnimation.value = false
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }
    fun onClickItem(cargo: Cargo) = navigateToCargoDetail(cargo)
    fun onClickCancelCargo(barId : Int) = cancelCargo(CargoInput(barId))
    fun navigateToCargoDetail(cargo: Cargo) {
        navigate(MainFragmentDirections.actionMainFragmentToCargoDetailFragment(cargo))
    }

}