package com.andishepardazansamandrail.app.location

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.domain.usecase.thinker.GetBarIdUseCase
import com.andishepardazansamandrail.local.preferences.PreferenceHelper
import com.andishepardazansamandrail.model.thinker.BarLocator
import com.google.android.gms.location.FusedLocationProviderClient
import io.nlopez.smartlocation.SmartLocation
import io.nlopez.smartlocation.location.config.LocationAccuracy
import io.nlopez.smartlocation.location.config.LocationParams
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import org.json.JSONObject
import org.koin.android.ext.android.inject
import java.io.IOException


class LocationService : Service() {
    private val mFusedLocationClient: FusedLocationProviderClient? = null


    private val NOTIFICATION_CHANNEL_ID = "my_notification_location"
    private val TAG = "LocationService"

    private val getBarIdUseCase: GetBarIdUseCase by inject()
    private val preferenceHelper: PreferenceHelper by inject()

    var barId : Int = getBarIdUseCase.invoke()!!
    override fun onCreate() {
        super.onCreate()
        isServiceStarted = true
        val builder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setOngoing(false)
                .setSmallIcon(R.drawable.slogo)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager: NotificationManager =
                getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            val notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_ID, NotificationManager.IMPORTANCE_LOW
            )
            notificationChannel.description = NOTIFICATION_CHANNEL_ID
            notificationChannel.setSound(null, null)
            notificationManager.createNotificationChannel(notificationChannel)
            startForeground(1, builder.build())
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        val builder = LocationParams.Builder()
            .setAccuracy(LocationAccuracy.HIGH)
            .setInterval(60000)
        SmartLocation.with(baseContext).location()
            .continuous()
            .config(builder.build()).start { location ->

            var barId : Int = getBarIdUseCase.invoke()!!

                        var loc = BarLocator(
                            barId,
                            location.latitude.toString() + "," + location.longitude.toString(),
                            location.latitude.toString(),
                            location.longitude.toString()
                        )

            loc?.let {
                AppExecutors.instance?.networkIO()?.execute {
                    sendLocation(it)
                }
            }

        }

//        LocationHelper().startListeningUserLocation(
//            baseContext, object : MyLocationListener {
//                override fun onLocationChanged(location: Location?) {
//                    mLocation = location
//                    var loc = BarLocator(
//                        barId, location?.latitude.toString() + "," + location?.longitude.toString(),
//                        location?.latitude.toString(),
//                        location?.longitude.toString()
//                    )
//                    mLocation?.let {
//                        AppExecutors.instance?.networkIO()?.execute {
//                            sendLocation(loc)
//                        }
//                    }
//                }
//            })
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        isServiceStarted = false
    }


    private fun sendLocation(barLocator: BarLocator){
        val JSON = MediaType.get("application/json; charset=utf-8")
        val jsonObject = JSONObject()
        var url  = "http://37.32.24.162:9002/api/BarLocator"
        jsonObject.put("barId", barLocator.barId)
        jsonObject.put("latLong", barLocator.latLong)
        jsonObject.put("latitude", barLocator.latitude)
        jsonObject.put("longitude", barLocator.longitude)
        var reqbody: RequestBody = RequestBody.create(JSON, jsonObject.toString())
        val client = OkHttpClient()
        val request = Request.Builder()
            .url(url)
            .method("POST", reqbody)
            .addHeader(
                "Authorization",
                "bearer " + preferenceHelper.getToken()
            )
            .addHeader("Content-Type", "application/json; charset=UTF-8")
            .build()

        try {
            val response = client.newCall(request).execute()
            if (response.code() === 200) {
                // Get response
                val jsonData = response.body()?.string()

            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }


    companion object {
        var mLocation: Location? = null
        var isServiceStarted = false
    }
}