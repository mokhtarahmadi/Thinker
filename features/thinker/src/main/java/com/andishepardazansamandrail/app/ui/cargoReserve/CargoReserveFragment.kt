package com.andishepardazansamandrail.app.ui.cargoReserve

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.andishepardazansamandrail.app.databinding.FragmentCargoReserveBinding
import com.andishepardazansamandrail.app.databinding.FragmentCurrentCargoBinding
import com.andishepardazansamandrail.app.ui.currentCorgo.CurrentCargoAdapter
import com.andishepardazansamandrail.app.ui.currentCorgo.CurrentCargoViewModel
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.viewVisibility
import kotlinx.android.synthetic.main.loading_central.*
import org.koin.android.viewmodel.ext.android.viewModel

class CargoReserveFragment :BaseFragment(){
    private val viewModel: CargoReserveViewModel by viewModel()
    private lateinit var binding: FragmentCargoReserveBinding
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentCargoReserveBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRecyclerView()

        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })

        viewModel.cancelCargo.observe(viewLifecycleOwner, Observer {

        })
    }

    private fun configureRecyclerView() {
        binding.cargoRv.adapter = CargoReserveAdapter(viewModel)
    }
}