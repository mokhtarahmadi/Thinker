package com.andishepardazansamandrail.app.ui.messages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.assistant.GetUserUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetMessagesUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.AskQuestion
import com.andishepardazansamandrail.model.thinker.Message
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MessageViewModel(private val dispatchers: AppDispatchers,
                       private val getUserUseCase: GetUserUseCase,
                       private val getMessagesUseCase: GetMessagesUseCase
)  : BaseViewModel() {

    init {
        getMessagess()
    }
    private val _messages = MediatorLiveData<Resource<List<Message>>>()
    val messages: LiveData<Resource<List<Message>>> get() = _messages
    private var messagesSource: LiveData<Resource<List<Message>>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    fun getMessagess() =
        viewModelScope.launch(dispatchers.main) {
            var personId = getUserUseCase.invoke().toInt()

            _messages.removeSource(messagesSource)
            withContext(dispatchers.io) {
                messagesSource = getMessagesUseCase.invoke(ConstRepo.GET_MESSAGES_URL + "?personId=${personId}")
            }
            _messages.addSource(messagesSource) {

                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _messages.value = it

                        }
                        _loadingAnimation.value = false

                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true


                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

}