package com.andishepardazansamandrail.app.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.SetUserTypeUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.UserLoginUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Login
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(private val userLoginUseCase: UserLoginUseCase,
                     private val setUserTypeUseCase : SetUserTypeUseCase,
                     private val dispatchers: AppDispatchers) : BaseViewModel() {

    // FOR DATA
    private val _login = MediatorLiveData<Resource<Login>>()
    val login: LiveData<Resource<Login>> get() = _login
    private var loginSource: LiveData<Resource<Login>> = MutableLiveData()

    private val _identityNumber = MutableLiveData<String>()
    val identityNumber: LiveData<String> get() = _identityNumber

    private val _driverSmartCode = MutableLiveData<String>()
    val driverSmartCode: LiveData<String> get() = _driverSmartCode

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    fun onButtonClick(identityNumber: String?, driverSmartCode: String?) {

        checkParameters(identityNumber, driverSmartCode)
    }

    private fun checkParameters(identityNumber: String?, driverSmartCode: String?) {

        if (identityNumber.isNullOrEmpty() || driverSmartCode.isNullOrEmpty()) {
            _snackbarError.value = Event(R.string.error_login)
        } else {
            userLogin(identityNumber, driverSmartCode)
        }
    }

    private fun userLogin(identityNumber: String, driverSmartCode: String) =
        viewModelScope.launch(dispatchers.main) {
            _login.removeSource(loginSource)
            withContext(dispatchers.io) {
                loginSource = userLoginUseCase.invoke(
                    ConstRepo.LOGIN_URL,
                    identityNumber,
                    driverSmartCode

                )
            }
            _login.addSource(loginSource) {

                _login.value = it

                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        _loadingAnimation.value = false
                        setUserTypeUseCase.invoke(1)
                        navigateToValidation()
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

    fun onTextChangedIdentityNumber(s: CharSequence) {
        _identityNumber.value = s.toString()
    }

    fun onTextChangedDriverSmartCode(s: CharSequence) {
        _driverSmartCode.value = s.toString()
    }

    private fun navigateToValidation() = navigate(LoginFragmentDirections.actionLoginFragmentToNavyFragment(0))
}