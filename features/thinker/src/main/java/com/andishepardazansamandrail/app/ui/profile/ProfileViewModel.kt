package com.andishepardazansamandrail.app.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.ui.main.MainFragmentDirections
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.GetDriverInfoUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.SetTokenUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.SetUserTypeUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.SetViewSelectedUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Driver
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProfileViewModel(private val getDriverInfoUseCase : GetDriverInfoUseCase,
                       private val setTokenUseCase : SetTokenUseCase,
                       private val setUserTypeUseCase: SetUserTypeUseCase,
                       private val dispatchers: AppDispatchers
                       ,private val setViewSelectedUseCase: SetViewSelectedUseCase
) : BaseViewModel() {

    private val _driverInfo = MediatorLiveData<Resource<Driver>>()
    val driverInfo: LiveData<Resource<Driver>> get() = _driverInfo
    private var driverInfoSource: LiveData<Resource<Driver>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    init {
        driverInfo()
    }


    fun setViewSelected(state : Int) {
        setViewSelectedUseCase.invoke(1)

        when(state){
            1 -> navigateToUserInfo()
            2 -> navigateToFinancial()
            3 -> navigateToNavy()
            4->navigateToMessage()

        }
    }


    private fun driverInfo() = viewModelScope.launch(dispatchers.main) {
        _driverInfo.removeSource(driverInfoSource)
        withContext(dispatchers.io) {
            driverInfoSource = getDriverInfoUseCase.invoke(
                ConstRepo.DRIVER_INFO_URL
            )
        }
        _driverInfo.addSource(driverInfoSource) {


            when (it.status) {

                // Success state
                Resource.Status.SUCCESS -> {
                    _driverInfo.value = it
                 //   _loadingAnimation.value = false

                }

                // Loading state
                //Resource.Status.LOADING -> _loadingAnimation.value = true

                // Error state
                Resource.Status.ERROR -> {
                  //  _loadingAnimation.value = false
                 //   _snackbarError.value = Event(R.string.an_error_happened)

                }
            }
        }
    }

    fun setToken(){
        setTokenUseCase.invoke()
        setUserTypeUseCase.invoke(0)
    }

    fun navigateToUserInfo() = navigate(MainFragmentDirections.actionMainFragmentToUserInfoFragment())
    fun navigateToFinancial() = navigate(MainFragmentDirections.actionMainFragmentToFinancialFragment())
    fun navigateToNavy() = navigate(MainFragmentDirections.actionMainFragmentToNavyFragment())
    fun navigateToMessage() = navigate(MainFragmentDirections.actionMainFragmentToMessageFragment())


}