package com.andishepardazansamandrail.app.ui.messages

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.Message

object MessageBinding {
    @BindingAdapter("app:itemsMessage")
    @JvmStatic fun setItemsMessage(recyclerView: RecyclerView, resource: Resource<List<Message>>?) {
        with(recyclerView.adapter as MessageAdapter) {
            resource?.data?.let { updateData(it) }
        }

    }


    @JvmStatic @BindingAdapter("bind:tvResponseMessage")
    fun setResponseMessage(view: AppCompatTextView, resource: Message?) {

        resource?.let {
            if (!it.responseMessage.isNullOrEmpty())
                view.text = it.responseMessage
        }

    }

    @JvmStatic @BindingAdapter("bind:tvFinancialMessage")
    fun setFinancialMessage(view: AppCompatTextView, resource: Message?) {

        resource?.let {
                view.text = "شماره مالی : ${it.financialId.toString()}"
        }

    }

    @JvmStatic @BindingAdapter("bind:tvWayBillMessage")
    fun setWayBillMessage(view: AppCompatTextView, resource: Message?) {

        resource?.let {
            view.text = "شماره بارنامه : ${it.wayBill.toString()}"
        }

    }



    @JvmStatic @BindingAdapter("bind:setMessageNoData")
    fun setMessageNoData(view: LinearLayoutCompat, resource: Resource<List<Message>>?) {

        resource?.data?.let {
            if (it.isNotEmpty())
                view.visibility = View.GONE
            else
                view.visibility = View.VISIBLE
        }

    }
}