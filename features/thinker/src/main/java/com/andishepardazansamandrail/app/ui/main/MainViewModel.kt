package com.andishepardazansamandrail.app.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.*
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.BarLocator
import com.andishepardazansamandrail.model.thinker.Cargo
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(
    private val activeCargoUseCase: ActiveCargoUseCase,
    private val getCargoStatusUseCase: GetCargoStatusUseCase,
    private val getViewSelectedUseCase: GetViewSelectedUseCase,
    private val  sendLocationUseCase: SendLocationUseCase,
    private val dispatchers: AppDispatchers
) : BaseViewModel() {

    init {
        cargoes()
    }
    private val _cargo1 = MediatorLiveData<Resource<Cargo?>>()
    val cargo1: LiveData<Resource<Cargo?>> get() = _cargo1
    private var cargo1Source: LiveData<Resource<Cargo>> = MutableLiveData()

    private val _isLocation = MediatorLiveData<Resource<Boolean>>()
    val isLocation: LiveData<Resource<Boolean>> get() = _isLocation
    private var isLocationSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _cargoStatus = MutableLiveData<Int>()
    val cargoStatus: LiveData<Int> get() = _cargoStatus
    fun isViewState() : Int {
        return getCargoStatusUseCase.invoke()!!
    }

    fun isViewSelected() : Int {
        return getViewSelectedUseCase.invoke()!!
    }

    private fun sendLocation(locator: BarLocator) =
        viewModelScope.launch(dispatchers.main) {
            _isLocation.removeSource(isLocationSource)
            withContext(dispatchers.io) {
                isLocationSource = sendLocationUseCase.invoke(ConstRepo.BAR_LOCATOR , locator)
            }
            _isLocation.addSource(isLocationSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _isLocation.value = it


                        }
                        //  _loadingAnimation.value = false
                    }

                    //  // Loading state
                    // Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        // _loadingAnimation.value = false
                        // _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
    }

    fun cargoes() =
        viewModelScope.launch(dispatchers.main) {
            _cargo1.removeSource(cargo1Source)
            withContext(dispatchers.io) {
                cargo1Source = activeCargoUseCase.invoke(ConstRepo.ACTIVE_BAR_URL)
            }
            _cargo1.addSource(cargo1Source) {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cargo1.value = it
                        }
                    }
                    Resource.Status.ERROR -> {
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

    fun onClickSendLocator(barLocator: BarLocator) = sendLocation(barLocator)

}