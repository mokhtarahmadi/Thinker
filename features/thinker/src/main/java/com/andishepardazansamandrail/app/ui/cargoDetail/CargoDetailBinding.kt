package com.andishepardazansamandrail.app.ui.cargoDetail

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.andishepardazansamandrail.model.thinker.Cargo

object CargoDetailBinding {
    @BindingAdapter("bind:cargoDetailPrice")
    @JvmStatic fun setCargoDetailPrice(view: TextView, resource: Int) {
        view.text = " کرایه:${resource} ریال "
    }

    @BindingAdapter("bind:cargoDetailOrigin")
    @JvmStatic fun setCargoDetailOrigin(view: TextView, resource: Cargo) {
        view.text = " مبدا:${resource.fromCity}  ${resource.sourceAddress}"
    }

    @BindingAdapter("bind:cargoDetailDest")
    @JvmStatic fun setCargoDetailDest(view: TextView, resource: Cargo) {
        view.text = "مقصد: ${resource.toCity}  ${resource.deliveryAddress}"
    }

    @BindingAdapter("bind:cargoDetailDate")
    @JvmStatic fun setCargoDetailDate(view: TextView, resource: String) {
        view.text = " تاریخ بارگیری:${resource}  "
    }
    @BindingAdapter("bind:cargoDetailTime")
    @JvmStatic fun setCargoDetailTime(view: TextView, resource: String) {
        view.text = " ساعت بارگیری:${resource}  "
    }

    @BindingAdapter("bind:cargoDetailGood")
    @JvmStatic fun setCargoDetailGood(view: TextView, resource: String) {
        view.text = " کالا :${resource}  "
    }

    @BindingAdapter("bind:cargoDetailPack")
    @JvmStatic fun setCargoDetailPack(view: TextView, resource: String) {
        view.text = " توع بسته بندی :${resource}  "
    }

    @BindingAdapter("bind:cargoDetailNav")
    @JvmStatic fun setCargoDetailNav(view: TextView, resource: String) {
        view.text = " ناوگان :${resource} "
    }

    @BindingAdapter("bind:cargoDetailTripTime")
    @JvmStatic fun setCargoDetailTripTime(view: TextView, resource: String?) {

        if(resource == null)
            view.text = " طول سفر : "

        resource?.let {
            view.text = " طول سفر :${resource} "
        }
    }
}