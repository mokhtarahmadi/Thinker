package com.andishepardazansamandrail.app.ui.currentCorgo

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemCurrentCargoBinding
import com.andishepardazansamandrail.model.thinker.Cargo

class CurrentCargoViewHolder (parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemCurrentCargoBinding.bind(parent)

    fun bindTo(cargo: Cargo, viewModel: CurrentCargoViewModel, position: Int) {
        binding.cargo = cargo
        binding.viewModel = viewModel

    }


}