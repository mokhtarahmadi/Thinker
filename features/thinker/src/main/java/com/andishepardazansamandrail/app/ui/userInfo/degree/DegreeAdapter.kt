package com.andishepardazansamandrail.app.ui.userInfo.degree

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.ui.userInfo.UserInfoViewModel
import com.andishepardazansamandrail.model.thinker.Degree

class DegreeAdapter (private val viewModel: UserInfoViewModel): RecyclerView.Adapter<DegreeViewHolder>() {

    private val items: MutableList<Degree> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = DegreeViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_degree,
            parent,
            false
        )
    )


    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: DegreeViewHolder, position: Int)  = holder.bindTo(
        items[position],
        viewModel,
        position
    )

    fun updateData(degrees: List<Degree>) {

        //val diffCallback = ProductItemDiffCallback(items, supplierItems)
        //val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(degrees)
        //diffResult.dispatchUpdatesTo(this)
    }
}