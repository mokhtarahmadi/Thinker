package com.andishepardazansamandrail.app.ui.userInfo.view

import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.andishepardazansamandrail.model.thinker.Driver


@BindingAdapter("bind:userName")
fun setUserName(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
         textView.text = "${driver.firstName} ${driver.lastName}"
        //textView.text = "جمال طالبی"
    }
}

@BindingAdapter("bind:userNationalCode")
fun setUserNationalCode(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.identityNumber }"
    }
}

@BindingAdapter("bind:userFatherName")
fun setUserFatherName(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.fatherName}"
    }
}


@BindingAdapter("bind:userIdentityCode")
fun setUserIdentityCode(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "ندارد"
    }
}


@BindingAdapter("bind:userBirthDay")
fun setUserBirthDay(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.birthDay}"
    }
}


@BindingAdapter("bind:userSmartCardId")
fun setUserSmartCardId(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.smartCardId}"
    }
}

@BindingAdapter("bind:userDriverType")
fun setUserDriverType(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.driverType}"
    }
}


@BindingAdapter("bind:userInsuranceNumber")
fun setUserInsuranceNumber(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.insuranceNumber}"
    }
}


@BindingAdapter("bind:userLicenseNumber")
fun setUserLicenseNumber(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.licenseNumber}"
    }
}



