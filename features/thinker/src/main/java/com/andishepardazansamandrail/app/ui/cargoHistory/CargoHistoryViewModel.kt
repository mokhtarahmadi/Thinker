package com.andishepardazansamandrail.app.ui.cargoHistory

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.CancelCargoRequestedUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetCargoesUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CargoHistoryViewModel(private val cargoesUseCase: GetCargoesUseCase,
                            private val cancelCargoUseCase: CancelCargoRequestedUseCase,
                            private val dispatchers: AppDispatchers
) : BaseViewModel() {

    init {
        cargoes()
    }
    // FOR DATA
    private val _cargoesHistory = MediatorLiveData<Resource<List<Cargo>>>()
    val cargoesHistory: LiveData<Resource<List<Cargo>>> get() = _cargoesHistory
    private var cargoesHistorySource: LiveData<Resource<List<Cargo>>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    private fun cargoes() =
        viewModelScope.launch(dispatchers.main) {
            _cargoesHistory.removeSource(cargoesHistorySource)
            withContext(dispatchers.io) {
                cargoesHistorySource = cargoesUseCase.invoke(
                    ConstRepo.GET_BAR_URL  + "/7"
                )
            }
            _cargoesHistory.addSource(cargoesHistorySource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cargoesHistory.value = it

                        }
                        _loadingAnimation.value = false
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

}