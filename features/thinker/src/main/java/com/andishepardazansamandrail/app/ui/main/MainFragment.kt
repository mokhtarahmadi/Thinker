package com.andishepardazansamandrail.app.ui.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.icu.util.TimeUnit
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.work.*
import com.etebarian.meowbottomnavigation.MeowBottomNavigation
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.databinding.FragmentMainBinding
import com.andishepardazansamandrail.app.location.LocationService
import com.andishepardazansamandrail.app.ui.home.HomeFragment
import com.andishepardazansamandrail.app.ui.map.MapFragment
import com.andishepardazansamandrail.app.ui.myGood.MyGoodFragment
import com.andishepardazansamandrail.app.ui.profile.ProfileFragment
import com.andishepardazansamandrail.app.workmanger.TrackLocationWorker
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.addFragment
import com.andishepardazansamandrail.common.extension.replaceFragment
import com.andishepardazansamandrail.model.thinker.BarLocator
import io.nlopez.smartlocation.SmartLocation
import kotlinx.android.synthetic.main.content_main.*
import org.koin.android.viewmodel.ext.android.viewModel


class MainFragment : BaseFragment() {
    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: FragmentMainBinding
    override fun getViewModel(): BaseViewModel = viewModel

    private val LOCATION_PERMISSION_CODE = 0
    private val ID_MENU = 1
    private val ID_MYGOOD = 2
    private val ID_HOME = 3
    private val ID_Message = 4
    private val ID_MAP = 5
    private var BarId : Int? = 0;

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMainBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().startService( Intent(requireContext(), LocationService::class.java))

        if (!checkPermission()) {
            requestPermission()
        }
       // requestLocationPermission()

        bottomNavigation.add(MeowBottomNavigation.Model(ID_MYGOOD, R.drawable.ic_baseline_local_shipping_24))
        bottomNavigation.add(MeowBottomNavigation.Model(ID_HOME, R.drawable.ic_home))
        bottomNavigation.add(MeowBottomNavigation.Model(ID_MENU, R.drawable.ic_menu))

//        bottomNavigation.add(MeowBottomNavigation.Model(ID_Message, R.drawable.ic_messages))
//        bottomNavigation.add(MeowBottomNavigation.Model(ID_MAP, R.drawable.ic_map))

        if(viewModel.isViewSelected()==1){
            profileFragment()
        }else{
            if (viewModel.isViewState()!=0 && viewModel.isViewState()!=5){
                myGoodFragment()
            }else{
                loadHomeFragment()
            }
        }

        viewModel.isLocation.observe(viewLifecycleOwner, Observer {  })

        viewModel.cargo1.observe(viewLifecycleOwner, Observer {
            it.data?.apply {
                BarId = barId
            }

            //sendLocation()
        });

       // sendLocation()
        initListeners()

        //startWorkManager()
    }


    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
        val result1 = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), 1)
    }

    fun sendLocation(){
        try {
            SmartLocation.with(context).location()/*.config(builder.build())*/
                .start { location ->

                    var loc = BarLocator(
                        BarId!!, location.latitude.toString() + "," +location.longitude.toString(),
                        location.latitude.toString(),
                        location.longitude.toString(),

                    )
                    viewModel.onClickSendLocator(loc)


                }
        } catch (e: Exception) {

        }

    }

    private fun startWorkManager() {

        val constraint: Constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val workRequest = PeriodicWorkRequest.Builder(TrackLocationWorker::class.java, 15, java.util.concurrent.TimeUnit.MINUTES)
            .setConstraints(constraint)
            .build()

        val workManager = WorkManager.getInstance()
        workManager.enqueueUniquePeriodicWork(
            "my_unique_worker",
            ExistingPeriodicWorkPolicy.KEEP,
            workRequest
        )
//        val periodicWorkRequest =
//            PeriodicWorkRequest.Builder(TrackLocationWorker::class.java, 15, java.util.concurrent.TimeUnit.MINUTES)
//                .addTag("periodic-work-request")
//                .build()
//
//        WorkManager.getInstance(requireContext()).enqueue(periodicWorkRequest)

//        val myWorkBuilder = PeriodicWorkRequest.Builder(
//            TrackLocationWorker::class.java,
//            15,
//            java.util.concurrent.TimeUnit.MINUTES
//        ).addTag("JobSendLocation")
//        val myWork = myWorkBuilder.build()
//        WorkManager.getInstance().enqueueUniquePeriodicWork(
//            "JobSendLocation",
//            ExistingPeriodicWorkPolicy.KEEP,
//            myWork
//        )
    }
    private fun loadHomeFragment() {
        bottomNavigation.show(ID_HOME, false)
        addFragment(HomeFragment(), R.id.container)
    }

    private fun myGoodFragment() {
        bottomNavigation.show(ID_MYGOOD, false)
        addFragment(MyGoodFragment(), R.id.container)
    }

    private fun profileFragment() {
        bottomNavigation.show(ID_MENU, false)
        addFragment(ProfileFragment(), R.id.container)
    }
    private fun initListeners() {

        bottomNavigation.setOnClickMenuListener { model: MeowBottomNavigation.Model ->
            when (model.id) {
                1 -> {
                    replaceFragment(ProfileFragment(), R.id.container)
                }
                2 -> {
                    replaceFragment(MyGoodFragment(), R.id.container)
                }
                3 -> {
                    replaceFragment(HomeFragment(), R.id.container)
                    bottomNavigation.show(ID_HOME, true)
                }
                4 -> {
                    //replaceFragment(ProductFragment(), R.id.container)
                }
                5 -> {
                    replaceFragment(MapFragment(), R.id.container)
                }
            }
            null
        }
    }


//    @RequiresApi(api = Build.VERSION_CODES.M)
//    private fun requestLocationPermission() {
//        if (ActivityCompat.shouldShowRequestPermissionRationale(
//                requireActivity(),
//                Manifest.permission.READ_SMS
//            )
//        ) {
//            Log.d("", "shouldShowRequestPermissionRationale(), no permission requested")
//            return
//        }
//        ActivityCompat.requestPermissions(
//            requireActivity(), arrayOf(
//                Manifest.permission.ACCESS_FINE_LOCATION,
//                Manifest.permission.ACCESS_COARSE_LOCATION
//            ),
//            LOCATION_PERMISSION_CODE
//        )
//    }

}