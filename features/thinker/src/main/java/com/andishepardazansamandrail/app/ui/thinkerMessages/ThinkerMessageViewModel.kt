package com.andishepardazansamandrail.app.ui.thinkerMessages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.assistant.GetUserUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetThinkerMessagesUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.ShowedMessageUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Message
import com.andishepardazansamandrail.model.thinker.ShowMessage
import com.andishepardazansamandrail.model.thinker.ThinkerMessage
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ThinkerMessageViewModel(private val dispatchers: AppDispatchers,
                              private val getUserUseCase: GetUserUseCase,
                              private val getThinkerMessagesUseCase: GetThinkerMessagesUseCase,
                              private val showedMessageUseCase: ShowedMessageUseCase,
):BaseViewModel() {

    init {
        getMessagess()
    }
    private val _messages = MediatorLiveData<Resource<List<ThinkerMessage>>>()
    val messages: LiveData<Resource<List<ThinkerMessage>>> get() = _messages
    private var messagesSource: LiveData<Resource<List<ThinkerMessage>>> = MutableLiveData()

    private val _setMessages = MediatorLiveData<Resource<Boolean>>()
    val setMessages: LiveData<Resource<Boolean>> get() = _setMessages
    private var setMessagesSource: LiveData<Resource<Boolean>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    fun getMessagess() =
        viewModelScope.launch(dispatchers.main) {
            var personId = getUserUseCase.invoke().toInt()

            _messages.removeSource(messagesSource)
            withContext(dispatchers.io) {
                messagesSource = getThinkerMessagesUseCase.invoke(ConstRepo.GET_THINKER_MESSAGES_URL )
            }
        _messages.addSource(messagesSource) {

            when (it.status) {

                // Success state
                Resource.Status.SUCCESS -> {
                    it?.data.apply {
                        _messages.value = it

                    }
                    _loadingAnimation.value = false

                }

                // Loading state
                Resource.Status.LOADING -> _loadingAnimation.value = true


                // Error state
                Resource.Status.ERROR -> {
                    _loadingAnimation.value = false
                    _snackbarError.value = Event(R.string.an_error_happened)

                }
            }
       }
   }

    fun showMessage(id : Int) =
        viewModelScope.launch(dispatchers.main) {

            _setMessages.removeSource(setMessagesSource)
            withContext(dispatchers.io) {
                setMessagesSource = showedMessageUseCase.invoke(ConstRepo.GET_THINKER_MESSAGES_URL , ShowMessage(id))
            }
            _setMessages.addSource(setMessagesSource) {

                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _setMessages.value = it

                        }
                        _loadingAnimation.value = false

                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true


                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

}