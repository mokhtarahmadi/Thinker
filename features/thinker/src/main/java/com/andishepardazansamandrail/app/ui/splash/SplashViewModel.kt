package com.andishepardazansamandrail.app.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.*
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SplashViewModel (private val getTokenUseCase : GetTokenUseCase,
                       private val activeCargoUseCase: ActiveCargoUseCase,
                       private val dispatchers: AppDispatchers,
                       private val navyTypeIdUseCase: GetNavyTypeIdUseCase,
                       private val getUserTypeUseCase: GetUserTypeUseCase,
                       private val setActiveCargoUseCase: SetActiveCargoUseCase
): BaseViewModel() {


    init {
       // setActiveCargoUseCase.invoke(0)
    }

    private val _cargo = MediatorLiveData<Resource<Cargo>>()
    val cargo: LiveData<Resource<Cargo>> get() = _cargo
    private var cargoSource: LiveData<Resource<Cargo>> = MutableLiveData()

    fun navigateToSelectUserType() = navigate(SplashFragmentDirections.actionSplashFragmentToSelectUserType())
    fun navigateToMainAssistant() = navigate(SplashFragmentDirections.actionSplashFragmentToMainAssistantFragment())
    fun navigateToMain() = navigate(SplashFragmentDirections.actionSplashFragmentToMainFragment())
    fun navigateToNavy() = navigate(SplashFragmentDirections.actionSplashFragmentToNavyFragment(0))
    fun navigateToLogin() = navigate(SplashFragmentDirections.actionSplashFragmentToLoginFragment())

    fun isUserLoggedIn() : Boolean{

        var x = getTokenUseCase.invoke()
        return x !=null && x!=""
      //  return getTokenUseCase.invoke() != null && getTokenUseCase.invoke() != ""
    }

    fun hasNavy() : Boolean{

        var x = navyTypeIdUseCase.invoke()
        return x !=null && x!=0
        //  return getTokenUseCase.invoke() != null && getTokenUseCase.invoke() != ""
    }

    fun userType() : Int{
        return getUserTypeUseCase.invoke()
    }

    fun setActivateCargoState(){
        setActiveCargoUseCase.invoke(1)
    }

     fun cargo() =
        viewModelScope.launch(dispatchers.main) {
            _cargo.removeSource(cargoSource)
            withContext(dispatchers.io) {
                cargoSource = activeCargoUseCase.invoke(
                    ConstRepo.ACTIVE_BAR_URL
                )
            }
            _cargo.addSource(cargoSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cargo.value = it

                        }
                    }

                    // Loading state

                    // Error state
                    Resource.Status.ERROR -> {
                      //  _snackbarError.value = Event(R.string.an_error_happened)
                        _cargo.value = it
                    }
                }
            }
        }
}