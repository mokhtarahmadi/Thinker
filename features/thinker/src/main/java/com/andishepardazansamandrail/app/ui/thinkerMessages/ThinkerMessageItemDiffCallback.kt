package com.andishepardazansamandrail.app.ui.thinkerMessages

import androidx.recyclerview.widget.DiffUtil
import com.andishepardazansamandrail.model.thinker.ThinkerMessage

class ThinkerMessageItemDiffCallback(private val oldList: List<ThinkerMessage>,
                                     private val newList: List<ThinkerMessage>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return true
    }
}