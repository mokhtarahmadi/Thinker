package com.andishepardazansamandrail.app.ui.cargoSalon

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.ui.common.SearchFilterAdapter
import com.andishepardazansamandrail.app.ui.main.MainFragmentDirections
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.thinker.GetCargoesOfTrailUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetCargoesUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetNavyTypeIdUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.CargoOfTrailInput
import com.andishepardazansamandrail.model.thinker.SearchFilter
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import saman.zamani.persiandate.PersianDate
import saman.zamani.persiandate.PersianDateFormat
import java.util.*


class CargoSalonViewModel(
    private val cargoesUseCase: GetCargoesUseCase,
    private val cargoesOfTrailUseCase: GetCargoesOfTrailUseCase,
    private val getNavyTypeIdUseCase : GetNavyTypeIdUseCase,
    private val dispatchers: AppDispatchers
): BaseViewModel() , SearchFilterAdapter.OnItemClickListener{

    // FOR DATA
    private val _cargoes = MediatorLiveData<Resource<List<Cargo>>>()
    val cargoes: LiveData<Resource<List<Cargo>>> get() = _cargoes
    private var cargoesSource: LiveData<Resource<List<Cargo>>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    var date :String = "";


    init {
        var trailId  = getNavyTypeIdUseCase.invoke()


        cargoes(trailId!!)
    }
    fun configureSearchFilterAdapter(filters_rv: RecyclerView) {

        filters_rv.adapter = SearchFilterAdapter(getSearchFilters(), this)
    }
    private fun getSearchFilters(): MutableList<SearchFilter> {
        val pdate = PersianDate(Date())
        val pdformater1 = PersianDateFormat("Y/m/d")

        val item1 = SearchFilter(
            1,
            "${pdate.dayName()} ${pdate.shDay} ${pdate.monthName()}",
            pdformater1.format(pdate)
        )
        val item2 = SearchFilter(2, "${pdate.addDay(1).dayName()}" ,pdformater1.format(pdate.addDay(1)) )
        val item3 = SearchFilter(3, "${pdate.addDay(2).dayName()}" ,pdformater1.format(pdate.addDay(2)) )
        val item4= SearchFilter(4, "${pdate.addDay(3).dayName()}" ,pdformater1.format(pdate.addDay(3)) )
        val item5 = SearchFilter(5, "${pdate.addDay(4).dayName()}" ,pdformater1.format(pdate.addDay(4)) )
        val item6 = SearchFilter(6, "${pdate.addDay(5).dayName()}" ,pdformater1.format(pdate.addDay(5)) )
        val item7 = SearchFilter(7, "${pdate.addDay(6).dayName()}" ,pdformater1.format(pdate.addDay(6)) )

        date = pdformater1.format(pdate)
        val filterItems = mutableListOf<SearchFilter>()
        filterItems.add(item1)
        filterItems.add(item2)
        filterItems.add(item3)
        filterItems.add(item4)
        filterItems.add(item5)
        filterItems.add(item6)
        filterItems.add(item7)

        return filterItems

    }

    override fun onItemClicked(searchItem: SearchFilter) {
        date = searchItem.date
        cargoes(0)
    }


    private fun cargoes(trailId : Int) =
        viewModelScope.launch(dispatchers.main) {
            _cargoes.removeSource(cargoesSource)
            withContext(dispatchers.io) {
                val pdate = PersianDate(Date())
                val pdformater1 = PersianDateFormat("Y/m/d")
                cargoesSource = cargoesOfTrailUseCase.invoke(
                    ConstRepo.GET_BAR_Trail_URL , CargoOfTrailInput(trailId,0,pdformater1.format(pdate))
                )
            }
            _cargoes.addSource(cargoesSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _cargoes.value = it

                        }
                        _loadingAnimation.value = false
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }


    fun onClickItem(cargo: Cargo) = navigateToCargoDetail(cargo)
    fun navigateToCargoDetail(cargo: Cargo) {
        navigate(MainFragmentDirections.actionMainFragmentToCargoDetailFragment(cargo))
    }

}