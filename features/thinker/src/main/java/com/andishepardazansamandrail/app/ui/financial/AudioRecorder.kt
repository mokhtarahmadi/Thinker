package com.andishepardazansamandrail.app.ui.financial

import android.media.MediaRecorder
import java.io.IOException
import java.lang.Exception

class AudioRecorder {
    private var mediaRecorder: MediaRecorder? = null


    private fun initMediaRecorder() {
        mediaRecorder = MediaRecorder()
//        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
//        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
//        mediaRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
        mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
    }


    @Throws(IOException::class)
    fun start(filePath: String?) {
        if (mediaRecorder == null) {
            initMediaRecorder()
        }
        try {
            mediaRecorder!!.setOutputFile(filePath)
            mediaRecorder!!.prepare()
            mediaRecorder!!.start()
        }catch (e : Exception){
            e.printStackTrace()
        }

    }

    fun stop() {
        try {
            mediaRecorder!!.stop()
//            destroyMediaRecorder()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun destroyMediaRecorder() {
        mediaRecorder!!.release()
        mediaRecorder = null
    }
}