package com.andishepardazansamandrail.app.ui.cargoReserve

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo

object CargoReserveBinding {
    @BindingAdapter("app:itemsReserveCargoes")
    @JvmStatic fun setItems(recyclerView: RecyclerView, resource: Resource<List<Cargo>>?) {
        with(recyclerView.adapter as CargoReserveAdapter) {
            resource?.data?.let { updateData(it) }
        }
    }

    @JvmStatic @BindingAdapter("bind:setReserveCargoesNoData")
    fun setReserveCargoesNoData(view: LinearLayoutCompat, resource: Resource<List<Cargo>>?) {

        resource?.data?.let {
            if (it.isNotEmpty())
                view.visibility = View.GONE
            else
                view.visibility = View.VISIBLE
        }

    }


    @BindingAdapter("bind:cargoPriceReserve")
    @JvmStatic fun setCargoPriceReserve(view: TextView, resource: Int) {
        view.text = "${resource} ریال "
    }

    @BindingAdapter("bind:setOriginReserve")
    @JvmStatic fun setOriginReserve(view: TextView, resource: Cargo) {
        view.text = "${resource.fromCity}  ${resource.sourceAddress}"
    }

    @BindingAdapter("bind:setDestReserve")
    @JvmStatic fun setDestReserve(view: TextView, resource: Cargo) {
        view.text = "${resource.toCity}  ${resource.deliveryAddress}"
    }
}