package com.andishepardazansamandrail.app.ui.thinkerMessages

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.model.thinker.ThinkerMessage

class ThinkerMessageAdapter (private val viewModel: ThinkerMessageViewModel): RecyclerView.Adapter<ThinkerMessageViewHolder>() {

    private val items: MutableList<ThinkerMessage> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ThinkerMessageViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_thinker_message,
            parent,
            false
        )
    )
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: ThinkerMessageViewHolder, position: Int) {

            holder.bindTo(items[position], viewModel,position)
    }

    fun updateData(messages: List<ThinkerMessage>) {

        val diffCallback = ThinkerMessageItemDiffCallback(items, messages)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(messages.filter { x -> x.subject!=null })

        //notifyDataSetChanged()
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(message: ThinkerMessage, position: Int){

        items[position] = message
        notifyItemChanged(position)
    }
}