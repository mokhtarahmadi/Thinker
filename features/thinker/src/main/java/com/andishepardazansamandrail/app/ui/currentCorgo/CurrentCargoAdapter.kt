package com.andishepardazansamandrail.app.ui.currentCorgo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.model.thinker.Cargo

class CurrentCargoAdapter  (private val viewModel: CurrentCargoViewModel): RecyclerView.Adapter<CurrentCargoViewHolder>() {

    private val items: MutableList<Cargo> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = CurrentCargoViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_current_cargo,
            parent,
            false
        )
    )
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: CurrentCargoViewHolder, position: Int)  = holder.bindTo(items[position], viewModel,position)

    fun updateData(cargoes: List<Cargo>) {

        val diffCallback = CurrentCargoItemDiffCallback(items, cargoes)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(cargoes)

        //notifyDataSetChanged()
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(cargo: Cargo, position: Int){

        items[position] = cargo
        notifyItemChanged(position)
    }
}