package com.andishepardazansamandrail.app.ui.cargoHistory

import androidx.recyclerview.widget.DiffUtil
import com.andishepardazansamandrail.model.thinker.Cargo

class CargoHistoryItemDiffCallback(private val oldList: List<Cargo>,
                                   private val newList: List<Cargo>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return true
    }
}