package com.andishepardazansamandrail.app.ui.navy.navyTypeList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.ui.navy.ChangeNavyViewModel
import com.andishepardazansamandrail.model.thinker.Navy

class NavyTypeAdapter (private val viewModel: ChangeNavyViewModel): RecyclerView.Adapter<NavyTypeViewHolder>() {

    private val items: MutableList<Navy> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = NavyTypeViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_navy_type2,
            parent,
            false
        )
    )


    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: NavyTypeViewHolder, position: Int)  = holder.bindTo(
        items[position],
        viewModel,
        position
    )

    fun updateData(supplierItems: List<Navy>) {

        //val diffCallback = ProductItemDiffCallback(items, supplierItems)
        //val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(supplierItems)

        //diffResult.dispatchUpdatesTo(this)
    }
}