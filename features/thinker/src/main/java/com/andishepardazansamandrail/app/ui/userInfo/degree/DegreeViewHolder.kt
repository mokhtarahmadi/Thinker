package com.andishepardazansamandrail.app.ui.userInfo.degree

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemDegreeBinding
import com.andishepardazansamandrail.app.ui.userInfo.UserInfoViewModel
import com.andishepardazansamandrail.model.thinker.Degree

class DegreeViewHolder(parent: View): RecyclerView.ViewHolder(parent) {

    private val binding = ItemDegreeBinding.bind(parent)

    fun bindTo(item: Degree, viewModel: UserInfoViewModel, position: Int) {
        binding.degreeItem = item
        binding.viewModel = viewModel
        binding.degreePosition = position
    }
}