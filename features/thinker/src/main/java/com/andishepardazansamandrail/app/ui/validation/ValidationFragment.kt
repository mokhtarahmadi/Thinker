package com.andishepardazansamandrail.app.ui.validation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.andishepardazansamandrail.app.databinding.FragmentLoginBinding
import com.andishepardazansamandrail.app.databinding.FragmentValidationBinding
import com.andishepardazansamandrail.app.ui.login.LoginViewModel
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.viewVisibility
import kotlinx.android.synthetic.main.loading_central.*
import org.koin.android.viewmodel.ext.android.viewModel

class ValidationFragment : BaseFragment() {
    private val viewModel: ValidationViewModel by viewModel()
    private lateinit var binding: FragmentValidationBinding
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentValidationBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners()
    {
        viewModel.loadingAnimation.observe(requireActivity(), Observer {
            if (it) {
                loading?.viewVisibility(View.VISIBLE)
            } else {
                loading?.viewVisibility(View.GONE)
            }
        })

        viewModel.cargo.observe(viewLifecycleOwner , Observer {

        })
    }

}