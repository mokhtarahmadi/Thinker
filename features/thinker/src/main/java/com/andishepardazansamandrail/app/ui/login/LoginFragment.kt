package com.andishepardazansamandrail.app.ui.login

import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.work.*
import com.andishepardazansamandrail.app.databinding.FragmentLogin2Binding
import com.andishepardazansamandrail.app.workmanger.TrackLocationWorker
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.handleSoftKeyboardVisibility
import com.andishepardazansamandrail.domain.utils.Resource
import kotlinx.android.synthetic.main.fragment_login2.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit


class LoginFragment: BaseFragment() {
    private val viewModel: LoginViewModel by viewModel()
    private lateinit var binding: FragmentLogin2Binding
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentLogin2Binding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListeners()
    }

    private fun initListeners() {

        //Changing font and setting up action when input type is password...
        edt_driverSmartCode.transformationMethod = PasswordTransformationMethod()
        edt_driverSmartCode.imeOptions = EditorInfo.IME_ACTION_DONE

        viewModel.login.observe(requireActivity(), Observer {
            it?.apply {
                if (it.status == Resource.Status.SUCCESS) {
                    //startWorkManager()
                }
            }
        })

        viewModel.loadingAnimation.observe(requireActivity(), Observer {
            if (it) {
                btn_login.startAnimation()

            } else {
                btn_login.revertAnimation()

            }
        })

        edt_driverSmartCode.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            var handled = false
            if (actionId == EditorInfo.IME_ACTION_DONE) {

                handleSoftKeyboardVisibility(InputMethodManager.HIDE_IMPLICIT_ONLY)
                viewModel.onButtonClick(
                    edt_identityNumber.text.toString(),
                    edt_driverSmartCode.text.toString()
                )

                handled = true
            }
            handled
        })
    }

    private fun startWorkManager() {
        val constraint: Constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val workRequest = PeriodicWorkRequest.Builder(TrackLocationWorker::class.java, 15, TimeUnit.MINUTES)
            .setConstraints(constraint)
            .build()

        val workManager = WorkManager.getInstance()
        workManager.enqueueUniquePeriodicWork(
            "my_unique_worker",
            ExistingPeriodicWorkPolicy.KEEP,
            workRequest
        )

//        val periodicWorkRequest =
//            PeriodicWorkRequest.Builder(TrackLocationWorker::class.java, 15, TimeUnit.MINUTES)
//                .addTag("periodic-work-request")
//                .build()
//
//        WorkManager.getInstance().enqueue(periodicWorkRequest)

//        val myWorkBuilder = PeriodicWorkRequest.Builder(
//            TrackLocationWorker::class.java,
//            15,
//            java.util.concurrent.TimeUnit.MINUTES
//        ).addTag("JobSendLocation")
//        val myWork = myWorkBuilder.build()
//        WorkManager.getInstance().enqueueUniquePeriodicWork(
//            "JobSendLocation",
//            ExistingPeriodicWorkPolicy.KEEP,
//            myWork
//        )
    }
}