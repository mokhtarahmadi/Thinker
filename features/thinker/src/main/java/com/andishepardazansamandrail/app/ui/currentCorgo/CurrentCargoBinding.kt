package com.andishepardazansamandrail.app.ui.currentCorgo

import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.ui.cargoSalon.CargoSalonAdapter
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.SearchFilter
import saman.zamani.persiandate.PersianDate
import saman.zamani.persiandate.PersianDateFormat
import java.text.SimpleDateFormat
import java.util.*

object CurrentCargoBinding {
    @BindingAdapter("app:itemsCurrentCargo")
    @JvmStatic fun setItems(recyclerView: RecyclerView, resource: Resource<List<Cargo>>?) {
        with(recyclerView.adapter as CurrentCargoAdapter) {
            resource?.data?.let { updateData(it) }
        }

    }

    @BindingAdapter("bind:wayBillText")
    @JvmStatic fun setwayBillText(textView: AppCompatTextView, cargo: Cargo?) {
        cargo?.apply {
            if (cargo.wayBill==null)
            textView.text = "شماره بارنامه : "
            else
                textView.text = "شماره بارنامه : ${cargo.wayBill}"

        }
    }

    @BindingAdapter("bind:originText")
    @JvmStatic fun setOriginText(textView: AppCompatTextView, cargo: Cargo?) {
        cargo?.apply {
            if (cargo.fromCity==null)
            textView.text = "مبدا : "
            else{
                if(cargo.sourceAddress == null)
                   textView.text = "مبدا : ${cargo.fromCity}"
                else
                    textView.text = "مبدا : ${cargo.fromCity} , ${cargo.sourceAddress}"

            }

        }
    }


    @BindingAdapter("bind:barIdText")
    @JvmStatic fun setBarIdText(textView: TextView, cargo: Cargo?) {
        cargo?.apply {
                textView.text = "${cargo.barId}"
        }
    }

    @BindingAdapter("bind:destText")
    @JvmStatic fun setDestText(textView: AppCompatTextView, cargo: Cargo?) {
        cargo?.apply {
            if (cargo.toCity==null)
            textView.text = "مقصد : "
            else{
                if (cargo.deliveryAddress ==null)
                    textView.text = "مقصد : ${cargo.toCity}"
                else
                    textView.text = "مقصد : ${cargo.toCity} , ${cargo.deliveryAddress}"


            }

        }
    }

    @BindingAdapter("bind:priceText")
    @JvmStatic fun setPriceText(textView: AppCompatTextView, cargo: Cargo?) {
        cargo?.apply {
            if (cargo.price==null)
                textView.text = "قیمت کرایه : "
            else
            textView.text = "قیمت کرایه : ${cargo.price}"
        }
    }

    @BindingAdapter("bind:dateText")
    @JvmStatic fun setDateText(textView: AppCompatTextView, cargo: Cargo?) {
        cargo?.apply {
            if (cargo.persianAppointmentDate==null)
                textView.text = "تاریخ : "
            else{
//                val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
//                var convertedDate: Date?  = sdf.parse( cargo.appointmentDate)
//                val pdate = PersianDate(convertedDate)
//                val pdformater1 = PersianDateFormat("Y/m/d")

                textView.text = "تاریخ :  ${persianAppointmentDate}"
               // textView.text = "تاریخ :  ${cargo.appointmentDate}  "
            }
        }
    }

    @BindingAdapter("bind:productText")
    @JvmStatic fun setProductText(textView: AppCompatTextView, cargo: Cargo?) {
        cargo?.apply {
            if (cargo.good==null)
                textView.text = "کالا : "
            else
               textView.text = "کالا : ${cargo.good}"
        }
    }

    @BindingAdapter("bind:packText")
    @JvmStatic fun setPackText(textView: AppCompatTextView, cargo: Cargo?) {
        cargo?.apply {
            if (cargo.pack==null)
                textView.text = "بسته بندی : "
            else
            textView.text = "بسته بندی : ${cargo.pack}"
        }
    }

    @BindingAdapter("bind:suggestText")
    @JvmStatic fun setSuggestText(textView: AppCompatTextView, cargo: Cargo?) {
        cargo?.apply {

            textView.text = "اعلام کننده : سمند ریل"
        }
    }
}