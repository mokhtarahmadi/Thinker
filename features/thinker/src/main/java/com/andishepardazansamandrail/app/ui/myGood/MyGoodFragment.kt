package com.andishepardazansamandrail.app.ui.myGood

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.dueeeke.tablayout.listener.OnTabSelectListener
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.databinding.FragmentHomeBinding
import com.andishepardazansamandrail.app.databinding.FragmentMyGoodBinding
import com.andishepardazansamandrail.app.ui.cargoHistory.CargoHistoryFragment
import com.andishepardazansamandrail.app.ui.cargoSalon.CargoSalonFragment
import com.andishepardazansamandrail.app.ui.cargoWorkflow.CargoWorkFlowFragment
import com.andishepardazansamandrail.app.ui.currentCorgo.CurrentCargoFragment
import com.andishepardazansamandrail.app.ui.home.HomeViewModel
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.ArrayList

class MyGoodFragment  : BaseFragment() {
    private val viewModel: MyGoodViewModel by viewModel()
    private lateinit var binding: FragmentMyGoodBinding

    override fun getViewModel(): BaseViewModel = viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMyGoodBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupTabLayout()
    }


    private fun setupTabLayout() {

        val mTitles = arrayOf(
            resources.getString(R.string.title_cargo_current),
            resources.getString(R.string.title_history),
        )

        val mFragments = ArrayList<Fragment>()
        mFragments.add(CurrentCargoFragment())
        mFragments.add(CargoHistoryFragment())

        val mAdapter = MyPagerAdapter(childFragmentManager, mFragments, mTitles)
        viewPager.adapter = mAdapter

        //tabLayouts.setupWithViewPager(viewPager)

        tabLayouts.setTabData(mTitles)
        tabLayouts.setOnTabSelectListener(object : OnTabSelectListener {
            override fun onTabSelect(position: Int) {
                viewPager.currentItem = position
            }

            override fun onTabReselect(position: Int) {}
        })

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                tabLayouts.currentTab = position
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        viewPager.currentItem = 0
    }

    class MyPagerAdapter(
        private val fm: FragmentManager,
        private val mFragments: ArrayList<Fragment>,
        private val mTitles: Array<String>
    ) : FragmentPagerAdapter(fm) {

        override fun getCount(): Int = mFragments.size

        override fun getPageTitle(position: Int): CharSequence {
            return mTitles[position]
        }

        override fun getItem(position: Int): Fragment {
            return mFragments[position]
        }
    }
}