package com.andishepardazansamandrail.app.ui.selectNavy.NavyList

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemNavyBinding
import com.andishepardazansamandrail.app.ui.selectNavy.NavyViewModel
import com.andishepardazansamandrail.model.thinker.Navy

class NavyViewHolder(parent: View): RecyclerView.ViewHolder(parent) {

    private val binding = ItemNavyBinding.bind(parent)

    fun bindTo(item: Navy, viewModel: NavyViewModel, position: Int) {
        binding.navyItem = item
        binding.viewModel = viewModel
        binding.navyPosition = position
    }
}