package com.andishepardazansamandrail.app.ui.navy

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.domain.usecase.thinker.GetNavyTypeUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.GetNavyUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.SetNavyIdUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.SetNavyTypeIdUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Navy
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ChangeNavyViewModel (private val getNavyUseCase: GetNavyUseCase,
                           private val getNavyTypeUseCase: GetNavyTypeUseCase,
                           private val setNavyIdUseCase: SetNavyIdUseCase,
                           private val setNavyTypeIdUseCase: SetNavyTypeIdUseCase,
                           private val dispatchers: AppDispatchers
): BaseViewModel() {

    init {
        navy()
    }
    private val _navy = MediatorLiveData<Resource<List<Navy>>>()
    val navy: LiveData<Resource<List<Navy>>> get() = _navy
    private var navySource: LiveData<Resource<List<Navy>>> = MutableLiveData()

    private val _navyType = MediatorLiveData<Resource<List<Navy>>>()
    val navyType: LiveData<Resource<List<Navy>>> get() = _navyType
    private var navyTypeSource: LiveData<Resource<List<Navy>>> = MutableLiveData()

    val _selectedNavy = MutableLiveData<Navy>()
    val selectedNavy: LiveData<Navy> get() = _selectedNavy

    val _selectedNavyType = MutableLiveData<Navy>()
    val selectedNavyType: LiveData<Navy> get() = _selectedNavyType

    fun navy() =
        viewModelScope.launch(dispatchers.main) {
            _navy.removeSource(navySource)
            withContext(dispatchers.io) {
                navySource = getNavyUseCase.invoke(
                    ConstRepo.NAVY
                )
            }
            _navy.addSource(navySource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _navy.value = it

                        }
                    }

                    // Loading state

                    // Error state
                    Resource.Status.ERROR -> {
                        //  _snackbarError.value = Event(R.string.an_error_happened)
                        _navy.value = it
                    }
                }
            }
        }


    fun navyType(navyId : Int) =
        viewModelScope.launch(dispatchers.main) {
            _navyType.removeSource(navyTypeSource)
            withContext(dispatchers.io) {
                navyTypeSource = getNavyTypeUseCase.invoke(
                    ConstRepo.NAVY_TYPE + "/${navyId}"
                )
            }
            _navyType.addSource(navyTypeSource) {


                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        it?.data.apply {
                            _navyType.value = it

                        }
                    }

                    // Loading state

                    // Error state
                    Resource.Status.ERROR -> {
                        //  _snackbarError.value = Event(R.string.an_error_happened)
                        _navyType.value = it
                    }
                }
            }
        }


    fun setNavy(navy : Navy){
        setNavyIdUseCase.invoke(navy.trailId)
        _selectedNavy.value = navy;
    }

    fun setNavyType(navy : Navy){
        setNavyTypeIdUseCase.invoke(navy.trailId)
        _selectedNavyType.value = navy;
    }
  //  fun navigateToMain() = navigate(NavyFragmentDirections.actionNavyFragmentToMainFragment())

}