package com.andishepardazansamandrail.app.ui.cargoHistory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.R
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveItemDiffCallback
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveViewHolder
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveViewModel
import com.andishepardazansamandrail.model.thinker.Cargo

class CargoHistoryAdaptor (private val viewModel: CargoHistoryViewModel): RecyclerView.Adapter<CargoHistoryViewHolder>() {

    private val items: MutableList<Cargo> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = CargoHistoryViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_cargo_history,
            parent,
            false
        )
    )
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: CargoHistoryViewHolder, position: Int)  = holder.bindTo(items[position], viewModel,position)

    fun updateData(cargoes: List<Cargo>) {

        val diffCallback = CargoHistoryItemDiffCallback(items, cargoes)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(cargoes)

        //notifyDataSetChanged()
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(cargo: Cargo, position: Int){

        items[position] = cargo
        notifyItemChanged(position)
    }
}