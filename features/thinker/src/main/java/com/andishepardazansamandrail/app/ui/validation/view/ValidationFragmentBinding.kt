package com.andishepardazansamandrail.app.ui.validation.view

import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.andishepardazansamandrail.model.thinker.Driver

@BindingAdapter("bind:textName")
fun setTextName(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.firstName} ${driver.lastName}"
       // textView.text = "جمال طالبی"
    }
}

@BindingAdapter("bind:textNationalCode")
fun setTextNationalCode(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.nationalCode }"
    }
}

@BindingAdapter("bind:textFatherName")
fun setTextFatherName(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.fatherName}"
    }
}


@BindingAdapter("bind:textIdentityCode")
fun setTextIdentityCode(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.identityNumber}"
    }
}


@BindingAdapter("bind:textBirthDay")
fun setTextBirthDay(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.birthDay}"
    }
}


@BindingAdapter("bind:textSmartCardId")
fun setTextSmartCardId(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.smartCardId}"
    }
}

@BindingAdapter("bind:textDriverType")
fun setTextDriverType(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.driverType}"
    }
}


@BindingAdapter("bind:textInsuranceNumber")
fun setTextInsuranceNumber(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.insuranceNumber}"
    }
}


@BindingAdapter("bind:textLicenseNumber")
fun setTextLicenseNumber(textView: AppCompatTextView, driver: Driver?) {
    driver?.apply {
        textView.text = "${driver.licenseNumber}"
    }
}



