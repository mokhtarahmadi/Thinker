package com.andishepardazansamandrail.app.ui.cargoSalon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.andishepardazansamandrail.app.databinding.FragmentCargoSalonBinding
import com.andishepardazansamandrail.app.databinding.FragmentProfileBinding
import com.andishepardazansamandrail.app.ui.home.HomeViewModel
import com.andishepardazansamandrail.app.ui.profile.ProfileFragment
import com.andishepardazansamandrail.app.ui.profile.ProfileViewModel
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.viewVisibility
import kotlinx.android.synthetic.main.fragment_cargo_salon.*
import kotlinx.android.synthetic.main.loading_central.*
import org.koin.android.viewmodel.ext.android.viewModel

class CargoSalonFragment : BaseFragment() {
    private val viewModel: CargoSalonViewModel by  viewModel()
    private lateinit var  binding: FragmentCargoSalonBinding



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCargoSalonBinding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return  binding.root

    }
    override fun getViewModel(): BaseViewModel = viewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRecyclerView()

        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })
    }
    private fun configureRecyclerView() {
       // viewModel.configureSearchFilterAdapter(filters_rv)
        binding.cargoRv.adapter = CargoSalonAdapter(viewModel)
    }
}