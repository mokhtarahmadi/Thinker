package com.andishepardazansamandrail.app.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.work.WorkManager
import com.andishepardazansamandrail.app.databinding.FragmentSplashBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import kotlinx.coroutines.*
import org.koin.android.viewmodel.ext.android.viewModel
import kotlin.coroutines.CoroutineContext

class SplashFragment : BaseFragment() , CoroutineScope {
    private val viewModel: SplashViewModel by viewModel()
    private lateinit var binding: FragmentSplashBinding
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSplashBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        launch {
            delay(3000)
            withContext(Dispatchers.Main) {
                when(viewModel.userType()){
                   0 -> viewModel.navigateToSelectUserType()
                    //0 -> viewModel.navigateToLogin()
                    1-> { if (viewModel.isUserLoggedIn()){
                        viewModel.cargo()
                    }
                    else{
                        viewModel.navigateToSelectUserType()
                        //viewModel.navigateToLogin()
                    }}
                    2 -> viewModel.navigateToMainAssistant()
                }

            }
        }

        viewModel.cargo.observe(viewLifecycleOwner , Observer {
            if (it == null || it.data?.status==6 || it.data?.status==0 || it.data==null)
                WorkManager.getInstance().cancelAllWorkByTag("JobSendLocation");

            if(viewModel.hasNavy())
                viewModel.navigateToMain()
            else
                viewModel.navigateToNavy()
        })
    }
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()
}