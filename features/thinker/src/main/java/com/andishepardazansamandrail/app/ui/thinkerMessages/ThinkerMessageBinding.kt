package com.andishepardazansamandrail.app.ui.thinkerMessages

import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.ui.messages.MessageAdapter
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Message
import com.andishepardazansamandrail.model.thinker.ThinkerMessage

object ThinkerMessageBinding {
    @BindingAdapter("app:itemsThinkerMessage")
    @JvmStatic fun setItemsThinkerMessage(recyclerView: RecyclerView, resource: Resource<List<ThinkerMessage>>?) {
        with(recyclerView.adapter as ThinkerMessageAdapter) {
            resource?.data?.let { updateData(it) }
        }

    }


    @JvmStatic @BindingAdapter("bind:tvSubjectMessage")
    fun setSubjectMessage(view: AppCompatTextView, resource: ThinkerMessage?) {

        resource?.let {

                view.text = it.subject
        }

    }

    @JvmStatic @BindingAdapter("bind:tvDescriptionMessage")
    fun setDescriptionMessage(view: AppCompatTextView, resource: ThinkerMessage?) {

        resource?.let {

            view.text = it.description
        }

    }
    @JvmStatic @BindingAdapter("bind:tvDateTimeMessage")
    fun setDateTimeMessage(view: AppCompatTextView, resource: ThinkerMessage?) {

        resource?.let {
            view.text =  it.persianDateTime
        }

    }





    @JvmStatic @BindingAdapter("bind:setThinkerMessageNoData")
    fun setThinkerMessageNoData(view: LinearLayoutCompat, resource: Resource<List<ThinkerMessage>>?) {

        resource?.data?.let {
            if (it.isNotEmpty())
                view.visibility = View.GONE
            else
                view.visibility = View.VISIBLE
        }

    }
}