package com.andishepardazansamandrail.app.ui.thinkerMessages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.andishepardazansamandrail.app.databinding.FragmentThinkerMessageBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.viewVisibility
import kotlinx.android.synthetic.main.loading_central.*
import org.koin.android.viewmodel.ext.android.viewModel

class ThinkerMessageFragment : BaseFragment() {
    private val viewModel: ThinkerMessageViewModel by  viewModel()
    private lateinit var  binding: FragmentThinkerMessageBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentThinkerMessageBinding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return  binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configureRecyclerView()
        viewModel.loadingAnimation.observe(viewLifecycleOwner, Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })
    }

    private fun configureRecyclerView() {
        binding.messagesThinkerRv.adapter = ThinkerMessageAdapter(viewModel)
    }
    override fun getViewModel(): BaseViewModel = viewModel
}