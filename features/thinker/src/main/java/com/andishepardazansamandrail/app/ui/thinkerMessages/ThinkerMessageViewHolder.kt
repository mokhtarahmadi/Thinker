package com.andishepardazansamandrail.app.ui.thinkerMessages

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.databinding.ItemThinkerMessageBinding
import com.andishepardazansamandrail.model.thinker.ThinkerMessage

class ThinkerMessageViewHolder (parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemThinkerMessageBinding.bind(parent)

    fun bindTo(message: ThinkerMessage, viewModel: ThinkerMessageViewModel, position: Int) {
        binding.message = message
        binding.viewModel = viewModel

    }


}