package com.andishepardazansamandrail.app.ui.suggestedCargo

import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class SuggestedCargoFragment : BaseFragment() {
    private val viewModel: SuggestedCargoViewModel by viewModel()

    override fun getViewModel(): BaseViewModel = viewModel
}