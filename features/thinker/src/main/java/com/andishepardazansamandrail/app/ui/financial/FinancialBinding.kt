package com.andishepardazansamandrail.app.ui.financial

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.app.ui.cargoReserve.CargoReserveAdapter
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.thinker.Cargo
import com.andishepardazansamandrail.model.thinker.Financial
import com.andishepardazansamandrail.model.thinker.FinancialDetail

object FinancialBinding {
    @BindingAdapter("app:itemsFinancial")
    @JvmStatic fun setItems(recyclerView: RecyclerView, resource: Resource<List<FinancialDetail>>?) {
        with(recyclerView.adapter as FinancialAdepter) {
            resource?.data?.let { updateData(it) }
        }
    }

    @BindingAdapter("bind:tvFinancial")
    @JvmStatic fun setFinancial(textView: TextView, resource: Int?) {
        resource?.apply {
            textView.text = "شماره : ${resource}"
        }
    }

    @BindingAdapter("bind:tvWayBill")
    @JvmStatic fun setWayBill(textView: TextView, resource: String?) {
        resource?.apply {
            textView.text = "شماره بارنامه: ${resource}"
        }
    }

    @BindingAdapter("bind:tvIssueDate")
    @JvmStatic fun setIssueDate(textView: TextView, resource: String?) {
        resource?.apply {
            textView.text = "تاریخ صدور: ${resource}"
        }
    }

    @BindingAdapter("bind:tvNationalCode")
    @JvmStatic fun setNationalCode(textView: TextView, resource: String?) {
        resource?.apply {
            textView.text = "کد ملی: ${resource}"
        }
    }

    @BindingAdapter("bind:tvPayable")
    @JvmStatic fun setPayable(textView: TextView, resource: Int?) {
        resource?.apply {
            textView.text = "قابل پرداخت: ${resource}"
        }
    }

    @BindingAdapter("bind:tvClearing")
    @JvmStatic fun setClearing(textView: TextView, resource: Int?) {
        resource?.apply {
            textView.text = "مقدار تسویه: ${resource}"
        }
    }

    @BindingAdapter("bind:tvRemaining")
    @JvmStatic fun setRemaining(textView: TextView, resource: Int?) {
        resource?.apply {
            textView.text = "مقدار باقیمانده: ${resource}"
        }
    }

    @JvmStatic @BindingAdapter("bind:setFinancialNoData")
    fun setFinancialNoData(view: LinearLayoutCompat, resource: Resource<List<Financial>>?) {

        resource?.data?.let {
            if (it.isNotEmpty())
                view.visibility = View.GONE
            else
                view.visibility = View.VISIBLE
        }

    }


    @BindingAdapter("bind:tvTotalPayable")
    @JvmStatic fun setTotalPayable(textView: TextView, resource: Long) {
        resource?.apply {
            textView.text = "${resource}"
        }
    }

    @BindingAdapter("bind:tvTotalClearing")
    @JvmStatic fun setTotalClearing(textView: TextView, resource: Long) {
        resource?.apply {
            textView.text = "${resource}"
        }
    }

    @BindingAdapter("bind:tvTotalRemaining")
    @JvmStatic fun setTotalRemaining(textView: TextView, resource: Long) {
        resource?.apply {
            textView.text = "${resource}"
        }
    }
}