package com.andishepardazansamandrail.assistant.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.andishepardazansamandrail.assistant.databinding.FragmentLoginAssistantBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class LoginAssistantFragment : BaseFragment()  {
    private val viewModel: LoginViewModel by  viewModel()
    private lateinit var binding: FragmentLoginAssistantBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginAssistantBinding.inflate(inflater,container,false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.login.observe(viewLifecycleOwner, Observer {

        })
    }
    override fun getViewModel(): BaseViewModel = viewModel
}