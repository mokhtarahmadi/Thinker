package com.andishepardazansamandrail.assistant.ui.main

import androidx.recyclerview.widget.DiffUtil
import com.andishepardazansamandrail.model.assistant.SemiTrailersDelivered

class SemiTrailerItemDiffCallback(private val oldList: List<SemiTrailersDelivered>,
                                  private val newList: List<SemiTrailersDelivered>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return true
    }
}