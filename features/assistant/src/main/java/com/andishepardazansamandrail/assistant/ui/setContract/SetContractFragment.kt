package com.andishepardazansamandrail.assistant.ui.setContract

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.andishepardazansamandrail.app.ui.cargoDetail.CargoDetailFragmentArgs
import com.andishepardazansamandrail.assistant.databinding.FragmentSetContractBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.initToolbar
import com.andishepardazansamandrail.common.extension.viewVisibility
import com.andishepardazansamandrail.model.assistant.SemiTrailer
import kotlinx.android.synthetic.main.fragment_set_contract.*
import kotlinx.android.synthetic.main.loading_central.*
import kotlinx.android.synthetic.main.toolbar_assistant.*
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class SetContractFragment : BaseFragment() {
    private val args: SetContractFragmentArgs by navArgs()
    private val viewModel: SetContractViewModel by viewModel {  parametersOf(args.viewState)}
    private lateinit var binding: FragmentSetContractBinding
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSetContractBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar("مسئول فنی", txt_title, rpl_back)



        if (args.viewState.key == 0 ){
             deliveryExpand.expand()
            infoExpand.collapse()
            rv_semi_trailer.viewVisibility(View.GONE)
            btn_next.viewVisibility(View.GONE)
        }else{
            deliveryExpand.collapse()
            infoExpand.expand()
            rv_semi_trailer.viewVisibility(View.VISIBLE)
            btn_next.viewVisibility(View.VISIBLE)

        }

        
        btn_delivery.setOnClickListener {

        }

        viewModel.setDeliveryOfAmount.observe(viewLifecycleOwner, Observer {  })
        viewModel.loadingAnimation.observe(requireActivity(), Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })

        viewModel.contract.observe(viewLifecycleOwner, Observer {

        })
        viewModel.setDelivery.observe(viewLifecycleOwner , Observer {
            it.data?.apply {
                deliveryExpand.collapse()
                infoExpand.expand()
                rv_semi_trailer.viewVisibility(View.VISIBLE)
            }


        })


        viewModel.semiTrailerOfSpec.observe(viewLifecycleOwner , Observer {
            it?.data?.let {
                if (!it.semiTrailerTechnicalSpecifications?.any { x -> x.deliveryOfAmount==0 })
                    btn_next.viewVisibility(View.GONE)
                else
                    btn_next.viewVisibility(View.VISIBLE)

            }
        })



        configureRecyclerView()
    }

    private fun configureRecyclerView() {
        rv_semi_trailer.adapter = SetContractAdapter(viewModel)
    }

}