package com.andishepardazansamandrail.assistant.ui.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.assistant.databinding.ItemMainAssistantBinding
import com.andishepardazansamandrail.model.assistant.SemiTrailersDelivered
import com.andishepardazansamandrail.model.assistant.ViewState

class SemiTrailerViewHolder(parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemMainAssistantBinding.bind(parent)

    fun bindTo(semiTrailersDelivered: SemiTrailersDelivered, viewModel: MainAssistantViewModel, position: Int) {
        binding.semiTrailersDelivered = semiTrailersDelivered
        binding.viewModel = viewModel

        binding.root.setOnClickListener {
            viewModel.navigateToSetContract2(ViewState(1,semiTrailersDelivered.trailerCode.toString()))
        }
        binding?.apply {
        }
    }
}