package com.andishepardazansamandrail.assistant.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.assistant.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.assistant.LoginAssistantUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.SetUserTypeUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.UserLoginUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.LoginInputAssistant
import com.andishepardazansamandrail.model.thinker.Login
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(private val userLoginUseCase: LoginAssistantUseCase,
                     private val setUserTypeUseCase : SetUserTypeUseCase,
                     private val dispatchers: AppDispatchers) : BaseViewModel() {

    private val _login = MediatorLiveData<Resource<Login>>()
    val login: LiveData<Resource<Login>> get() = _login
    private var loginSource: LiveData<Resource<Login>> = MutableLiveData()

    private val _email = MutableLiveData<String>()
    val email: LiveData<String> get() = _email

    private val _password = MutableLiveData<String>()
    val password: LiveData<String> get() = _password

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    fun onButtonClick(email: String?, password: String?) {

        checkParameters(email, password)
    }

    private fun checkParameters(email: String?, password: String?) {

        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            _snackbarErrorServer.value = Event("ایمیل و پسورد خود وارد کنید")
        } else {
            userLogin(LoginInputAssistant(email, password))
        }
    }

    private fun userLogin(loginInputAssistant : LoginInputAssistant) =
        viewModelScope.launch(dispatchers.main) {
            _login.removeSource(loginSource)
            withContext(dispatchers.io) {
                loginSource = userLoginUseCase.invoke(
                    ConstRepo.LOGIN_ASSISTANT_URL,
                    loginInputAssistant

                )
            }
            _login.addSource(loginSource) {

                _login.value = it

                when (it.status) {

                    // Success state
                    Resource.Status.SUCCESS -> {
                        _loadingAnimation.value = false
                        setUserTypeUseCase.invoke(2)
                        navigateToMain()
                    }

                    // Loading state
                    Resource.Status.LOADING -> _loadingAnimation.value = true

                    // Error state
                    Resource.Status.ERROR -> {
                        _loadingAnimation.value = false
                        _snackbarError.value = Event(R.string.an_error_happened)

                    }
                }
            }
        }

    fun onTextChangedIEmail(s: CharSequence) {
        _email.value = s.toString()
    }

    fun onTextChangedPassword(s: CharSequence) {
        _password.value = s.toString()
    }

    fun navigateToMain() = navigate(LoginAssistantFragmentDirections.actionLoginFragmentToMainAssistantFragment())
}