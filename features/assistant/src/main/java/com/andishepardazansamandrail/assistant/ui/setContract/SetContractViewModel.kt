package com.andishepardazansamandrail.assistant.ui.setContract

import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.andishepardazansamandrail.assistant.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.domain.usecase.assistant.*
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.*
import com.andishepardazansamandrail.model.thinker.Cargo
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class SetContractViewModel(private val viewState: ViewState,
    private val dispatchers: AppDispatchers,
                           private val getDriversUseCase: GetDriversUseCase,
                           private val getSemiTrailersUseCase: GetSemiTrailersUseCase,
                           private val setDeliveryUseCase: SetDeliveryUseCase,
                           private val setContractsUseCase: SetContractsUseCase,
                           private val getPercentsUseCase: GetPercentsUseCase,
                           private val getUserUseCase: GetUserUseCase,
                           private val setDeliveryOfAmountUseCase: SetDeliveryOfAmountUseCase,
                           private val getTechnicalSpecificationBySemiTrailerUseCase: GetTechnicalSpecificationBySemiTrailerUseCase
)  : BaseViewModel() {


    init {
        if (viewState.key == 0 ){
            getDrivers()
            getSemiTrailers()
        }else{
            getTechnicalSpecificationBySemiTrailer(viewState.value)
        }


    }

    var user = getUserUseCase.invoke()
    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    private val itemsPercent: MutableList<Percent> = mutableListOf()

    private val itemsSetContract: MutableList<SetContract> = ArrayList()
    private val itemsSetDeliveryOfAmount: MutableList<SetDeliveryOfAmount> = ArrayList()

    private val _drivers = MediatorLiveData<Resource<List<Driver>>>()
    val drivers: LiveData<Resource<List<Driver>>> get() = _drivers
    private var driversSource: LiveData<Resource<List<Driver>>> = MutableLiveData()


    private val _semiTrailers = MediatorLiveData<Resource<List<SemiTrailer>>>()
    val semiTrailers: LiveData<Resource<List<SemiTrailer>>> get() = _semiTrailers
    private var semiTrailersSource: LiveData<Resource<List<SemiTrailer>>> = MutableLiveData()

    private val _contract = MediatorLiveData<Resource<Unit>>()
    val contract: LiveData<Resource<Unit>> get() = _contract
    private var contractSource: LiveData<Resource<Unit>> = MutableLiveData()


    private val _setDelivery = MediatorLiveData<Resource<Int>>()
    val setDelivery: LiveData<Resource<Int>> get() = _setDelivery
    private var setDeliverySource: LiveData<Resource<Int>> = MutableLiveData()

    private val _setDeliveryOfAmount = MediatorLiveData<Resource<Unit>>()
    val setDeliveryOfAmount: LiveData<Resource<Unit>> get() = _setDeliveryOfAmount
    private var setDeliveryOfAmountSource: LiveData<Resource<Unit>> = MutableLiveData()

    private val _semiTrailerOfSpec = MediatorLiveData<Resource<SemiTrailerOfSpec>>()
    val semiTrailerOfSpec: LiveData<Resource<SemiTrailerOfSpec>> get() = _semiTrailerOfSpec
    private var semiTrailerOfSpecSource: LiveData<Resource<SemiTrailerOfSpec>> = MutableLiveData()

    private val _percents = MediatorLiveData<List<Percent>>()
    val percents: LiveData<List<Percent>> get() = _percents

    private fun getDrivers() = viewModelScope.launch(dispatchers.main) {
        _drivers.removeSource(driversSource)
        withContext(dispatchers.io) {
            driversSource = getDriversUseCase(ConstRepo.GET_DRIVER_URL)
        }
        _drivers.addSource(driversSource) {
            _drivers.value = it
            if (it.status == Resource.Status.ERROR)
                _snackbarErrorServer.value = Event("خطایی رخ داده است")
        }
    }



    private fun getSemiTrailers() = viewModelScope.launch(dispatchers.main) {
        _semiTrailers.removeSource(semiTrailersSource)
        withContext(dispatchers.io) {
            semiTrailersSource = getSemiTrailersUseCase(ConstRepo.GET_SEMI_TRAILER_URL)
        }
        _semiTrailers.addSource(semiTrailersSource) {
            _semiTrailers.value = it
            if (it.status == Resource.Status.ERROR)
                _snackbarErrorServer.value = Event("خطایی رخ داده است")
        }
    }

    private fun setDelivery(setDelivery: SetDelivery , trailerCode: String) = viewModelScope.launch(dispatchers.main) {
        _setDelivery.removeSource(setDeliverySource)
        withContext(dispatchers.io) {
            setDeliverySource = setDeliveryUseCase.invoke(ConstRepo.SET_DELIVERY_URL , setDelivery)
        }
        _setDelivery.addSource(setDeliverySource) {
            _setDelivery.value = it
            when (it.status){
                Resource.Status.SUCCESS -> {
                    getTechnicalSpecificationBySemiTrailer(trailerCode)
                }
                Resource.Status.ERROR ->{
                    _loadingAnimation.value = false
                    _snackbarErrorServer.value = Event("خطایی رخ داده است")
                }
                Resource.Status.LOADING -> _loadingAnimation.value = true
            }
        }
    }

    private fun setDeliveryOfAmounts(setDeliveryOfAmounts: List<SetDeliveryOfAmount> , deliveryId: Int) = viewModelScope.launch(dispatchers.main) {
        _setDeliveryOfAmount.removeSource(setDeliveryOfAmountSource)
        withContext(dispatchers.io) {
            var url  = ConstRepo.SET_DELIVERY_OF_AMOUNT_URL + "/${deliveryId}"
            setDeliveryOfAmountSource = setDeliveryOfAmountUseCase.invoke( url, setDeliveryOfAmounts)
        }
        _setDeliveryOfAmount.addSource(setDeliveryOfAmountSource) {
            _setDeliveryOfAmount.value = it

            when (it.status){
                Resource.Status.SUCCESS -> {
                    _loadingAnimation.value = false
                    navigateUp()
                }
                Resource.Status.ERROR ->{
                    _loadingAnimation.value = false
                    _snackbarErrorServer.value = Event("خطایی رخ داده است")
                }
                Resource.Status.LOADING -> _loadingAnimation.value = true
            }
        }
    }

    private fun setContracts(setContracts: List<SetContract>) = viewModelScope.launch(dispatchers.main) {
        _contract.removeSource(contractSource)
        withContext(dispatchers.io) {
            contractSource = setContractsUseCase.invoke(ConstRepo.SET_CONTRACT_URL , setContracts)
        }
        _contract.addSource(contractSource) {
            _contract.value = it

            when (it.status){
                Resource.Status.SUCCESS -> {
                    _loadingAnimation.value = false
                    navigateUp()
                }
                Resource.Status.ERROR ->{
                    _loadingAnimation.value = false
                    _snackbarErrorServer.value = Event("خطایی رخ داده است")
                }
                Resource.Status.LOADING -> _loadingAnimation.value = true
            }
        }
    }

    private fun getTechnicalSpecificationBySemiTrailer(trailerCode : String) = viewModelScope.launch(dispatchers.main) {
        _semiTrailerOfSpec.removeSource(semiTrailerOfSpecSource)
        withContext(dispatchers.io) {
            var url = ConstRepo.GetTechnicalSpecificationBySemiTrailer_URL + "?trailerCode=${trailerCode}"
            semiTrailerOfSpecSource = getTechnicalSpecificationBySemiTrailerUseCase.invoke(url,trailerCode)
        }
        _semiTrailerOfSpec.addSource(semiTrailerOfSpecSource) {
            if (it.status == Resource.Status.ERROR){
                _loadingAnimation.value = false
                _snackbarErrorServer.value = Event("خطایی رخ داده است")
            }

            if (it.status == Resource.Status.SUCCESS){
                _loadingAnimation.value = false
                _semiTrailerOfSpec.value = it
                getPercentss()
            }

        }
    }
    fun onApplyClick(
        spinDriver: AppCompatSpinner,
        spinSemiTrailer: AppCompatSpinner
    ){


        if (spinDriver.selectedItemPosition > 0 && spinSemiTrailer.selectedItemPosition > 0) {

            var semiTrailerId = semiTrailers.value!!.data!![spinSemiTrailer.selectedItemPosition-1].semiTrailerId
            var driverId = drivers.value!!.data!![spinDriver.selectedItemPosition - 1 ].personId
            var trailerCode = semiTrailers.value!!.data!![spinSemiTrailer.selectedItemPosition-1].trailerCode
            setDelivery(SetDelivery(0, semiTrailerId,driverId,getUserUseCase.invoke().toInt(),null,null ),trailerCode)
        }else{
            _snackbarErrorServer.value = Event("راننده و نیم یدک را انتخاب کنید")

        }

    }

    fun setValueForContract(spinPercentType: AppCompatSpinner, semiTrailerTechnicalSpecification: SemiTrailerTechnicalSpecification ,type : Int){
        var percent = percents.value?.get(spinPercentType.selectedItemPosition - 1)?.id
        if (type ==1){
            if (itemsSetContract.size>0){
                var item =
                    itemsSetContract.singleOrNull { x -> x.technicalSpecificationId == semiTrailerTechnicalSpecification.technicalSpecificationId }
                if (item!=null)
                    itemsSetContract.remove(item)
            }


            var setContract = SetContract(semiTrailerTechnicalSpecification.contractId,semiTrailerTechnicalSpecification.technicalSpecificationId,semiTrailerOfSpec.value?.data?.semiTrailerDelivery?.deliveryId!!,user.toInt(),percent!!)
            itemsSetContract.add(setContract)

        }else{
            if (itemsSetDeliveryOfAmount.size>0){
                var item =
                    itemsSetDeliveryOfAmount?.singleOrNull { x -> x.technicalSpecificationId == semiTrailerTechnicalSpecification.technicalSpecificationId.toLong() }
                if (item!=null)
                    itemsSetDeliveryOfAmount.remove(item)
            }


            var setContract = SetDeliveryOfAmount(semiTrailerTechnicalSpecification.technicalSpecificationId.toLong(),percent?.toDouble()!!)
            itemsSetDeliveryOfAmount.add(setContract)
        }
    }


    fun getPercentss(){
        _percents.value = getPercentsUseCase.invoke().value?.data
    }

    fun setContractOnClick(){
        semiTrailerOfSpec?.value?.data?.semiTrailerTechnicalSpecifications?.let{
            if (it.any { x -> x.deliveryAmount==0 } && itemsSetContract.any { x -> x.deliveryAmount==0 }){
                _snackbarErrorServer.value = Event("درصد سلامت کل مقادیر باید مشخص شود")
            }else{
                if (it.any { x -> x.contractId==0 }){
                    setContracts(itemsSetContract)
                }else {
                    if (it.any { x -> x.deliveryOfAmount==0 } && itemsSetDeliveryOfAmount.any { x -> x.deliveryOfAmount==0.toDouble() }) {
                        _snackbarErrorServer.value = Event("درصد سلامت کل مقادیر باید مشخص شود")
                    }else
                    {
                        setDeliveryOfAmounts(itemsSetDeliveryOfAmount,semiTrailerOfSpec.value?.data?.semiTrailerDelivery?.deliveryId!!)
                    }
                }
            }
        }
    }

}