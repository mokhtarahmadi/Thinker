package com.andishepardazansamandrail.assistant.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.andishepardazansamandrail.assistant.databinding.FragmentMainAssistantBinding
import com.andishepardazansamandrail.common.base.BaseFragment
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.extension.initToolbar
import com.andishepardazansamandrail.common.extension.viewVisibility
import com.andishepardazansamandrail.model.assistant.SearchSemiTrailer
import kotlinx.android.synthetic.main.filter_assistant.*
import kotlinx.android.synthetic.main.filter_assistant_expand.*
import kotlinx.android.synthetic.main.fragment_main_assistant.*
import kotlinx.android.synthetic.main.loading_central.*
import kotlinx.android.synthetic.main.toolbar_assistant.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainAssistantFragment : BaseFragment() {

    private val viewModel: MainAssistantViewModel by viewModel()
    private lateinit var binding: FragmentMainAssistantBinding
    override fun getViewModel(): BaseViewModel = viewModel
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentMainAssistantBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar("مسئول فنی", txt_title, rpl_back)
        rpl_yes.setOnClickListener {
            var driverName =  ""
            var plate = ""
            var trailerCode = ""
            var trailerName = ""

            if (spin_name.text.toString().isNotEmpty())
                driverName = spin_name.text.toString()

            if (spin_code.text.toString().isNotEmpty())
                plate = spin_code.text.toString()

            if (spin_irc.text.toString().isNotEmpty())
                trailerCode = spin_irc.text.toString()

            if (spin_gtin.text.toString().isNotEmpty())
                trailerName = spin_gtin.text.toString()

            viewModel.clickSearch(SearchSemiTrailer(driverName,plate,trailerCode,trailerName))
        }

        viewModel.loadingAnimation.observe(requireActivity(), Observer {
            if (it) {
                loading.viewVisibility(View.VISIBLE)
            } else {
                loading.viewVisibility(View.GONE)
            }
        })

        rpl_no.setOnClickListener {
            advancedSearchExpand.collapse()
        }

        configureRecyclerView()

        viewModel.semiTrailers.observe(viewLifecycleOwner, Observer {
            advancedSearchExpand.collapse()
        })
        viewModel.searchExpand.observe(requireActivity(), Observer {

            if (activity != null && isAdded) {
                if (it) {
                    advancedSearchExpand.expand()
                } else {
                    advancedSearchExpand.collapse()
                    viewModel.clickSearch(SearchSemiTrailer())
                }
            }

        })

    }


    private fun configureRecyclerView() {

        viewModel.configureSearchFilterAdapter(filters_rv)
        semiTrailer_rv.adapter = SemiTrailerAdapter(viewModel)

    }

}