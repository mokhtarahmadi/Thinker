package com.andishepardazansamandrail.assistant.ui.setContract

import androidx.recyclerview.widget.DiffUtil
import com.andishepardazansamandrail.model.assistant.SemiTrailerTechnicalSpecification
import com.andishepardazansamandrail.model.thinker.Cargo

class SetContractItemDiffCallback(private val oldList: List<SemiTrailerTechnicalSpecification>,
                                  private val newList: List<SemiTrailerTechnicalSpecification>) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return true
    }
}