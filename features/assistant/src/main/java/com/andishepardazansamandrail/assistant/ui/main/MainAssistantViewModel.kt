package com.andishepardazansamandrail.assistant.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.assistant.R
import com.andishepardazansamandrail.common.base.BaseViewModel
import com.andishepardazansamandrail.common.utils.ConstRepo
import com.andishepardazansamandrail.common.utils.Event
import com.andishepardazansamandrail.common.utils.SearchFilterAdapter
import com.andishepardazansamandrail.domain.usecase.assistant.GetSemiTrailersDeliveredUseCase
import com.andishepardazansamandrail.domain.usecase.thinker.SetTokenUseCase
import com.andishepardazansamandrail.domain.utils.AppDispatchers
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.SearchSemiTrailer
import com.andishepardazansamandrail.model.assistant.SemiTrailer
import com.andishepardazansamandrail.model.assistant.SemiTrailersDelivered
import com.andishepardazansamandrail.model.assistant.ViewState
import com.andishepardazansamandrail.model.thinker.SearchFilter
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainAssistantViewModel(private val dispatchers: AppDispatchers,
                             private val setTokenUseCase: SetTokenUseCase,
                             private val getSemiTrailersDeliveredUseCase: GetSemiTrailersDeliveredUseCase
                             ) : BaseViewModel() ,SearchFilterAdapter.OnItemClickListener{


    init {
        clickSearch(SearchSemiTrailer())
    }

    private val _semiTrailers = MediatorLiveData<Resource<List<SemiTrailersDelivered>>>()
    val semiTrailers: LiveData<Resource<List<SemiTrailersDelivered>>> get() = _semiTrailers
    private var semiTrailersSource: LiveData<Resource<List<SemiTrailersDelivered>>> = MutableLiveData()

    private val _loadingAnimation = MutableLiveData<Boolean>()
    val loadingAnimation: LiveData<Boolean> get() = _loadingAnimation

    val _searchExpand = MutableLiveData<Boolean>()
    val searchExpand: LiveData<Boolean> get() = _searchExpand

    fun configureSearchFilterAdapter(filters_rv: RecyclerView) {

        filters_rv.adapter = SearchFilterAdapter(getSearchFilters(), this)
    }

    private fun getSearchFilters(): MutableList<SearchFilter> {

        val item1 = SearchFilter(1, "همه","")
        val item2 = SearchFilter(2, "جستجوی\u200Cپیشرفته","")

        val filterItems = mutableListOf<SearchFilter>()
        filterItems.add(item1)
        filterItems.add(item2)

        return filterItems

    }

    override fun onItemClicked(searchItem: SearchFilter) {
        when (searchItem.id) {

            1 -> {
                _searchExpand.value = false
            }
            2 -> {
                _searchExpand.value = true
            }

        }
    }


    fun navigateToSetContract() = navigate(MainAssistantFragmentDirections.mainAssistantToSetContract(
        ViewState()
    ))

    fun navigateToSetContract2(viewState: ViewState) = navigate(MainAssistantFragmentDirections.mainAssistantToSetContract(
        viewState
    ))

    fun clickSearch(searchSemiTrailer: SearchSemiTrailer){

        getSemiTrailers(searchSemiTrailer)
    }

    private fun getSemiTrailers(searchSemiTrailer: SearchSemiTrailer) = viewModelScope.launch(dispatchers.main) {
        _semiTrailers.removeSource(semiTrailersSource)
        withContext(dispatchers.io) {
            semiTrailersSource = getSemiTrailersDeliveredUseCase(ConstRepo.GetSemiTrailersDelivered_URL,searchSemiTrailer)
        }
        _semiTrailers.addSource(semiTrailersSource) {
            _semiTrailers.value = it
            if (it.status == Resource.Status.ERROR)
                _snackbarError.value = Event(R.string.an_error_happened)
        }
    }
}