package com.andishepardazansamandrail.assistant.ui.main

import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.assistant.ui.setContract.SetContractAdapter
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.SemiTrailerTechnicalSpecification
import com.andishepardazansamandrail.model.assistant.SemiTrailersDelivered

@BindingAdapter("bind:itemSemiTrailerDelivered")
fun itemSemiTrailerDelivered(recyclerView: RecyclerView, resource: Resource<List<SemiTrailersDelivered>>?) {
    with(recyclerView.adapter as SemiTrailerAdapter) {
        resource?.data?.let { updateData(it) }
    }
}


@BindingAdapter("bind:textPersonName1")
fun textPersonName(textView: AppCompatTextView, value: String?) {
    if (value==null)
        textView.text = "راننده : "
    else
        textView.text = "راننده : ${value}"

}


@BindingAdapter("bind:textTrailerName1")
fun textTrailerName(textView: AppCompatTextView, value: String?) {
    if (value==null)
        textView.text = "نیم یدک : "
    else
        textView.text = "نیم یدک : ${value}"

}

@BindingAdapter("bind:textPlate1")
fun textPlate1(textView: AppCompatTextView, value: String?) {
    value?.let {
        var plate = it.split("-")
        textView.text = "${plate.first()}"

    }

}

@BindingAdapter("bind:textPlate2")
fun textPlate2(textView: AppCompatTextView, value: String?) {
    value?.let {
        var plate = it.split("-")
        plate?.apply {
            textView.text = "${plate[1]}"
        }

    }

}

@BindingAdapter("bind:textPlate3")
fun textPlate3(textView: AppCompatTextView, value: String?) {
    value?.let {
        var plate = it.split("-")
        plate?.apply {
            textView.text = "${plate[2]}"
        }

    }

}
@BindingAdapter("bind:textPlate4")
fun textPlate4(textView: AppCompatTextView, value: String?) {
    value?.let {
        var plate = it.split("-")
        plate?.apply {
            textView.text = "${plate.last()}"
        }

    }

}

@BindingAdapter("bind:textPersianDeliveryDate1")
fun textPersianDeliveryDate(textView: AppCompatTextView, value: String?) {
    if (value==null)
        textView.text = "تاریخ تحویل : "
    else
        textView.text = "تاریخ تحویل : ${value}"

}

