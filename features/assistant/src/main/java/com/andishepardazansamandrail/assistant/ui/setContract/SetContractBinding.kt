package com.andishepardazansamandrail.assistant.ui.setContract

import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatSpinner
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.assistant.R
import com.andishepardazansamandrail.domain.utils.Resource
import com.andishepardazansamandrail.model.assistant.Driver
import com.andishepardazansamandrail.model.assistant.Percent
import com.andishepardazansamandrail.model.assistant.SemiTrailer
import com.andishepardazansamandrail.model.assistant.SemiTrailerTechnicalSpecification


@BindingAdapter("bind:itemSemiTrailerTechnicalSpecification")
 fun itemSemiTrailerTechnicalSpecification(
    recyclerView: RecyclerView,
    resource: List<SemiTrailerTechnicalSpecification>?
) {
    with(recyclerView.adapter as SetContractAdapter) {
        resource?.let { updateData(it) }
    }
}

@BindingAdapter(value = ["bind:percentItems", "bind:viewModel" , "bind:semiTrailerTechnicalSpecification"])
fun percentItems(
    spinner: AppCompatSpinner,
    resource: List<Percent>?,
    viewModel: SetContractViewModel,
    semiTrailerTechnicalSpecification: SemiTrailerTechnicalSpecification?
) {

    val list = mutableListOf<String>()
    list.add("-----")
    resource?.let { it?.forEach { x -> list.add("${x.value}") } }

    val daysAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
        spinner.context,
        R.layout.item_spin,
        list
    )
    spinner.adapter = daysAdapter


    spinner.setOnItemSelectedListener(object : OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            var type = 1
            if (semiTrailerTechnicalSpecification?.contractId!=0 ) {
                type = 2
            }

            if (position!=0)
            viewModel.setValueForContract(spinner,semiTrailerTechnicalSpecification!!,type)

        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    })
}

    @BindingAdapter("bind:driverItems")
    fun driverItems(spinner: AppCompatSpinner, resource: Resource<List<Driver>>?) {

        val list = mutableListOf<String>()
        list.add("-----")
        resource?.let { it.data?.forEach { x -> list.add("${x.firstName} ${x.lastName}") } }

        val daysAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            spinner.context,
            R.layout.item_spin,
            list
        )
        spinner.adapter = daysAdapter
    }

    @BindingAdapter("bind:semiTrailerItems")
    fun semiTrailerItems(spinner: AppCompatSpinner, resource: Resource<List<SemiTrailer>>?) {

        val list = mutableListOf<String>()
        list.add("-----")
        resource?.let { it.data?.forEach { x -> list.add(x.linsensePlate) } }

        val daysAdapter: ArrayAdapter<String> = ArrayAdapter(
            spinner.context,
            R.layout.item_spin,
            list
        )
        spinner.adapter = daysAdapter
    }


@BindingAdapter("bind:textPersonName")
fun textPersonName(textView: AppCompatTextView, value: String?) {
   if (value==null)
       textView.text = "راننده : "
    else
       textView.text = "راننده : ${value}"

}


@BindingAdapter("bind:textTrailerName")
fun textTrailerName(textView: AppCompatTextView, value: String?) {
    if (value==null)
        textView.text = "نیم یدک : "
    else
        textView.text = "نیم یدک : ${value}"

}

@BindingAdapter("bind:textLinsensePlate")
fun textLinsensePlate(textView: AppCompatTextView, value: String?) {
    if (value==null)
        textView.text = "شماره پلاک : "
    else
        textView.text = "شماره پلاک : ${value}"

}

@BindingAdapter("bind:textPersianDeliveryDate")
fun textPersianDeliveryDate(textView: AppCompatTextView, value: String?) {
    if (value==null)
        textView.text = "تاریخ تحویل : "
    else
        textView.text = "تاریخ تحویل : ${value}"

}

