package com.andishepardazansamandrail.assistant.ui.setContract

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.assistant.R
import com.andishepardazansamandrail.model.assistant.SemiTrailerTechnicalSpecification

class SetContractAdapter (private val viewModel: SetContractViewModel): RecyclerView.Adapter<SetContractViewHolder>() {

    private val items: MutableList<SemiTrailerTechnicalSpecification> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = SetContractViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_set_contract,
            parent,
            false
        )
    )
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: SetContractViewHolder, position: Int)  = holder.bindTo(items[position], viewModel,position)

    fun updateData(semiTrailerTechnicalSpecifications: List<SemiTrailerTechnicalSpecification>) {

        val diffCallback = SetContractItemDiffCallback(items, semiTrailerTechnicalSpecifications)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(semiTrailerTechnicalSpecifications)

//        notifyDataSetChanged()
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(semiTrailerTechnicalSpecification: SemiTrailerTechnicalSpecification, position: Int){

        items[position] = semiTrailerTechnicalSpecification
        notifyItemChanged(position)
    }
}