package com.andishepardazansamandrail.assistant.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.assistant.R
import com.andishepardazansamandrail.model.assistant.SemiTrailersDelivered

class SemiTrailerAdapter(private val viewModel: MainAssistantViewModel): RecyclerView.Adapter<SemiTrailerViewHolder>() {

    private val items: MutableList<SemiTrailersDelivered> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = SemiTrailerViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.item_main_assistant,
            parent,
            false
        )
    )
    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: SemiTrailerViewHolder, position: Int)  = holder.bindTo(items[position], viewModel,position)

    fun updateData(semiTrailersDelivered: List<SemiTrailersDelivered>) {

        val diffCallback = SemiTrailerItemDiffCallback(items, semiTrailersDelivered)
        val diffResult = DiffUtil.calculateDiff(diffCallback)

        items.clear()
        items.addAll(semiTrailersDelivered)

//        notifyDataSetChanged()
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(semiTrailersDelivered: SemiTrailersDelivered, position: Int){

        items[position] = semiTrailersDelivered
        notifyItemChanged(position)
    }
}