package com.andishepardazansamandrail.assistant.di

import com.andishepardazansamandrail.assistant.ui.login.LoginViewModel
import com.andishepardazansamandrail.assistant.ui.main.MainAssistantViewModel
import com.andishepardazansamandrail.assistant.ui.setContract.SetContractViewModel
import com.andishepardazansamandrail.domain.usecase.assistant.*
import com.andishepardazansamandrail.model.assistant.ViewState
import com.andishepardazansamandrail.model.thinker.Cargo
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val featureAssistantModule = module {

    factory { GetDriversUseCase(get()) }
    factory { GetSemiTrailersUseCase(get()) }
    factory { SetDeliveryUseCase(get()) }
    factory { SetContractsUseCase(get()) }
    factory { GetPercentsUseCase(get()) }
    factory { LoginAssistantUseCase(get()) }
    factory { SetDeliveryOfAmountUseCase(get()) }
    factory { GetTechnicalSpecificationBySemiTrailerUseCase(get()) }
    factory { GetSemiTrailersDeliveredUseCase(get()) }
    factory { GetUserUseCase(get()) }
    viewModel { LoginViewModel(get(),get(),get()) }
    viewModel { MainAssistantViewModel(get(),get(),get()) }
    viewModel { (viewState : ViewState)-> SetContractViewModel(viewState,get(),get(),get(),get(),get(),get(),get(),get(),get()) }
}
