package com.andishepardazansamandrail.assistant.ui.setContract

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.andishepardazansamandrail.assistant.databinding.ItemSetContractBinding
import com.andishepardazansamandrail.common.extension.viewVisibility
import com.andishepardazansamandrail.model.assistant.SemiTrailerTechnicalSpecification
import kotlinx.android.synthetic.main.fragment_set_contract.*

class SetContractViewHolder (parent: View): RecyclerView.ViewHolder(parent) {
    private val binding = ItemSetContractBinding.bind(parent)

    fun bindTo(semiTrailerTechnicalSpecification: SemiTrailerTechnicalSpecification, viewModel: SetContractViewModel, position: Int) {
        binding.semiTrailerTechnicalSpecification = semiTrailerTechnicalSpecification
        binding.viewModel = viewModel


        semiTrailerTechnicalSpecification?.let {
            if (it.contractId!=0 ){
                if (it.deliveryAmount==0)
                    binding.rlFirst2.viewVisibility(View.GONE)
                if (it.deliveryAmount!=0){
                    binding.rlFirst2.viewVisibility(View.VISIBLE)
                    binding.evFirst.text ="تخمین سلامت اولیه ${semiTrailerTechnicalSpecification.deliveryAmount} درصد می باشد"
                    binding.rlFirst.viewVisibility(View.GONE)

                    if (it.deliveryOfAmount==0){
                        binding.rlSecond.viewVisibility(View.GONE)
                        binding.rlSecond2.viewVisibility(View.VISIBLE)
                    }else{
                        binding.evSecond.text ="تخمین سلامت ثانویه ${semiTrailerTechnicalSpecification.deliveryOfAmount} درصد می باشد"
                        binding.rlSecond.viewVisibility(View.VISIBLE)
                        binding.rlSecond2.viewVisibility(View.GONE)

                    }

                }

            }
        }
    }
}